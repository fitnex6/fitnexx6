<%@include file="protect.jsp" %>
<%@page import="dao.MissionDAO"%>
<%@page import="entity.Mission"%>
<%@page import="java.util.TreeMap"%>
<%@page import="entity.Building"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entity.Checkpoint"%>
<%@page import="dao.BuildingDAO"%>
<%@page import="dao.CheckpointDAO"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Checkpoint</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <!-- Help Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Checkpoint Help</h4>
                            </div>

                            <div class="modal-body">
                                <center><b>Step 1: Create a Building</b><br><br> <a href="/Fitnexx6AdminWeb/building.jsp"><img src="images/building.png" style=width:128px;height:128px;></img></a></center><br><br>
                                <center><b>Step 2: Create a Mission</b><br><br> <a href="/Fitnexx6AdminWeb/mission.jsp"><img src="images/mission.png" style=width:128px;height:128px;></img></a></center><br><br>
                                <center><b>Step 3: Create a Checkpoint</b><br><br> <a href="/Fitnexx6AdminWeb/checkpoint.jsp"><img src="images/checkpoint.png" style=width:128px;height:128px;></img></a></center>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">
                                        Checkpoints
                                        <!-- Trigger the modal with a button -->
                                        <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#myModal"><i class="fa fa-question" aria-hidden="true"></i></button>
                                    </h1>
                                </div>
                            </div>                         
                            <div class="table-responsive" style="overflow-x: hidden;">
                                <%                                    char staffType = loggedIn.getStaffType();

                                    CheckpointDAO checkpointDAO = new CheckpointDAO();
                                    BuildingDAO buildingDAO = new BuildingDAO();
                                    MissionDAO missionDAO = new MissionDAO();
                                    ArrayList<Checkpoint> checkpointList = checkpointDAO.getCheckpoint();
                                    TreeMap<Integer, Building> buildingMap = buildingDAO.getBuilding();
                                    TreeMap<Integer, Mission> missionMap = missionDAO.getMission();

                                    if (staffType == 'A' || staffType == 'R') {
                                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                                    } else {
                                %>
                                <a class="add btn btn-success btn-md" data-toggle="modal" href="#add">Add Checkpoint</a><br><br>

                                <table id="mytable" class="table table-bordered table-striped">
                                    <thead>
                                    <th>Checkpoint ID</th>
                                    <th>Location Details</th>
                                    <th>Building</th>
                                    <th>Mission ID</th>
                                    <th>Action</th>
                                    </thead>

                                    <tbody>
                                        <%                                            for (Checkpoint checkpoint : checkpointList) {
                                                out.println("<tr>");
                                                out.println("<td align='middle'>" + checkpoint.getCheckpointID() + "</td>");
                                                out.println("<td align='middle'>" + checkpoint.getLocationDetails() + "</td>");
                                                out.println("<td align='middle'>" + buildingMap.get(checkpoint.getBuildingID()).getName() + "</td>");
                                                out.println("<td align='middle'>" + checkpoint.getMissionID() + "</td>");
                                        %>
                                    <td align="middle"><a class="edit btn btn-primary btn-sm" data-toggle="modal" data-id="<%=checkpoint.getCheckpointID()%>" data-details="<%=checkpoint.getLocationDetails()%>" data-bid="<%=checkpoint.getBuildingID()%>" data-missionid="<%=checkpoint.getMissionID()%>" href="#edit">Edit</a>
                                        <a class="delete btn btn-danger btn-sm" data-toggle="modal" data-id="<%=checkpoint.getCheckpointID()%>" href="#delete">Delete</a>
                                        <a class="generate btn btn-success btn-sm" data-toggle="modal" data-id="<%=checkpoint.getCheckpointID()%>" data-missiontype="<%=missionMap.get(checkpoint.getMissionID()).getMissionType()%>" href="#generate">QR Code</a></td>
                                        <%
                                                    out.println("</tr>");
                                                }
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="AddCheckpointServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Checkpoint</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    Location Details: <input class="form-control" type="text" placeholder="Location Details" name="locationDetails" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Building:
                                    <select class="form-control" name="buildingID" >
                                        <%
                                            for (Integer key : buildingMap.keySet()) {
                                                out.println("<option value=" + key + ">" + buildingMap.get(key).getName() + "</option>");
                                            }
                                        %>
                                    </select>
                                </div>       
                                <div class="form-group">
                                    Mission ID:
                                    <select class="form-control" name="missionID" >
                                        <%
                                            for (Integer key : missionMap.keySet()) {
                                                Mission mission = missionMap.get(key);

                                                if (!mission.getMissionType().equalsIgnoreCase("Special")) {
                                                    out.println("<option value=" + key + ">" + mission.getMissionID() + " - " + mission.getDescription() + "</option>");
                                                }
                                            }
                                        %>
                                    </select>
                                </div>  
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="EditCheckpointServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Checkpoint</h4>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="checkpointID" id="editCheckpointID">
                                </div>
                                <div class="form-group">
                                    Location Details: <input class="form-control" type="text" placeholder="Location Details" name="locationDetails" id="editLocationDetails" required="yes" maxlength="100"></div>
                                <div class="form-group">
                                    Building:
                                    <select class="form-control" name="buildingID" id="editBuildingID" >
                                        <%
                                            for (Integer key : buildingMap.keySet()) {
                                                out.println("<option id=" + key + " value=" + key + ">" + buildingMap.get(key).getName() + "</option>");
                                            }
                                        %>
                                    </select>
                                </div>
                                <div class="form-group">
                                    Mission ID:
                                    <select class="form-control" name="missionID" id="editMissionID" >
                                        <%
                                            for (Integer key : missionMap.keySet()) {
                                                Mission mission = missionMap.get(key);

                                                if (!mission.getMissionType().equalsIgnoreCase("Special")) {
                                                    out.println("<option value=" + key + ">" + mission.getMissionID() + " - " + mission.getDescription() + "</option>");
                                                }
                                            }
                                        %>
                                    </select>
                                </div>  
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Edit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="delete" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="DeleteCheckpointServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Delete Checkpoint</h4>
                            </div>

                            <div class="modal-body">
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record? </div>
                                <input type="hidden" name="checkpointID" id="deleteCheckpointID">
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="generate" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">QR Code</h4>
                        </div>

                        <div class="modal-body">
                            <center><b>Click on the image to download the QR Code!</b></center>
                            <center><a href="" download="qrcode" class="saveQRCode"><img src="" style=width:258px;height:258px; class="generateQR"></img></a></center>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Back</button>
                        </div>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <script>
                $(document).ready(function () {
                    $('#mytable').DataTable();
                });

                $(document).on("click", ".edit", function () {
                    var checkpointID = $(this).data('id');
                    var locationDetails = $(this).data('details');
                    var buildingID = $(this).data('bid');
                    var missionID = $(this).data('missionid');

                    $(".modal-body #editCheckpointID").val(checkpointID);
                    $(".modal-body #editLocationDetails").val(locationDetails);
                    $(".modal-body #editBuildingID").val(buildingID);
                    $(".modal-body #editMissionID").val(missionID);

                    /*
                     $(".modal-body #editBuildingID option").each(function ()
                     {
                     var temp = parseInt($(this).val());
                     
                     if (temp == buildingID) {
                     $(this).prop('selected', true);
                     }
                     });
                     
                     $(".modal-body #editMissionID option").each(function ()
                     {
                     var temp = parseInt($(this).val());
                     
                     if (temp == missionID) {
                     $(this).prop('selected', true);
                     }
                     });
                     */
                });

                $(document).on("click", ".delete", function () {
                    var checkpointID = $(this).data('id');
                    $(".modal-body #deleteCheckpointID").val(checkpointID);
                });

                $(document).on("click", ".generate", function () {
                    var checkpointID = $(this).data('id');
                    var missionType = $(this).data('missiontype');
                    $(".generateQR").attr("src", "GenerateQRCodeServlet?missionType=" + missionType + "&checkpointID=" + checkpointID);
                    $(".saveQRCode").attr("href", "GenerateQRCodeServlet?missionType=" + missionType + "&checkpointID=" + checkpointID);
                });
            </script>
        </div>
    </body>
</html>

