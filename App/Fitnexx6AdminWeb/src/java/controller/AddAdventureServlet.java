package controller;

import dao.AdventureDAO;
import dao.ClanDAO;
import entity.Staff;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import scheduledTasks.EndAdventure;
import scheduledTasks.StartAdventure;
import utility.DateAndTime;

@WebServlet(name = "AddAdventureServlet", urlPatterns = {"/AddAdventureServlet"})
@MultipartConfig(maxFileSize = 16777215)
public class AddAdventureServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            } else if (loggedIn.getStaffType() != 'M' && loggedIn.getStaffType() != 'E') {
                response.sendRedirect("home.jsp");
                return;
            }

            String name = request.getParameter("name");
            String description = request.getParameter("description");
            int points = Integer.parseInt(request.getParameter("points"));
            Date startDate = DateAndTime.stringToDate(request.getParameter("startDate"));
            Date endDate = DateAndTime.stringToDate(request.getParameter("endDate"));

            String[] clanName = request.getParameterValues("clanName");
            String[] clanDescription = request.getParameterValues("clanDescription");
            ArrayList<String> filteredClanName = new ArrayList<>();
            ArrayList<Part> clanPicture = new ArrayList<>();
            boolean noError = true;

            for (String s : clanName) {
                boolean duplicate = false;

                for (String compareTo : filteredClanName) {
                    if (s.equalsIgnoreCase(compareTo)) {
                        duplicate = true;
                        break;
                    }
                }

                if (!duplicate) {
                    filteredClanName.add(s);
                }
            }

            if (filteredClanName.size() != clanName.length) {
                noError = false;
                session.setAttribute("adventureError", "Add Adventure Failed! Please enter unique clan name!");
            } else {
                for (Part part : request.getParts()) {
                    String fileName = getSubmittedFileName(part);

                    if (fileName != null) {
                        String extension = fileName.substring(fileName.lastIndexOf("."));

                        if (extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpg")) {
                            clanPicture.add(part);
                        } else {
                            noError = false;
                            session.setAttribute("adventureError", "Add Adventure Failed! Please upload clan picture in .PNG or .JPG format only!");
                            break;
                        }
                    }
                }
            }

            if (noError) {
                AdventureDAO adventureDAO = new AdventureDAO();
                ClanDAO clanDAO = new ClanDAO();

                Date currentDate = new Date();
                Date currentDateWithoutTime = DateAndTime.setTimeToMidnight(currentDate);
                Date endDateWithoutTime = DateAndTime.setTimeToMidnight(DateAndTime.addOneDay(endDate));

                long startWaitTime = 0;
                long endWaitTime = TimeUnit.MILLISECONDS.toSeconds(endDateWithoutTime.getTime() - currentDate.getTime()) + 1;

                if (currentDateWithoutTime.before(startDate)) {
                    startWaitTime = TimeUnit.MILLISECONDS.toSeconds(startDate.getTime() - currentDate.getTime()) + 121;
                }

                int adventureID = adventureDAO.addAdventure(name, description, points, startDate, endDate, false);

                if (adventureID != 0) {
                    for (int i = 0; i < filteredClanName.size(); i++) {
                        InputStream inputStream = clanPicture.get(i).getInputStream();

                        BufferedImage inputImage = ImageIO.read(inputStream);
                        BufferedImage outputImage = new BufferedImage(350, 250, inputImage.getType());

                        Graphics2D g2d = outputImage.createGraphics();
                        g2d.drawImage(inputImage, 0, 0, 350, 250, null);
                        g2d.dispose();

                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                        ImageIO.write(outputImage, "jpg", outStream);
                        InputStream is = new ByteArrayInputStream(outStream.toByteArray());

                        clanDAO.addClan(filteredClanName.get(i), clanDescription[i], is, adventureID);
                    }

                    StartAdventure.scheduler.schedule(new StartAdventure(adventureID, name, "admin", startDate, endDate, getServletContext()), startWaitTime, TimeUnit.SECONDS);
                    EndAdventure.scheduler.schedule(new EndAdventure(adventureID, name, "admin", points, startDate, endDate, getServletContext()), endWaitTime, TimeUnit.SECONDS);                   
                } else {
                    session.setAttribute("adventureError", "Add Adventure Failed! There is already an adventure ongoing during the selected date!");
                }
            }

            response.sendRedirect("adventure.jsp");
        } catch (Exception e) {
            response.sendRedirect("home.jsp");
        }
    }

    protected String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1);
            }
        }
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
