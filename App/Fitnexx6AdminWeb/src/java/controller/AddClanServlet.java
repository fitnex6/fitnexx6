package controller;

import dao.ClanDAO;
import entity.Staff;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

@WebServlet(name = "AddClanServlet", urlPatterns = {"/AddClanServlet"})
@MultipartConfig(maxFileSize = 16777215)
public class AddClanServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        try {
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            } else if (loggedIn.getStaffType() != 'M' && loggedIn.getStaffType() != 'E') {
                response.sendRedirect("home.jsp");
                return;
            }

            int adventureID = Integer.parseInt(request.getParameter("adventureID"));
            String clanName = request.getParameter("clanName");
            String description = request.getParameter("description");
            Part clanPicture = request.getPart("clanPicture");

            String fileName = getSubmittedFileName(clanPicture);
            String extension = fileName.substring(fileName.lastIndexOf("."));

            if (extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpg")) {
                InputStream inputStream = clanPicture.getInputStream();

                BufferedImage inputImage = ImageIO.read(inputStream);
                BufferedImage outputImage = new BufferedImage(350, 250, inputImage.getType());

                Graphics2D g2d = outputImage.createGraphics();
                g2d.drawImage(inputImage, 0, 0, 350, 250, null);
                g2d.dispose();

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                ImageIO.write(outputImage, "jpg", outStream);
                InputStream is = new ByteArrayInputStream(outStream.toByteArray());

                ClanDAO clanDAO = new ClanDAO();

                clanDAO.addClan(clanName, description, is, adventureID);
            } else {
                session.setAttribute("clanError", "Add Clan Failed! Please upload clan picture in .PNG or .JPG format only!");
            }

            response.sendRedirect("clan.jsp");
        } catch (Exception e) {
            if (e.getMessage().contains("Duplicate entry")) {
                session.setAttribute("clanError", "Add Clan Failed! Clan already exist!");
                response.sendRedirect("clan.jsp");
            } else {
                response.sendRedirect("home.jsp");
            }
        }
    }

    protected String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1);
            }
        }
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
