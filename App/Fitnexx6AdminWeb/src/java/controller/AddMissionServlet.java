package controller;

import dao.MissionDAO;
import entity.Staff;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AddMissionServlet", urlPatterns = {"/AddMissionServlet"})
public class AddMissionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            } else if (loggedIn.getStaffType() != 'M' && loggedIn.getStaffType() != 'E') {
                response.sendRedirect("home.jsp");
                return;
            }

            String missionType = request.getParameter("missionType");
            String description = request.getParameter("description");
            int points = Integer.parseInt(request.getParameter("points"));
            MissionDAO missionDAO = new MissionDAO();

            if (missionType.equalsIgnoreCase("Checkpoint")) {
                String[] checkpoints = request.getParameterValues("checkpoints");
                ArrayList<String> uniqueCheckpoints = new ArrayList<>();
                
                for(String checkpoint : checkpoints) {
                    if(!uniqueCheckpoints.contains(checkpoint)) {
                        uniqueCheckpoints.add(checkpoint);
                    }
                }
                
                int missionID = missionDAO.addMission(description, missionType, 0, points);
                missionDAO.addMissionCheckpoints(missionID, uniqueCheckpoints);
            } else if (missionType.equalsIgnoreCase("Steps")) {
                int steps = Integer.parseInt(request.getParameter("steps"));

                missionDAO.addMission(description, missionType, steps, points);
            } else {
                missionDAO.addMission(description, missionType, 0, points);
            }

            response.sendRedirect("mission.jsp");
        } catch (Exception e) {         
            response.sendRedirect("home.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
