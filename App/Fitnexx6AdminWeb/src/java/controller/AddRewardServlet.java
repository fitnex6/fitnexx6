package controller;

import dao.RewardDAO;
import entity.Staff;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

@WebServlet(name = "AddRewardServlet", urlPatterns = {"/AddRewardServlet"})
@MultipartConfig(maxFileSize = 16777215)
public class AddRewardServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            } else if (loggedIn.getStaffType() == 'A') {
                response.sendRedirect("home.jsp");
                return;
            }

            String description = request.getParameter("description");
            int price = Integer.parseInt(request.getParameter("price"));
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            boolean isActive = Boolean.parseBoolean(request.getParameter("isActive"));
            Part productPicture = request.getPart("productPicture");

            String fileName = getSubmittedFileName(productPicture);
            String extension = fileName.substring(fileName.lastIndexOf("."));

            if (extension.equalsIgnoreCase(".png") || extension.equalsIgnoreCase(".jpg")) {
                InputStream inputStream = productPicture.getInputStream();
                
                BufferedImage inputImage = ImageIO.read(inputStream);
                BufferedImage outputImage = new BufferedImage(350, 250, inputImage.getType());

                Graphics2D g2d = outputImage.createGraphics();
                g2d.drawImage(inputImage, 0, 0, 350, 250, null);
                g2d.dispose();

                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                ImageIO.write(outputImage, "jpg", outStream);
                InputStream is = new ByteArrayInputStream(outStream.toByteArray()); 

                RewardDAO rewardDAO = new RewardDAO();

                rewardDAO.addReward(description, is, price, quantity, 0, isActive);
            } else {
                session.setAttribute("rewardError", "Add Reward Failed! Please upload product picture in .PNG or .JPG format only!");
            }

            response.sendRedirect("reward.jsp");
        } catch (Exception e) {
            response.sendRedirect("home.jsp");
        }
    }

    protected String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1);
            }
        }
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
