package controller;

import dao.RedemptionStaffDAO;
import dao.StaffDAO;
import entity.Staff;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utility.Cryptography;
import utility.Email;
import utility.EmailValidator;

@WebServlet(name = "AddStaffServlet", urlPatterns = {"/AddStaffServlet"})
public class AddStaffServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        try {
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            } else if (loggedIn.getStaffType() != 'M' && loggedIn.getStaffType() != 'E') {
                response.sendRedirect("home.jsp");
                return;
            }

            final String email = request.getParameter("email");
            final String name = request.getParameter("name");
            final String password = Cryptography.generateRandString(8);
            final char staffType = request.getParameter("staffType").charAt(0);
            boolean isActive = Boolean.parseBoolean(request.getParameter("isActive"));

            if (!EmailValidator.validate(email)) {
                session.setAttribute("staffError", "Please enter a valid email address");
            } else {
                StaffDAO staffDAO = new StaffDAO();
                staffDAO.addStaff(email.toLowerCase(), name, password, staffType, isActive);

                // Setting up and sending of email takes alot of time, so do in separate thread
                // to not keep user waiting for response. Good idea?
                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String content = "Username: " + email.toLowerCase() + "\nPassword: " + password;

                            if (staffType == 'R') {
                                int staffPin = Integer.parseInt(Cryptography.generateStaffPin());
                                RedemptionStaffDAO redemptionStaffDAO = new RedemptionStaffDAO();
                                redemptionStaffDAO.addRedemptionStaff(staffPin, email.toLowerCase());
                                content += "\nRedemption Pin: " + staffPin;
                            }

                            Email.sendMail(email, "", "Your login details for Fitnexx Admin", content);
                        } catch (Exception e) {

                        }
                    }
                });
                
                t1.start();
            }

            response.sendRedirect("staff.jsp");
        } catch (Exception e) {
            if (e.getMessage().contains("Duplicate entry")) {
                session.setAttribute("staffError", "Email has already been taken!");
                response.sendRedirect("staff.jsp");
                return;
            }
            
            response.sendRedirect("home.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
