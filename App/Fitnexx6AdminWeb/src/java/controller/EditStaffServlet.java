package controller;

import dao.RedemptionStaffDAO;
import dao.StaffDAO;
import entity.Staff;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utility.Cryptography;
import utility.Email;

@WebServlet(name = "EditStaffServlet", urlPatterns = {"/EditStaffServlet"})
public class EditStaffServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        try {
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            } else if (loggedIn.getStaffType() != 'M' && loggedIn.getStaffType() != 'E') {
                response.sendRedirect("home.jsp");
                return;
            }

            final String email = request.getParameter("email");
            String name = request.getParameter("name");
            char staffType = request.getParameter("staffType").charAt(0);
            char oldStaffType = request.getParameter("oldStaffType").charAt(0);
            boolean isActive = Boolean.parseBoolean(request.getParameter("isActive"));

            StaffDAO staffDAO = new StaffDAO();
            final RedemptionStaffDAO redemptionStaffDAO = new RedemptionStaffDAO();
            staffDAO.editStaff(email.toLowerCase(), name, staffType, isActive);

            if (oldStaffType == 'R' && staffType != 'R') {
                redemptionStaffDAO.deleteRedemptionStaff(email);
            } else if (oldStaffType != 'R' && staffType == 'R') {
                final int staffPin = Integer.parseInt(Cryptography.generateStaffPin());
                redemptionStaffDAO.addRedemptionStaff(staffPin, email.toLowerCase());

                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String content = "Redemption Pin: " + staffPin;

                            Email.sendMail(email, "", "Your Redemption Pin for Fitnexx Admin", content);
                        } catch (Exception e) {

                        }
                    }
                });

                t1.start();

            }
            response.sendRedirect("staff.jsp");
        } catch (Exception e) {
            response.sendRedirect("home.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
