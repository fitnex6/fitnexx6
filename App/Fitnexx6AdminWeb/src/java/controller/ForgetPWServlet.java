package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Email;
import utility.EmailValidator;

@WebServlet(name = "ForgetPWServlet", urlPatterns = {"/ForgetPWServlet"})
public class ForgetPWServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            final String email = request.getParameter("email");
            boolean success = false;

            if (email != null && EmailValidator.validate(email)) {
                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // Email activationCode to user to activate account
                        String content = "Please click on the following link to reset your password! \n"
                                + "http://fypfitnexx-leroyce.rhcloud.com/Fitnexx6AdminWeb/ResetPWServlet?email=" + email.toLowerCase();
                        Email.sendMail(email, "", "Fitnexx6 Admin Account Recovery", content);
                    }
                });

                t1.start();

                success = true;
                request.setAttribute("forgetPWSuccessMsg", "A link to reset your password has been sent. Please check your email and follow the link provided.");
            }

            request.setAttribute("inputEmail", email);

            if (!success) {
                request.setAttribute("forgetPWErrorMsg", "Invalid email!");
            }
            
            RequestDispatcher view = request.getRequestDispatcher("forgetPW.jsp");
            view.forward(request, response);
        } catch (Exception e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
