package controller;

import dao.RedemptionStaffDAO;
import entity.Staff;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utility.Email;

@WebServlet(name = "ForgetStaffPinServlet", urlPatterns = {"/ForgetStaffPinServlet"})
public class ForgetStaffPinServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            } else if (loggedIn.getStaffType() != 'R') {
                response.sendRedirect("home.jsp");
                return;
            }

            final String email = request.getParameter("email");
            RedemptionStaffDAO redemptionStaffDAO = new RedemptionStaffDAO();
            final int staffPin = redemptionStaffDAO.getStaffPin(email);

            if (email != null && staffPin != -1) {
                Thread t1 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String content = "Here are your redemption details: \n"
                                + "Username: " + email.toLowerCase() + "\nRedemption Pin: " + staffPin;
                        Email.sendMail(email, "", "Your redemption details for Fitnexx6 Admin", content);
                    }
                });

                t1.start();
            }

            session.setAttribute("forgetStaffPinSuccess", "Please check your email for your redemption details");
            response.sendRedirect("changePW.jsp");
        } catch (Exception e) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
