package controller;

import entity.Staff;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

@WebServlet(name = "GenerateQRCodeServlet", urlPatterns = {"/GenerateQRCodeServlet"})
public class GenerateQRCodeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            }
            
            String checkpointIDQRText = request.getParameter("checkpointID");
            String missionType = request.getParameter("missionType");
            String missionIDQRText = request.getParameter("missionID");
            ByteArrayOutputStream out;           
            
            if(missionIDQRText != null) {
                out = QRCode.from("Special-" + missionIDQRText).to(ImageType.PNG).withSize(800, 800).stream();
            } else {
                out = QRCode.from(missionType + "-" + checkpointIDQRText).to(ImageType.PNG).withSize(800, 800).stream();
            }
            
            response.setContentType("image/png");
            response.setContentLength(out.size());

            OutputStream os = response.getOutputStream();
            os.write(out.toByteArray());

            os.flush();
            os.close();
        } catch (Exception e) {
            response.sendRedirect("home.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
