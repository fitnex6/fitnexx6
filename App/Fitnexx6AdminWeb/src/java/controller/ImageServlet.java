package controller;

import dao.ClanDAO;
import dao.NewsFeedDAO;
import dao.RewardDAO;
import entity.Clan;
import entity.NewsFeed;
import entity.Reward;
import entity.Staff;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ImageServlet", urlPatterns = {"/ImageServlet"})
public class ImageServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            Staff loggedIn = (Staff) session.getAttribute("loggedIn");

            if (loggedIn == null) {
                response.sendRedirect("index.jsp");
                return;
            }

            String dateTime = request.getParameter("dateTime");
            String email = request.getParameter("email");
            String clanName = request.getParameter("clanName");
            byte[] content = null;

            if (clanName != null) {
                int adventureID = Integer.parseInt(request.getParameter("adventureID"));

                ClanDAO clanDAO = new ClanDAO();
                Clan clan = clanDAO.getClan(clanName, adventureID);
                content = clan.getClanPicture();
            } else if (dateTime != null && email != null) {
                NewsFeedDAO newsFeedDAO = new NewsFeedDAO();
                NewsFeed newsFeed = newsFeedDAO.getNewsFeed(dateTime, email);
                content = newsFeed.getNewsPicture();
            } else {
                int rewardID = Integer.parseInt(request.getParameter("rewardID"));

                RewardDAO rewardDAO = new RewardDAO();
                Reward reward = rewardDAO.getReward(rewardID);
                content = reward.getProductPicture();
            }

            response.setContentType("image/png;image/jpeg");
            OutputStream o = response.getOutputStream();
            o.write(content);
            o.flush();
            o.close();
        } catch (Exception e) {
            response.sendRedirect("home.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
