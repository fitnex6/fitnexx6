package controller;

import dao.StaffDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utility.Cryptography;
import utility.Email;

@WebServlet(name = "ResetPWServlet", urlPatterns = {"/ResetPWServlet"})
public class ResetPWServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String message = null;

        try {
            final String email = request.getParameter("email");
            final String newPassword = Cryptography.generateRandString(8);
            StaffDAO staffDAO = new StaffDAO();
            staffDAO.updatePW(email, newPassword, null);

            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    // Email activationCode to user to activate account
                    String content = "You have successfully reset your password! Here are your login details: \n"
                            + "Username: " + email.toLowerCase() + "\nPassword: " + newPassword;
                    Email.sendMail(email, "", "Your new password for Fitnexx6 Admin", content);
                }
            });

            t1.start();

            message = "You have successfully reset your password. Please check your email for your new login details";
        } catch (Exception e) {
            message = "An error has occured. Please try again later";
        }

        HttpSession session = request.getSession();
        session.setAttribute("message", message);
        response.sendRedirect("resetPWSuccess.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
