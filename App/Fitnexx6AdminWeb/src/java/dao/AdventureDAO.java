package dao;

import entity.Adventure;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.TreeMap;

public class AdventureDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public TreeMap<Integer, Adventure> getAdventure() throws Exception {
        try {
            TreeMap<Integer, Adventure> adventureMap = new TreeMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM adventure";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int adventureID = rs.getInt("adventureID");
                String name = rs.getString("name");
                String description = rs.getString("description");
                int points = rs.getInt("points");
                Date startDate = rs.getDate("startDate");
                Date endDate = rs.getDate("endDate");
                boolean isActive = rs.getBoolean("isActive");

                Adventure adventure = new Adventure(adventureID, name, description, points, startDate, endDate, isActive);
                adventureMap.put(adventureID, adventure);
            }

            return adventureMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int addAdventure(String name, String description, int points, Date startDate, Date endDate, boolean isActive) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT MIN(startDate) AS 'startDate', MAX(endDate) AS 'endDate' FROM adventure";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            Date minDate = null;
            Date maxDate = null;

            while (rs.next()) {
                minDate = rs.getDate("startDate");
                maxDate = rs.getDate("endDate");
            }

            boolean valid = false;

            if (minDate == null && maxDate == null) {
                valid = true;
            } else if (startDate.after(maxDate) || endDate.before(minDate)) {
                valid = true;
            }

            int adventureID = 0;

            if (valid) {
                sql = "INSERT INTO adventure (name, description, points, startDate, endDate, isActive) VALUES (?, ?, ?, ?, ?, ?)";
                stmt = conn.prepareStatement(sql);

                stmt.setString(1, name);
                stmt.setString(2, description);
                stmt.setInt(3, points);
                stmt.setTimestamp(4, new Timestamp(startDate.getTime()));
                stmt.setTimestamp(5, new Timestamp(endDate.getTime()));
                stmt.setBoolean(6, isActive);

                stmt.executeUpdate();

                sql = "SELECT adventureID FROM adventure ORDER BY adventureID DESC LIMIT 1";
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    adventureID = rs.getInt("adventureID");
                }
            }

            return adventureID;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void editAdventure(int adventureID, String name, String description, int points, Date startDate, Date endDate) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            sql = "UPDATE adventure SET name = ?, description = ?, points = ?, startDate = ?, endDate = ? "
                    + "WHERE adventureID = ?";

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, name);
            stmt.setString(2, description);
            stmt.setInt(3, points);
            stmt.setTimestamp(4, new Timestamp(startDate.getTime()));
            stmt.setTimestamp(5, new Timestamp(endDate.getTime()));
            stmt.setInt(6, adventureID);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void deleteAdventure(int adventureID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM adventure WHERE adventureID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, adventureID);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void activateAdventure(int adventureID, boolean active) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "UPDATE adventure SET isActive = ? WHERE adventureID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setBoolean(1, active);
            stmt.setInt(2, adventureID);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
}
