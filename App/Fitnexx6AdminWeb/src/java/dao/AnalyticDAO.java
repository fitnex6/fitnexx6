package dao;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import entity.AnalyticData;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author jeremy.seow.2014
 */
public class AnalyticDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public int[] getDashBoard() throws Exception {
        try {
            int totalUsers = 0;
            int totalSteps = 0;
            int totalMissionsCompleted = 0;
            int totalFeedback = 0;

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT COUNT(*) FROM user";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            if (rs.next()) {
                totalUsers = rs.getInt("count(*)");
            }

            sql = "SELECT SUM(steps) FROM steps_history";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            if (rs.next()) {
                totalSteps = rs.getInt("sum(steps)");
            }

            sql = "SELECT COUNT(*) FROM accepted_missions WHERE isCompleted = true";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            if (rs.next()) {
                totalMissionsCompleted = rs.getInt("count(*)");
            }

            sql = "SELECT COUNT(*) FROM feedback_history";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            if (rs.next()) {
                totalFeedback = rs.getInt("count(*)");
            }

            return new int[]{totalUsers, totalSteps, totalMissionsCompleted, totalFeedback};
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public AnalyticData getMostPopularRoutes() throws Exception {
        try {
            JsonArray values = new JsonArray();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT b.latitude, b.longitude, COUNT(*) FROM  checkpoint cp "
                    + "INNER JOIN scan s ON cp.checkpointID = s.checkpointID "
                    + "INNER JOIN building b ON cp.bid = b.bid "
                    + "GROUP BY b.bid";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                JsonObject temp = new JsonObject();
                temp.addProperty("lat", rs.getDouble("b.latitude"));
                temp.addProperty("lng", rs.getDouble("b.longitude"));
                temp.addProperty("count", rs.getDouble("COUNT(*)"));
                values.add(temp);
            }

            AnalyticData aData = new AnalyticData(values);
            return aData;

        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }

    }

    public AnalyticData getActiveUsers() throws Exception {
        try {
            JsonArray labels = new JsonArray();
            JsonArray valuesUser = new JsonArray();
            JsonArray valuesMission = new JsonArray();
            ArrayList<JsonArray> listOfValues = new ArrayList<JsonArray>();
            //HashMap<String, Integer[]> range = new HashMap<String, Integer[]>();
            JsonObject adventures = new JsonObject();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * "
                    + "FROM (SELECT DATE( last_active_date ) AS dateRecorded, NULL AS ad_name, COUNT( * ) AS user_count, mission_count "
                    + "FROM user u LEFT OUTER JOIN ("
                    + "SELECT DATE( dateTime_completed ) AS dateCompleted, COUNT( * ) AS mission_count "
                    + "FROM accepted_missions "
                    + "GROUP BY DATE( dateTime_completed )) AS temp ON DATE( last_active_date ) = temp.dateCompleted "
                    + "GROUP BY DATE( last_active_date ) "
                    + "HAVING dateRecorded IS NOT NULL "
                    + "UNION "
                    + "SELECT DATE( dateTime_completed ) AS dateRecorded, NULL , user_count, COUNT( * ) AS mission_count "
                    + "FROM accepted_missions am LEFT OUTER JOIN (SELECT DATE( last_active_date ) AS dateCompleted, COUNT( * ) AS user_count "
                    + "FROM user "
                    + "GROUP BY DATE( last_active_date )) AS temp ON DATE( dateTime_completed ) = temp.dateCompleted "
                    + "GROUP BY DATE( dateTime_completed ) "
                    + "HAVING dateRecorded IS NOT NULL "
                    + "UNION "
                    + "SELECT startDate, name, NULL , NULL FROM adventure "
                    + "UNION "
                    + "SELECT endDate, name, NULL , NULL FROM adventure) AS result "
                    + "ORDER BY dateRecorded, ad_name ASC";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String startDate = rs.getDate("dateRecorded").toString();
                String adName = rs.getString("ad_name");
                int userCount = rs.getInt("user_count");
                int missionCount = rs.getInt("mission_count");

                if (!labels.contains(new JsonPrimitive(startDate))) {
                    labels.add(new JsonPrimitive(startDate));
                    valuesUser.add(new JsonPrimitive(userCount));
                    valuesMission.add(new JsonPrimitive(missionCount));
                }
                //System.out.println(startDate);
                if (adName != null) {
                    JsonObject temp = adventures.getAsJsonObject(adName);
                    JsonObject range = new JsonObject();

                    if (temp == null) {
                        range.addProperty("startIndex", labels.size() - 1);
                        range.addProperty("endIndex", labels.size() - 1);
                        adventures.add(adName, range);
                    } else {
                        range.addProperty("startIndex", temp.get("startIndex").getAsInt());
                        range.addProperty("endIndex", labels.size() - 1);
                        adventures.add(adName, range);
                    }
                }
            }
            listOfValues.add(valuesUser);
            listOfValues.add(valuesMission);

            AnalyticData aData = new AnalyticData(labels, listOfValues, adventures);
            return aData;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
    
    public AnalyticData getBmiRange() throws Exception{
        try{
            JsonArray labels = new JsonArray();
            JsonArray valuesMax = new JsonArray();
            JsonArray valuesAvg = new JsonArray();
            JsonArray valuesMin = new JsonArray();
            ArrayList<JsonArray> listOfValues = new ArrayList<JsonArray>();
            
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT DATE_FORMAT(dateTime,'%Y-%M'), MAX(bmi), AVG(bmi), MIN(bmi) "
                    + "FROM fitness_history " 
                    + "WHERE bmi <= 30 " 
                    + "AND bmi >= 15 " 
                    + "GROUP BY YEAR(dateTime), MONTH(dateTime) ";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while(rs.next()){
                labels.add(new JsonPrimitive(rs.getString("DATE_FORMAT(dateTime,'%Y-%M')")));
                valuesMax.add(new JsonPrimitive(rs.getDouble("MAX(bmi)")));
                valuesAvg.add(new JsonPrimitive(Math.round(rs.getDouble("AVG(bmi)")*100.0)/100.0));
                valuesMin.add(new JsonPrimitive(rs.getDouble("MIN(bmi)")));
            }
            listOfValues.add(valuesMax);
            listOfValues.add(valuesAvg);
            listOfValues.add(valuesMin);
            
            AnalyticData aData = new AnalyticData(labels, listOfValues);
            return aData;
        }finally{
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public AnalyticData getHourlyLoginBreakdown() throws Exception {
        try {
            JsonArray labels = new JsonArray();
            JsonArray values = new JsonArray();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT Hour(dateTime) as `Hour` ,count(*) AS `loginCount` FROM `steps_history`\n"
                    + "GROUP BY `Hour`";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                labels.add(new JsonPrimitive(rs.getInt("hour")));
                values.add(new JsonPrimitive(rs.getInt("loginCount")));
            }
            AnalyticData aData = new AnalyticData(labels, values);
            return aData;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public AnalyticData getAvgStepsBreakdownByGender() throws Exception {
        try {
            JsonArray labels = new JsonArray();
            JsonArray values = new JsonArray();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT gender,avg(steps) FROM `steps_history` inner join `user` on `steps_history`.email=`user`.email where gender IS NOT NULL GROUP BY gender";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                labels.add(new JsonPrimitive(rs.getString("gender")));
                values.add(new JsonPrimitive(rs.getInt("avg(steps)")));
            }
            AnalyticData aData = new AnalyticData(labels, values);
            return aData;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public AnalyticData getAvgStepsBreakdownByFaculty() throws Exception {
        try {
            JsonArray labels = new JsonArray();
            JsonArray values = new JsonArray();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT faculty,avg(steps) FROM `steps_history` inner join `user` on `steps_history`.email=`user`.email GROUP BY faculty";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                labels.add(new JsonPrimitive(rs.getString("faculty")));
                values.add(new JsonPrimitive(rs.getInt("avg(steps)")));
            }
            AnalyticData aData = new AnalyticData(labels, values);
            return aData;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public AnalyticData getAvgStepsBreakdownByTier() throws Exception {
        try {
            JsonArray labels = new JsonArray();
            JsonArray values = new JsonArray();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT tier,avg(steps) FROM `steps_history` inner join `user` on `steps_history`.email=`user`.email GROUP BY tier ORDER BY FIELD(tier,'Beginner','Bronze','Silver','Gold','Platinum')";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                labels.add(new JsonPrimitive(rs.getString("tier")));
                values.add(new JsonPrimitive(rs.getInt("avg(steps)")));
            }
            AnalyticData aData = new AnalyticData(labels, values);
            return aData;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int getPastWeekStepsCount() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT sum(steps) FROM `steps_history` where DATE(`dateTime`) BETWEEN DATE_SUB(curDate(),INTERVAL 1 WEEK) and curdate()";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            int result = 0;
            while (rs.next()) {
                result = rs.getInt("sum(steps)");

            }
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int getTotalStepCount() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT sum(steps) FROM `steps_history`";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            int result = 0;
            while (rs.next()) {
                result = rs.getInt("sum(steps)");

            }
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int getAvgStepCount() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT avg(steps) FROM `steps_history`";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            int result = 0;
            while (rs.next()) {
                result = rs.getInt("avg(steps)");

            }
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int getHighestStepCount() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT max(Steps) FROM `steps_history`";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            int result = 0;
            while (rs.next()) {
                result = rs.getInt("max(steps)");

            }
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int getPast7DaysRedemptionCount() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT count(*) as `Redemption_Count` FROM `redemption` WHERE DATE(`dateTime`) BETWEEN DATE_SUB(curDate(),INTERVAL 1 WEEK) and curdate() ORDER BY `dateTime` DESC  ";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            int result = 0;
            while (rs.next()) {
                result = rs.getInt("Redemption_Count");

            }
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int getTotalRedemptionCount() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT count(*) as `Redemption_Count` FROM `redemption`";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            int result = 0;
            while (rs.next()) {
                result = rs.getInt("Redemption_Count");

            }
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public double getAvgRedemptionCount() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "select avg(`count`) `avg_count` from (SELECT `email`,count(*) AS `count` FROM `redemption` group by `email`) AS `redemption_count`";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            double result = 0;
            while (rs.next()) {
                result = rs.getDouble("avg_count");

            }
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int getUniqueUserRedemptionCount() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "select count(`count`) `user_count` from (SELECT `email`,count(*) AS `count` FROM `redemption` group by `email`) AS `redemption_count`";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            int result = 0;
            while (rs.next()) {
                result = rs.getInt("user_count");

            }
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

}
