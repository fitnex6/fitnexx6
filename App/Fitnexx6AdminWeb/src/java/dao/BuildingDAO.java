package dao;

import entity.Building;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.TreeMap;

public class BuildingDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public TreeMap<Integer, Building> getBuilding() throws Exception {
        try {
            TreeMap<Integer, Building> result = new TreeMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM building";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int buildingID = rs.getInt("bid");
                String name = rs.getString("name");
                double latitude = rs.getDouble("latitude");
                double longitude = rs.getDouble("longitude");
                
                Building b = new Building(buildingID, name, latitude, longitude);
                result.put(buildingID, b);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void addBuilding(String name, double latitude, double longitude) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO building (name, latitude, longitude) VALUES (?, ?, ?)";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, name);
            stmt.setDouble(2, latitude);
            stmt.setDouble(3, longitude);
            
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void editBuilding(int buildingID, String name, double latitude, double longitude) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            sql = "UPDATE building SET name = ?, latitude = ?, longitude = ? "
                    + "WHERE bid = ?";

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, name);
            stmt.setDouble(2, latitude);
            stmt.setDouble(3, longitude);
            stmt.setInt(4, buildingID);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void deleteBuilding(int buildingID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM building WHERE bid = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, buildingID);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
}
