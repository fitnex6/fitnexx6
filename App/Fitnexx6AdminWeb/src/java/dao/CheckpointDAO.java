package dao;

import entity.Checkpoint;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CheckpointDAO {
    
    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;
    
    public ArrayList<Checkpoint> getCheckpoint() throws Exception {
        try {
            ArrayList<Checkpoint> result = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM checkpoint";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int checkpointID = rs.getInt("checkpointID");
                String locationDetails = rs.getString("location_details");
                int buildingID = rs.getInt("bid");
                int missionID = rs.getInt("missionID");

                Checkpoint c = new Checkpoint(checkpointID, locationDetails, buildingID, missionID);
                result.add(c);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void addCheckpoint(String locationDetails, int buildingID, int missionID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO checkpoint (location_details, bid, missionID) VALUES (?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, locationDetails);
            stmt.setInt(2, buildingID);
            stmt.setInt(3, missionID);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void editCheckpoint(int checkpointID, String locationDetails, int buildingID, int missionID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            sql = "UPDATE checkpoint SET location_details = ?, bid = ?, missionID = ? "
                    + "WHERE checkpointID = ?";

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, locationDetails);
            stmt.setInt(2, buildingID);
            stmt.setInt(3, missionID);
            stmt.setInt(4, checkpointID);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void deleteCheckpoint(int checkpointID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM checkpoint WHERE checkpointID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkpointID);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
