package dao;

import entity.Clan;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ClanDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public Clan getClan(String inputClanName, int inputAdventureID) throws Exception {
        try {
            Clan clan = null;
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM clan WHERE clan_name = ? AND adventureID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, inputClanName);
            stmt.setInt(2, inputAdventureID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String clanName = rs.getString("clan_name");
                String description = rs.getString("description");
                byte[] clanPicture = rs.getBytes("clan_picture");
                int adventureID = rs.getInt("adventureID");

                clan = new Clan(clanName, description, clanPicture, adventureID);
            }

            return clan;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<Clan> getClan() throws Exception {
        try {
            ArrayList<Clan> clanList = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM clan";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String clanName = rs.getString("clan_name");
                String description = rs.getString("description");
                byte[] clanPicture = rs.getBytes("clan_picture");
                int adventureID = rs.getInt("adventureID");

                Clan clan = new Clan(clanName, description, clanPicture, adventureID);
                clanList.add(clan);
            }

            return clanList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<Clan> getClan(int inputAdventureID) throws Exception {
        try {
            ArrayList<Clan> clanList = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM clan WHERE adventureID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, inputAdventureID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String clanName = rs.getString("clan_name");
                String description = rs.getString("description");
                byte[] clanPicture = rs.getBytes("clan_picture");
                int adventureID = rs.getInt("adventureID");

                Clan clan = new Clan(clanName, description, clanPicture, adventureID);
                clanList.add(clan);
            }

            return clanList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void addClan(String clanName, String description, InputStream clanPicture, int adventureID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO clan (clan_name, description, clan_picture, adventureID) VALUES (?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, clanName);
            stmt.setString(2, description);
            stmt.setBlob(3, clanPicture);
            stmt.setInt(4, adventureID);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void editClan(int oldAdventureID, String oldClanName, String clanName, String description, InputStream clanPicture, int adventureID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            if (clanPicture != null) {
                sql = "UPDATE clan SET clan_name = ?, "
                        + "description = ?, adventureID = ?, clan_picture = ? "
                        + "WHERE clan_name = ? AND adventureID = ?";
            } else {
                sql = "UPDATE clan SET clan_name = ?, "
                        + "description = ?, adventureID = ? "
                        + "WHERE clan_name = ? AND adventureID = ?";
            }
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, clanName);
            stmt.setString(2, description);
            stmt.setInt(3, adventureID);

            if (clanPicture != null) {
                stmt.setBlob(4, clanPicture);
                stmt.setString(5, oldClanName);
                stmt.setInt(6, oldAdventureID);
            } else {
                stmt.setString(4, oldClanName);
                stmt.setInt(5, oldAdventureID);
            }

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void deleteClan(int adventureID, String clanName) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM clan WHERE adventureID = ? AND clan_name = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, adventureID);
            stmt.setString(2, clanName);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public HashMap<String, Integer> getClanStats() throws Exception {
        try {
            HashMap<String, Integer> resultMap = new HashMap<>();
            HashMap<String, String> userMap = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();

            sql = "SELECT * FROM adventure WHERE isActive = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setBoolean(1, true);
            rs = stmt.executeQuery();
            Date startDate = null;
            Date endDate = null;
            int adventureID = 0;

            while (rs.next()) {
                startDate = rs.getDate("startDate");
                endDate = rs.getDate("endDate");
                adventureID = rs.getInt("adventureID");
            }

            if (adventureID != 0) {
                sql = "SELECT DISTINCT am.email, clan_name FROM accepted_missions am "
                        + "INNER JOIN user u ON am.email = u.email "
                        + "WHERE CAST(dateTime_accepted AS date) BETWEEN ? AND ? "
                        + "AND CAST(dateTime_completed AS date) BETWEEN ? AND ? "
                        + "AND isCompleted = ? AND clan_name IS NOT NULL";
                stmt = conn.prepareStatement(sql);
                stmt.setDate(1, new java.sql.Date(startDate.getTime()));
                stmt.setDate(2, new java.sql.Date(endDate.getTime()));
                stmt.setDate(3, new java.sql.Date(startDate.getTime()));
                stmt.setDate(4, new java.sql.Date(endDate.getTime()));
                stmt.setBoolean(5, true);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    String email = rs.getString("email");
                    String clanName = rs.getString("clan_name");

                    userMap.put(email, clanName);
                }

                for (String user : userMap.keySet()) {
                    sql = "SELECT count(*) AS count FROM accepted_missions "
                            + "WHERE CAST(dateTime_accepted AS date) BETWEEN ? AND ? "
                            + "AND CAST(dateTime_completed AS date) BETWEEN ? AND ? "
                            + "AND isCompleted = ? "
                            + "AND email = ?";
                    stmt = conn.prepareStatement(sql);
                    stmt.setDate(1, new java.sql.Date(startDate.getTime()));
                    stmt.setDate(2, new java.sql.Date(endDate.getTime()));
                    stmt.setDate(3, new java.sql.Date(startDate.getTime()));
                    stmt.setDate(4, new java.sql.Date(endDate.getTime()));
                    stmt.setBoolean(5, true);
                    stmt.setString(6, user);
                    rs = stmt.executeQuery();

                    while (rs.next()) {
                        int count = rs.getInt("count");

                        if (resultMap.get(userMap.get(user)) == null) {
                            resultMap.put(userMap.get(user), count);
                        } else {
                            int currentCount = resultMap.get(userMap.get(user));
                            resultMap.put(userMap.get(user), count + currentCount);
                        }
                    }
                }

                sql = "SELECT clan_name FROM clan "
                        + "WHERE adventureID = ? ";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, adventureID);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    String clanName = rs.getString("clan_name");

                    if (!resultMap.containsKey(clanName)) {
                        resultMap.put(clanName, 0);
                    }
                }
            }

            return resultMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void awardPoints(int points, String clan) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "UPDATE user SET points = points + ? WHERE clan_name = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, points);
            stmt.setString(2, clan);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void clearClans() throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "UPDATE user SET clan_name = null";
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
}
