package dao;

import entity.Feedback;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

public class FeedbackDAO {
    
    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;
   
    public ArrayList<Feedback> getFeedback() throws Exception {
        try {
            ArrayList<Feedback> result = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM feedback_history";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Date dateTime = new java.util.Date(rs.getTimestamp("dateTime").getTime());
                String area_of_concern = rs.getString("area_of_concern");
                String feedback = rs.getString("feedback");
                String email = rs.getString("email");

                Feedback fb = new Feedback(dateTime,area_of_concern,feedback,email);
                result.add(fb);
            }
            
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }    
}
