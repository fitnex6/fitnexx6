package dao;

import entity.Mission;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class MissionDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public TreeMap<Integer, Mission> getMission() throws Exception {
        try {
            TreeMap<Integer, Mission> missionMap = new TreeMap<>();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM mission";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Mission mission = new Mission(rs.getInt("missionID"), rs.getString("description"), rs.getString("mission_type"),
                        rs.getInt("steps"), rs.getInt("points"));

                missionMap.put(rs.getInt("missionID"), mission);
            }

            return missionMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public HashMap<Integer, String> getMissionCheckpoints() throws Exception {
        try {
            ArrayList<Integer> missionIDList = new ArrayList<>();
            HashMap<Integer, String> resultMap = new HashMap<>();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT DISTINCT missionID FROM mission_checkpoints";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int missionID = rs.getInt("missionID");

                missionIDList.add(missionID);
            }

            for (Integer missionID : missionIDList) {
                sql = "SELECT * FROM mission_checkpoints WHERE missionID = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, missionID);
                rs = stmt.executeQuery();
                String checkpointIDs = "";

                while (rs.next()) {
                    checkpointIDs += rs.getInt("checkpointID") + ",";
                }

                resultMap.put(missionID, checkpointIDs.substring(0, checkpointIDs.length() - 1));
            }

            return resultMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int addMission(String description, String missionType, int steps, int points) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO mission (description, mission_type, steps, points) VALUES (?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, description);
            stmt.setString(2, missionType);

            if (steps == 0) {
                stmt.setNull(3, java.sql.Types.INTEGER);
            } else {
                stmt.setInt(3, steps);
            }
            stmt.setInt(4, points);

            stmt.executeUpdate();

            int missionID = 0;

            if (missionType.equalsIgnoreCase("Checkpoint")) {
                sql = "SELECT missionID FROM mission ORDER BY missionID DESC LIMIT 1";
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    missionID = rs.getInt("missionID");
                }
            }

            return missionID;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void addMissionCheckpoints(int missionID, ArrayList<String> checkpointIDArr) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO mission_checkpoints (missionID, checkpointID) VALUES (?, ?)";
            stmt = conn.prepareStatement(sql);

            for (String checkpointID : checkpointIDArr) {
                stmt.setInt(1, missionID);
                stmt.setInt(2, Integer.parseInt(checkpointID));

                stmt.executeUpdate();
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void editMission(int missionID, String description, int steps, int points) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            sql = "UPDATE mission SET description = ?, steps = ?, points = ? "
                    + "WHERE missionID = ?";

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, description);

            if (steps == 0) {
                stmt.setNull(2, java.sql.Types.INTEGER);
            } else {
                stmt.setInt(2, steps);
            }

            stmt.setInt(3, points);
            stmt.setInt(4, missionID);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void editMissionCheckpoints(int missionID, ArrayList<String> checkpointIDArr) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM mission_checkpoints WHERE missionID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, missionID);
            stmt.executeUpdate();

            sql = "INSERT INTO mission_checkpoints (missionID, checkpointID) VALUES (?, ?)";
            stmt = conn.prepareStatement(sql);

            for (String checkpointID : checkpointIDArr) {
                stmt.setInt(1, missionID);
                stmt.setInt(2, Integer.parseInt(checkpointID));

                stmt.executeUpdate();
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void deleteMission(int missionID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM mission WHERE missionID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, missionID);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
}
