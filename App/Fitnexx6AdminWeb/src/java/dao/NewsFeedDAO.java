package dao;

import entity.NewsFeed;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class NewsFeedDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public NewsFeed getNewsFeed(String inputDateTime, String inputEmail) throws Exception {
        try {
            NewsFeed newsFeed = null;
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM newsfeed where dateTime = ? AND email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, inputDateTime);
            stmt.setString(2, inputEmail);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Date dateTime = new java.util.Date(rs.getTimestamp("dateTime").getTime());
                String email = rs.getString("email");
                String subject = rs.getString("subject");
                String content = rs.getString("content");
                byte[] picture = rs.getBytes("news_picture");
                String category = rs.getString("category");

                newsFeed = new NewsFeed(dateTime, email, subject, content, picture, category);
            }

            return newsFeed;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<NewsFeed> getNewsFeed() throws Exception {
        try {
            ArrayList<NewsFeed> result = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM newsfeed ORDER BY dateTime DESC";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                Date dateTime = new java.util.Date(rs.getTimestamp("dateTime").getTime());
                String email = rs.getString("email");
                String subject = rs.getString("subject");
                String content = rs.getString("content");
                byte[] picture = rs.getBytes("news_picture");
                String category = rs.getString("category");

                NewsFeed newsFeed = new NewsFeed(dateTime, email, subject, content, picture, category);
                result.add(newsFeed);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void addNewsFeed(String email, String subject, String content, InputStream newsPicture, String category) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT into newsfeed VALUES(?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(new Date().getTime()));
            stmt.setString(2, email);
            stmt.setString(3, subject);
            stmt.setString(4, content);
            stmt.setBlob(5, newsPicture);
            stmt.setString(6, category);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void editNewsFeed(String dateTime, String oldEmail, String email, String subject, String content, InputStream newsPicture, String category) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            if (newsPicture != null) {
                sql = "UPDATE newsfeed SET dateTime = ?, "
                        + "email = ?, subject = ?, content = ?, category = ?, news_picture = ? "
                        + "WHERE dateTime = ? AND email = ?";
            } else {
                sql = "UPDATE newsfeed SET dateTime = ?, "
                        + "email = ?, subject = ?, content = ?, category = ? "
                        + "WHERE dateTime = ? AND email = ?";
            }

            stmt = conn.prepareStatement(sql);

            stmt.setTimestamp(1, new Timestamp(new Date().getTime()));
            stmt.setString(2, email);
            stmt.setString(3, subject);
            stmt.setString(4, content);
            stmt.setString(5, category);

            if (newsPicture != null) {
                stmt.setBlob(6, newsPicture);
                stmt.setString(7, dateTime);
                stmt.setString(8, oldEmail);
            } else {
                stmt.setString(6, dateTime);
                stmt.setString(7, oldEmail);
            }

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void deleteNewsFeed(String dateTime, String email) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM newsfeed WHERE dateTime = ? AND email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, dateTime);
            stmt.setString(2, email);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
}
