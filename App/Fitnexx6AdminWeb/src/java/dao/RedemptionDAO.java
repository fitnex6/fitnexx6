package dao;

import entity.Redemption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

public class RedemptionDAO {
    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;
    
    public ArrayList<Redemption> getRedemption() throws Exception {
        try {
            ArrayList<Redemption> result = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM redemption";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int transactionID = rs.getInt("transaction_id");
                Date dateTime = new java.util.Date(rs.getTimestamp("dateTime").getTime());
                String email = rs.getString("email");
                int rewardID = rs.getInt("rewardID");
                int staffPin = rs.getInt("staff_pin");
               
                Redemption r = new Redemption(transactionID, dateTime, email, rewardID, staffPin);
                result.add(r);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }   
}
