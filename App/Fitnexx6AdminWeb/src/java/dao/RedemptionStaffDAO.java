package dao;

import entity.RedemptionStaff;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.TreeMap;

public class RedemptionStaffDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public TreeMap<Integer, RedemptionStaff> getRedemptionStaff() throws Exception {
        try {
            TreeMap<Integer, RedemptionStaff> result = new TreeMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM redemption_staff";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int staffPin = rs.getInt("staff_pin");
                String email = rs.getString("email");

                RedemptionStaff r = new RedemptionStaff(staffPin, email);
                result.put(staffPin, r);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void addRedemptionStaff(int staffPin, String email) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO redemption_staff (staff_pin, email) VALUES (?, ?)";
            stmt = conn.prepareStatement(sql);

            stmt.setInt(1, staffPin);
            stmt.setString(2, email);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void deleteRedemptionStaff(String email) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM redemption_staff WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
    
    public int getStaffPin(String email) throws Exception {
        try {
            int staffPin = -1;
            conn = ConnectionPoolManager.getInstance().getConnection();         
            sql = "SELECT staff_pin FROM redemption_staff where email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            rs = stmt.executeQuery();

            while (rs.next()) {
                staffPin = rs.getInt("staff_pin");
            }

            return staffPin;
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
}
