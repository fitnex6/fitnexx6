package dao;

import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import entity.AnalyticData;
import entity.Reward;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class RewardDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public Reward getReward(int inputRewardID) throws Exception {
        try {
            Reward reward = null;
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM reward where rewardID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, inputRewardID);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int rewardID = rs.getInt("rewardID");
                String description = rs.getString("description");
                byte[] productPicture = rs.getBytes("product_picture");
                int price = rs.getInt("price");
                int qty = rs.getInt("qty");
                int qtyClaimed = rs.getInt("qty_claimed");
                boolean isActive = rs.getBoolean("isActive");

                reward = new Reward(rewardID, description, productPicture, price, qty, qtyClaimed, isActive);
            }

            return reward;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public TreeMap<Integer, Reward> getReward() throws Exception {
        try {
            TreeMap<Integer, Reward> result = new TreeMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM reward";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int rewardID = rs.getInt("rewardID");
                String description = rs.getString("description");
                byte[] productPicture = rs.getBytes("product_picture");
                int price = rs.getInt("price");
                int qty = rs.getInt("qty");
                int qtyClaimed = rs.getInt("qty_claimed");
                boolean isActive = rs.getBoolean("isActive");

                Reward r = new Reward(rewardID, description, productPicture, price, qty, qtyClaimed, isActive);
                result.put(rewardID, r);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public AnalyticData getRewardDecriptions() throws Exception {
        try {
            JsonArray labels = new JsonArray();
            JsonArray values = new JsonArray();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT `description`,`qty_claimed` FROM `reward` WHERE 1";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                labels.add(new JsonPrimitive(rs.getString("description")));
                values.add(new JsonPrimitive(rs.getInt("qty_claimed")));
            }
            AnalyticData aData = new AnalyticData(labels, values);
            return aData;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public HashMap<String, AnalyticData> getRewardsBreakdownByFaculty() throws Exception {

        HashMap<String, AnalyticData> dataHash = new HashMap<>();

        try {
            JsonArray labels;
            JsonArray values;

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "select `reward`.rewardID,`reward`.description,faculty, Count(*) AS facultyCount FROM `redemption` INNER JOIN `user` ON `redemption`.email = `user`.email INNER JOIN `reward` on `redemption`.rewardID=`reward`.rewardID \n"
                    + "Group BY `user`.faculty,`reward`.rewardID\n"
                    + "ORDER BY `reward`.`rewardID` ASC";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
//                labels.add(new JsonPrimitive(rs.getString("faculty")));
//                values.add(new JsonPrimitive(rs.getInt("qty_claimed")));
                int tempRewardID = rs.getInt("rewardID");
                String tempDescription = rs.getString("description");
                String tempFaculty = rs.getString("faculty");
                int count = rs.getInt("facultyCount");

                //dataHash does not contain key yet
                if (dataHash.containsKey(tempDescription)) {
                    AnalyticData tempAD = dataHash.get(tempDescription);
                    tempAD.addLabel(tempFaculty);
                    tempAD.addValues(count);
                } else {
                    labels = new JsonArray();
                    values = new JsonArray();
                    AnalyticData newAD = new AnalyticData(labels, values);
                    newAD.addLabel(tempFaculty);
                    newAD.addValues(count);
                    dataHash.put(tempDescription, newAD);
                }
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }

        return dataHash;
    }

    public void addReward(String description, InputStream productPicture, int price, int qty, int qtyClaimed, boolean isActive) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO reward (description, product_picture, price, qty, qty_claimed, isActive) VALUES (?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, description);
            stmt.setBlob(2, productPicture);
            stmt.setInt(3, price);
            stmt.setInt(4, qty);
            stmt.setInt(5, qtyClaimed);
            stmt.setBoolean(6, isActive);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void editReward(int rewardID, String description, InputStream productPicture, int price, int qty, boolean isActive) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            if (productPicture != null) {
                sql = "UPDATE reward SET description = ?, "
                        + "isActive = ?, price = ?, qty = ?, product_picture = ? "
                        + "WHERE rewardID = ?";
            } else {
                sql = "UPDATE reward SET description = ?, "
                        + "isActive = ?, price = ?, qty = ? "
                        + "WHERE rewardID = ?";
            }
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, description);
            stmt.setBoolean(2, isActive);
            stmt.setInt(3, price);
            stmt.setInt(4, qty);

            if (productPicture != null) {
                stmt.setBlob(5, productPicture);
                stmt.setInt(6, rewardID);
            } else {
                stmt.setInt(5, rewardID);
            }

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void deleteReward(int rewardID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM reward WHERE rewardID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, rewardID);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
}
