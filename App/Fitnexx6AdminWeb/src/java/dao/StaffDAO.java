package dao;

import entity.Staff;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class StaffDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public ArrayList<Staff> getStaff() throws Exception {
        try {
            ArrayList<Staff> staffList = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM staff WHERE email NOT IN (?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, "admin");
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");
                String name = rs.getString("name");
                char staffType = rs.getString("staff_type").charAt(0);
                boolean isActive = rs.getBoolean("isActive");

                Staff staff = new Staff(email, name, staffType, isActive);
                staffList.add(staff);
            }

            return staffList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public Staff getStaff(String inputEmail, String inputPassword) throws Exception {
        try {
            Staff s = null;
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM staff WHERE email = ? AND password = ? AND isActive = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, inputEmail);
            stmt.setString(2, inputPassword);
            stmt.setBoolean(3, true);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");
                String name = rs.getString("name");
                char staffType = rs.getString("staff_type").charAt(0);
                boolean isActive = rs.getBoolean("isActive");

                s = new Staff(email, name, staffType, isActive);
            }

            return s;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void addStaff(String email, String name, String password, char staffType, boolean isActive) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO staff (email, name, password, staff_type, isActive) VALUES (?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);

            stmt.setString(1, email);
            stmt.setString(2, name);
            stmt.setString(3, password);
            stmt.setString(4, Character.toString(staffType));
            stmt.setBoolean(5, isActive);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void updatePW(String email, String newPassword, String oldPassword) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            
            if (oldPassword == null) {
                sql = "UPDATE staff SET password = ? WHERE email = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, newPassword);
                stmt.setString(2, email);
            } else {
                sql = "UPDATE staff SET password = ? WHERE email = ? AND password = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, newPassword);
                stmt.setString(2, email);
                stmt.setString(3, oldPassword);
            }

            int count = stmt.executeUpdate();

            if (count == 0) {
                throw new Exception("Old password must match the current password");
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void editStaff(String email, String name, char staffType, boolean isActive) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            sql = "UPDATE staff SET name = ?, staff_type = ?, isActive = ? "
                    + "WHERE email = ?";

            stmt = conn.prepareStatement(sql);

            stmt.setString(1, name);
            stmt.setString(2, Character.toString(staffType));
            stmt.setBoolean(3, isActive);
            stmt.setString(4, email);

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
    
    public void deleteStaff(String email) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM staff WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
}
