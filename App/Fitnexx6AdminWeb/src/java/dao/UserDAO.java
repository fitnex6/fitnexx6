package dao;

import entity.Clan;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

public class UserDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public ArrayList<User> getUser() throws Exception {
        try {
            ArrayList<User> userList = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM user";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");
                String name = rs.getString("name");
                boolean isActive = rs.getBoolean("isActive");
                Date lastActiveDate = rs.getTimestamp("last_active_date");

                int steps = 0;

                String sql2 = "SELECT SUM(steps) as total_steps FROM steps_history WHERE email = ?";
                PreparedStatement stmt2 = conn.prepareStatement(sql2);
                stmt2.setString(1, email);
                ResultSet rs2 = stmt2.executeQuery();

                while (rs2.next()) {
                    steps += rs2.getInt("total_steps");
                }

                User user = new User(email, name, steps, isActive, lastActiveDate);
                userList.add(user);
            }

            return userList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void setClans(ArrayList<Clan> clans) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            for (int i = 0; i < clans.size(); i++) {
                sql = "SET @rowNum=0";
                stmt = conn.prepareStatement(sql);
                stmt.executeUpdate();
                sql = "UPDATE user u INNER JOIN (SELECT @rowNum := @rowNum+1 AS rowNum, email FROM user ORDER BY email) AS temp "
                        + "ON u.email = temp.email "
                        + "SET u.clan_name = ? "
                        + "WHERE MOD(rowNum, ?) = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, clans.get(i).getClanName());
                stmt.setInt(2, clans.size());
                stmt.setInt(3, i);
                stmt.executeUpdate();
            }

            //stmt.executeBatch();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public JsonArray getAllFaculty() throws Exception {
        try {
            JsonArray resultJsonArray = new JsonArray();
            JsonArray values = new JsonArray();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT distinct`faculty` FROM `user` WHERE 1";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                resultJsonArray.add(new JsonPrimitive(rs.getString("faculty")));
            }

            return resultJsonArray;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
