package entity;

import java.util.Date;

public class Adventure {

    private int adventureID;
    private String name;
    private String description;
    private int points;
    private Date startDate;
    private Date endDate;
    private boolean isActive;

    public Adventure(int adventureID, String name, String description, int points, Date startDate, Date endDate, boolean isActive) {
        this.adventureID = adventureID;
        this.name = name;
        this.description = description;
        this.points = points;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isActive = isActive;
    }

    public int getAdventureID() {
        return adventureID;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getPoints() {
        return points;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public boolean isActive() {
        return isActive;
    }
}
