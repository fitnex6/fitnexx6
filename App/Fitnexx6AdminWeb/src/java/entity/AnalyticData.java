/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import java.util.ArrayList;

/**
 *
 * @author jeremy.seow.2014
 */
public class AnalyticData {

    private JsonArray labels;
    private JsonArray values;
    private ArrayList<JsonArray> listOfValues;
    private JsonObject adventures;

    public AnalyticData(JsonArray labels, ArrayList<JsonArray> listOfValues) {
        this.labels = labels;
        this.listOfValues = listOfValues;
    }

    public AnalyticData(JsonArray labels, JsonArray values) {
        this.labels = labels;
        this.values = values;
    }
    
    public AnalyticData(JsonArray values) {
        this.values = values;
    }

    public AnalyticData(JsonArray labels, ArrayList<JsonArray> listOfValues, JsonObject adventures) {
        this.labels = labels;
        this.listOfValues = listOfValues;
        this.adventures = adventures;
    }

    public JsonArray getLabels() {
        return labels;
    }
    
    public String getLabelsStr(){
        return labels.toString();
    }
 
    public String getValuesStr(){
        return values.toString();
    }
 
    public JsonArray getValues() {
        return values;
    }

    public ArrayList<JsonArray> getValuesList() {
        return listOfValues;
    }

    public JsonObject getAdventures() {
        return adventures;
    }

    public void addLabel(String label) {
        labels.add(new JsonPrimitive(label));
    }

    public void addValues(Integer value) {
        values.add(new JsonPrimitive(value));
    }

}
