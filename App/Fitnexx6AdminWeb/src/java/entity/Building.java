package entity;

public class Building {
    private int buildingID;
    private String name;
    private double latitude;
    private double longitude;

    public Building(int buildingID, String name, double latitude, double longitude) {
        this.buildingID = buildingID;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getBuildingID() {
        return buildingID;
    }

    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }    
}
