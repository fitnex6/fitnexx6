package entity;

public class Checkpoint {
    private int checkpointID;
    private String locationDetails;
    private int buildingID;
    private int missionID;

    public Checkpoint(int checkpointID, String locationDetails, int buildingID, int missionID) {
        this.checkpointID = checkpointID;
        this.locationDetails = locationDetails;
        this.buildingID = buildingID;
        this.missionID = missionID;
    }

    public int getCheckpointID() {
        return checkpointID;
    }

    public String getLocationDetails() {
        return locationDetails;
    }

    public int getBuildingID() {
        return buildingID;
    }
    
    public int getMissionID() {
        return missionID;
    }
    
}
