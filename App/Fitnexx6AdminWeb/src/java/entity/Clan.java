package entity;

public class Clan {

    private String clanName;
    private String description;
    private byte[] clanPicture;
    private int adventureID;

    public Clan(String clanName, String description, byte[] clanPicture, int adventureID) {
        this.clanName = clanName;
        this.description = description;
        this.clanPicture = clanPicture;
        this.adventureID = adventureID;
    }

    public String getClanName() {
        return clanName;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getClanPicture() {
        return clanPicture;
    }

    public int getAdventureID() {
        return adventureID;
    }

}
