package entity;

import java.util.Date;

public class Feedback {
    
    private Date dateTime;
    private String areaOfConcern;
    private String feedback;
    private String email;

    public Feedback(Date dateTime,String areaOfConcern, String feedback, String email) {
        this.dateTime = dateTime;
        this.areaOfConcern = areaOfConcern;
        this.feedback = feedback;
        this.email = email;
    }

    public Date getDateTime()  {
        return dateTime;
    }
    
    public String getAreaOfConcern() {
        return areaOfConcern;
    }

    public String getFeedback() {
        return feedback;
    }

    public String getEmail() {
        return email;
    }
}
