package entity;

import java.util.Date;

public class Redemption {
    private int transactionID;
    private Date dateTime;
    private String email;
    private int rewardID;
    private int staffPin;

    public Redemption(int transactionID, Date dateTime, String email, int rewardID, int staffPin) {
        this.transactionID = transactionID;
        this.dateTime = dateTime;
        this.email = email;
        this.rewardID = rewardID;
        this.staffPin = staffPin;
    }

    public int getTransactionID() {
        return transactionID;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public String getEmail() {
        return email;
    }

    public int getRewardID() {
        return rewardID;
    }

    public int getStaffPin() {
        return staffPin;
    }
}
