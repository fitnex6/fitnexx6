package entity;

public class RedemptionStaff {
    private int staffPin;
    private String email;

    public RedemptionStaff(int staffPin, String email) {
        this.staffPin = staffPin;
        this.email = email;
    }

    public int getStaffPin() {
        return staffPin;
    }

    public String getEmail() {
        return email;
    }   
}
