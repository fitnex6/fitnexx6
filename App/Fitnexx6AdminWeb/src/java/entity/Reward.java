package entity;

public class Reward {

    private int rewardID;
    private String description;
    private byte[] productPicture;
    private int price;
    private int qty;
    private int qtyClaimed;
    private boolean isActive;

    public Reward(int rewardID, String description, byte[] productPicture, int price, int qty, int qtyClaimed, boolean isActive) {
        this.rewardID = rewardID;
        this.description = description;
        this.productPicture = productPicture;
        this.price = price;
        this.qty = qty;
        this.qtyClaimed = qtyClaimed;
        this.isActive = isActive;
    }

    public int getRewardID() {
        return rewardID;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getProductPicture() {
        return productPicture;
    }

    public int getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }

    public int getQtyClaimed() {
        return qtyClaimed;
    }

    public boolean isActive() {
        return isActive;
    }
}
