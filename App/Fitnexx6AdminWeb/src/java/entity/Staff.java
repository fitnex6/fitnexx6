package entity;

public class Staff {

    private String email;
    private String name;
    private char staffType;
    private boolean isActive;

    public Staff(String email, String name, char staffType, boolean isActive) {
        this.email = email;
        this.name = name;
        this.staffType = staffType;
        this.isActive = isActive;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public char getStaffType() {
        return staffType;
    }
    
    public boolean isActive() {
        return isActive;
    }

    public String getStaffTypeName() {
        switch (staffType) {
            case 'M':
                return "Master Admin";
            case 'R':
                return "Redemption";
            case 'E':
                return "Admin";
            case 'A':
                return "Analytics";
        }
        return null;
    }
}
