package entity;

import java.util.Date;

public class User {

    private String email;
    private String name;
    private int steps;
    private boolean isActive;
    private Date lastActiveDate;

    public User(String email, String name, int steps, boolean isActive, Date lastActiveDate) {
        this.email = email;
        this.name = name;
        this.steps = steps;
        this.isActive = isActive;
        this.lastActiveDate = lastActiveDate;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public int getSteps() {
        return steps;
    }

    public boolean isActive() {
        return isActive;
    }

    public Date getLastActiveDate() {
        return lastActiveDate;
    }
}
