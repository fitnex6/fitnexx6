package scheduledTasks;

import dao.AdventureDAO;
import dao.ClanDAO;
import dao.NewsFeedDAO;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

public class EndAdventure implements Runnable {

    public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private int adventureID;
    private String adventureName;
    private String adminEmail;
    private int points;
    private Date startDate;
    private Date endDate;
    private ServletContext context;

    public EndAdventure(int adventureID, String adventureName, String adminEmail, int points, Date startDate, Date endDate, ServletContext context) {
        this.adventureID = adventureID;
        this.adventureName = adventureName;
        this.adminEmail = adminEmail;
        this.points = points;
        this.startDate = startDate;
        this.endDate = endDate;
        this.context = context;
    }

    public void run() {
        ClanDAO cDAO = new ClanDAO();
        AdventureDAO aDAO = new AdventureDAO();
        NewsFeedDAO nDAO = new NewsFeedDAO();

        try {
            //String topClan = cDAO.awardWinningClan(points, startDate, endDate);
            HashMap<String, Integer> clanStatsMap = cDAO.getClanStats();

            if (!clanStatsMap.isEmpty()) {
                Map<String, Integer> sortedClanMap = sortByComparator(clanStatsMap);

                for (String clan : sortedClanMap.keySet()) {
                    if (sortedClanMap.get(clan) != 0) {
                        InputStream inputStream = context.getResourceAsStream("/images/adventure.jpg");

                        BufferedImage inputImage = ImageIO.read(inputStream);
                        BufferedImage outputImage = new BufferedImage(350, 250, inputImage.getType());
                        Graphics2D g2d = outputImage.createGraphics();
                        g2d.drawImage(inputImage, 0, 0, 350, 250, null);
                        g2d.dispose();

                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                        ImageIO.write(outputImage, "jpg", outStream);
                        InputStream is = new ByteArrayInputStream(outStream.toByteArray());

                        nDAO.addNewsFeed(adminEmail, "Results of Adventure " + adventureName + "!", "The winner of adventure " + adventureName + " is " + clan
                                + "! Congratulations, each member will receive " + points + " points as reward!", is, "Event");
                        cDAO.awardPoints(points, clan);
                        break;
                    }
                }
            }

            cDAO.clearClans();
            aDAO.activateAdventure(adventureID, false);
        } catch (Exception e) {

        }
    }

    protected Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) {
        // Convert Map to List
        List<Map.Entry<String, Integer>> list
                = new LinkedList<>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                    Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // Convert sorted map back to a Map
        Map<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}
