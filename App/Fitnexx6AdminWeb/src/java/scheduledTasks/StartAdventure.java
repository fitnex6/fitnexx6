package scheduledTasks;

import dao.AdventureDAO;
import dao.ClanDAO;
import dao.NewsFeedDAO;
import dao.UserDAO;
import entity.Clan;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import utility.DateAndTime;

public class StartAdventure implements Runnable {

    public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private int adventureID;
    private String adventureName;
    private String adminEmail;
    private Date startDate;
    private Date endDate;
    private ServletContext context;

    public StartAdventure(int adventureID, String adventureName, String adminEmail, Date startDate, Date endDate, ServletContext context) {
        this.adventureID = adventureID;
        this.adventureName = adventureName;
        this.adminEmail = adminEmail;
        this.startDate = startDate;
        this.endDate = endDate;
        this.context = context;
    }

    public void run() {
        UserDAO uDAO = new UserDAO();
        AdventureDAO aDAO = new AdventureDAO();
        ClanDAO cDAO = new ClanDAO();
        NewsFeedDAO nDAO = new NewsFeedDAO();

        try {
            ArrayList<Clan> clans = cDAO.getClan(adventureID);
            uDAO.setClans(clans);
            aDAO.activateAdventure(adventureID, true);
            
            InputStream inputStream = context.getResourceAsStream("/images/adventure.jpg");            

            BufferedImage inputImage = ImageIO.read(inputStream);
            BufferedImage outputImage = new BufferedImage(350, 250, inputImage.getType());
            Graphics2D g2d = outputImage.createGraphics();
            g2d.drawImage(inputImage, 0, 0, 350, 250, null);
            g2d.dispose();

            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            ImageIO.write(outputImage, "jpg", outStream);
            InputStream is = new ByteArrayInputStream(outStream.toByteArray());

            nDAO.addNewsFeed(adminEmail, "Adventure " + adventureName + "!", "A new adventure has begun for the period " + DateAndTime.dateToStringNewsFeedFormat(startDate)
                    + " to " + DateAndTime.dateToStringNewsFeedFormat(endDate) + "! Gather your clan members "
                    + "and complete missions together for glory while having fun and keeping fit! ", is, "Event");
        } catch (Exception e) {

        }
    }
}
