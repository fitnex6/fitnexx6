<%@include file="protect.jsp" %>
<%@page import="java.util.TreeMap"%>
<%@page import="entity.Adventure"%>
<%@page import="dao.AdventureDAO"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Adventure</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li>                               
                            </ul>
                        </li>
                        <li class="active">
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <legend><h1>Adventures</h1></legend>                          
                            <div class="table-responsive" style="overflow-x: hidden;">
                                <%                                    char staffType = loggedIn.getStaffType();

                                    AdventureDAO adventureDAO = new AdventureDAO();
                                    TreeMap<Integer, Adventure> adventureMap = adventureDAO.getAdventure();

                                    if (staffType == 'A' || staffType == 'R') {
                                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                                    } else {
                                        String error = (String) session.getAttribute("adventureError");

                                        if (error != null) {
                                %>
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> <%=error%> </div>
                                <%
                                    }

                                    session.removeAttribute("adventureError");
                                %>
                                <a class="add btn btn-success btn-md" data-toggle="modal" href="#add">Add Adventure</a><br><br>

                                <table id="mytable" class="table table-bordered table-striped">
                                    <thead>
                                    <th>Adventure ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Points</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Active</th>
                                    <th>Action</th>
                                    </thead>

                                    <tbody>
                                        <%
                                            for (Adventure adventure : adventureMap.values()) {
                                                out.println("<tr>");
                                                out.println("<td align='middle'>" + adventure.getAdventureID() + "</td>");
                                                out.println("<td align='middle'>" + adventure.getName() + "</td>");
                                                out.println("<td align='middle'>" + adventure.getDescription() + "</td>");
                                                out.println("<td align='middle'>" + adventure.getPoints() + "</td>");
                                                out.println("<td align='middle'>" + adventure.getStartDate() + "</td>");
                                                out.println("<td align='middle'>" + adventure.getEndDate() + "</td>");

                                                if (adventure.isActive()) {
                                                    out.println("<td align='middle'>Yes</td>");
                                                } else {
                                                    out.println("<td align='middle'>No</td>");
                                                }
                                        %>
                                    <td align="middle"><a class="edit btn btn-primary btn-sm" data-toggle="modal" data-id="<%=adventure.getAdventureID()%>" data-name="<%=adventure.getName()%>" data-description="<%=adventure.getDescription()%>" data-points="<%=adventure.getPoints()%>" data-startdate="<%=adventure.getStartDate()%>" data-enddate="<%=adventure.getEndDate()%>" data-isactive="<%=adventure.isActive()%>" href="#edit">Edit</a>
                                        <a class="delete btn btn-danger btn-sm" data-toggle="modal" data-id="<%=adventure.getAdventureID()%>" data-isactive="<%=adventure.isActive()%>" href="#delete">Delete</a></td>
                                        <%
                                                    out.println("</tr>");
                                                }
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="AddAdventureServlet" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Adventure</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    Name: <input class="form-control" type="text" placeholder="Name" name="name" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Description: <input class="form-control" type="text" placeholder="Description" name="description" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Points: <input class="form-control" type="number" placeholder="Points" name="points" required="yes" min="0" max="1000000">
                                </div>
                                <div class="form-group">
                                    Start Date: <input class="form-control" onchange="changeDate('add', this.value)" type="date" name="startDate" required="yes" max="2036-12-31" min="<%= new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date())%>"/>
                                </div>
                                <div class="form-group">
                                    End Date: <input class="form-control" id="addEndDate" type="date" name="endDate" required="yes" max="2036-12-31" min="<%= new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date())%>"/>
                                </div> 

                                <br><legend><h3>Clans</h3></legend> 
                                <table width="100%" id="addClanTable">
                                    <%
                                        for (int i = 0; i < 2; i++) {

                                    %>
                                    <tr>
                                    <div class="form-group" id="clanDetails">
                                        <b><u>Clan Details:</u></b>
                                    </div>                                   
                                    <div class="form-group" id="addClanName">
                                        Clan Name: <input class="form-control" type="text" placeholder="Clan Name" name="clanName" required="yes" maxlength="100">
                                    </div>
                                    <div class="form-group" id="addClanDescription">
                                        Description: <input class="form-control" type="text" placeholder="Description" name="clanDescription" required="yes" maxlength="100">
                                    </div>
                                    <div class="form-group" id="addClanPicture">
                                        Clan Picture: <input type="file" name="clanPicture" accept="image/png, image/jpeg" required="yes">
                                    </div>
                                    </tr>
                                    <%                                        }
                                    %>                                
                                </table>

                                <div class="form-group">
                                    <br><input class="btn btn-success" type="button" value="Add Clan" id="addClanOption">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="EditAdventureServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Adventure</h4>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="adventureID" id="editAdventureID">
                                </div>
                                <div class="form-group">
                                    Name: <input class="form-control" type="text" placeholder="Name" name="name" id="editName" required="yes" maxlength="100">
                                </div>       
                                <div class="form-group">
                                    Description: <input class="form-control" type="text" placeholder="Description" name="description" id="editDescription" required="yes" maxlength="100">
                                </div>  
                                <div class="form-group">
                                    Points: <input class="form-control" type="number" placeholder="Points" name="points" id="editPoints" required="yes" min="0" max="1000000">
                                </div>
                                <div class="form-group">
                                    Start Date: <input class="form-control" onchange="changeDate('edit', this.value)" type="date" name="startDate" id="editStartDate" required="yes" max="2036-12-31" min="<%= new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date())%>"/>
                                </div>
                                <div class="form-group">
                                    End Date: <input class="form-control" type="date" name="endDate" id="editEndDate" required="yes" max="2036-12-31" min="<%= new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date())%>"/>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Edit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="delete" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="DeleteAdventureServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Delete Adventure</h4>
                            </div>

                            <div class="modal-body">
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record? </div>
                                <input type="hidden" name="adventureID" id="deleteAdventureID" >
                                <input type="hidden" name="isActive" id="deleteIsActive" >
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <script>
                $(document).ready(function () {
                    $('#mytable').DataTable();
                });

                $(document).on("click", ".edit", function () {
                    var adventureID = $(this).data('id');
                    var name = $(this).data('name');
                    var description = $(this).data('description');
                    var points = $(this).data('points');
                    var startDate = $(this).data('startdate');
                    var endDate = $(this).data('enddate');
                    var isActive = $(this).data('isactive');

                    $(".modal-body #editAdventureID").val(adventureID);
                    $(".modal-body #editName").val(name);
                    $(".modal-body #editDescription").val(description);
                    $(".modal-body #editPoints").val(points);
                    $(".modal-body #editStartDate").val(startDate);
                    $(".modal-body #editEndDate").val(endDate);

                    if (isActive) {
                        $(".modal-body #editStartDate").attr('readonly', true);
                    }
                });

                $(document).on("click", ".delete", function () {
                    var adventureID = $(this).data('id');
                    var isActive = $(this).data('isactive');
                    
                    $(".modal-body #deleteAdventureID").val(adventureID);
                    $(".modal-body #deleteIsActive").val(isActive);
                });

                function changeDate(type, value) {
                    switch (type) {
                        case 'add':
                            $(".modal-body #addEndDate").attr("min", value);
                            break;
                        case 'edit':
                            $(".modal-body #editEndDate").attr("min", value);
                            break;
                    }
                }

                $("#addClanOption").click(function () {
                    var tr = $("<tr />");

                    var removeButton = $("<input class=\"btn btn-danger\" type=\"button\" value=\"Remove This Clan\" /><br><br>");
                    removeButton.click(function () {
                        $(this).parent().remove();
                    });


                    tr.append($("#clanDetails").clone());
                    tr.append($("#addClanName").clone());
                    tr.append($("#addClanDescription").clone());
                    tr.append($("#addClanPicture").clone());
                    tr.append(removeButton);

                    $("#addClanTable").append(tr);
                });
            </script>
        </div>
    </body>
</html>

