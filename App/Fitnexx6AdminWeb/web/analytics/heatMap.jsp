<%@include file="../protect.jsp" %>
<%@page import="dao.AnalyticDAO,entity.AnalyticData"%>

<%    char staffType = loggedIn.getStaffType();

    if (staffType == 'R') {
        response.sendRedirect("/Fitnexx6AdminWeb/home.jsp");
    }
%>
<head>
    <style>
        body, html { margin:0; padding:0; height:100%;}
        body { font-family:sans-serif; }
        body * { font-weight:200;}
        h1 { position:absolute; background:white; padding:10px;}
        #map { height:100%; }
        .leaflet-container {
            background: rgba(0,0,0,.8) !important;
        }
        h1 { position:absolute; background:black; color:white; padding:10px; font-weight:200; z-index:10000;}
        #all-examples-info { position:absolute; background:white; font-size:16px; padding:20px; top:100px; width:350px; line-height:150%; border:1px solid rgba(0,0,0,.2);}
    </style>
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
    <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
    <script src="../js/build/heatmap.js"></script>
    <script src="../js/plugins/leaflet-heatmap/leaflet-heatmap.js"></script>
</head>
<body>

    <div id="map"></div>

    <%
        AnalyticDAO aDAO = new AnalyticDAO();
        AnalyticData aData = aDAO.getMostPopularRoutes();

    %>
    <script>
        window.onload = function () {
            var testData = {
                //max: 8,
                data: <%=aData.getValues()%>
            };
            var baseLayer = L.tileLayer(
                    'https://api.mapbox.com/styles/v1/jeremiasgoh/ciq1noy05003ldtnpwpkkeea4/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiamVyZW1pYXNnb2giLCJhIjoiY2lxMW5pdnAyMDB5YmZ0bTR2MjlkYjlpOSJ9.c2xTUXhru4niIYMVFQnKVA', {
                        attribution: 'map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery � <a href="http://mapbox.com">Mapbox</a>'
                    }
            );
            var cfg = {
                // radius should be small ONLY if scaleRadius is true (or small radius is intended)
                "radius": 30,
                "maxOpacity": .8,
                // scales the radius based on map zoom
                "scaleRadius": false,
                // if set to false the heatmap uses the global maximum for colorization
                // if activated: uses the data maximum within the current map boundaries 
                //   (there will always be a red spot with useLocalExtremas true)
                "useLocalExtrema": true,
                // which field name in your data represents the latitude - default "lat"
                latField: 'lat',
                // which field name in your data represents the longitude - default "lng"
                lngField: 'lng',
                // which field name in your data represents the data value - default "value"
                valueField: 'count'
            };
            var heatmapLayer = new HeatmapOverlay(cfg);
            var map = new L.Map('map', {
                center: new L.LatLng(1.332796, 103.775193),
                zoom: 17,
                layers: [baseLayer, heatmapLayer]
            });

            heatmapLayer.setData(testData);
            // make accessible for debugging
            layer = heatmapLayer;
        };
    </script>
</body>
