<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@include file="../protect.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dao.AnalyticDAO,entity.AnalyticData,java.util.*,java.util.Map.Entry"%>
<%@page import="com.google.gson.*"%>
<!DOCTYPE html>


<head>
    <title>User Activeness</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js'></script>
    <style>
        canvas{
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

</head>

<body>
    <div id="wrapper">            
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../home.jsp">FitNexx Admin</a>
            </div>

            <!-- Top Menu Items -->
            <%@ include file="../topnavbar.jsp" %>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="Missions" class="collapse">
                            <li>
                                <a href="../building.jsp">Buildings</a>
                            </li>
                            <li>
                                <a href="../checkpoint.jsp">Checkpoints</a>
                            </li>
                            <li>
                                <a href="../mission.jsp">Missions</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="../adventure.jsp"> Adventure</a>
                            </li>
                            <li>
                                <a href="../clan.jsp"> Clan</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="../reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                    </li>
                    <li>
                        <a href="../redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                    </li>
                    <li>
                        <a href="../newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                    </li>
                    <li>
                        <a href="../feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                    </li>
                    <li>
                        <a href="../staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                    </li>
                    <li>
                        <a href="../user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                    </li>
                    <li class="active">
                        <a href="../analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="container-fluid">
                <a href="../analytics/analyticshome.jsp"><i class="fa fa-long-arrow-left" aria-hidden="true"></i><b> Back to Analytics Home</b></a>
                <legend><h1>User Analytics</h1></legend>
                <ul class="nav navbar-nav ">
                    <li><a href="user.jsp">User Activeness</a>
                    </li>
                    <li><a href="hourlyloginbreakdown.jsp">Hourly Login Breakdown</a>
                    </li>
                    <li><a href="bmibreakdown.jsp">BMI Breakdown</a>
                    </li>
                </ul>
                <br/>
                <br/>
                <%                        char staffType = loggedIn.getStaffType();

                    if (staffType == 'R') {
                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                    } else {
                %>

                <%            AnalyticDAO aDAO = new AnalyticDAO();
                    AnalyticData aData = null;
                    //Iterator<String> iter = null;
                    //JsonArray dateRangeIndex = new JsonArray();
                    JsonObject adventures = null;

                    try {
                        aData = aDAO.getActiveUsers();
                        adventures = aData.getAdventures().getAsJsonObject();
                        //iter = aData.getRange().keySet().iterator();
                        //while(iter.hasNext()){
                        //    Integer[] dateRange = aData.getRange().get(iter.next());
                        //}
                    } catch (Exception e) {
                        //System.out.println(e.getMessage());
                    }

                %>
                <script>
                    // Ths variables are randoming colors but turn out surprisingly well so i keeping it. Scale i don know what its doing
                    var randomScalingFactor = function () {
                        return Math.round(Math.random() * 100);
                        //return 0;
                    };
                    var randomColorFactor = function () {
                        return Math.round(Math.random() * 255);
                    };
                    var randomColor = function (opacity) {
                        return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
                    };

                </script>

                <!--Hourly Logins-->
                <h1><center>Hourly Logins Breakdown</center></h1>

                <div style="width:100%;">
                    <canvas id="loginBreakdownCanvas"></canvas>
                </div>
                <%            AnalyticData loginBreakdownAD = aDAO.getHourlyLoginBreakdown();

                %>
                <script>
                    var ctx4 = document.getElementById("loginBreakdownCanvas");
                    var data4 = {
                        labels: <%=loginBreakdownAD.getLabels()%>, // Label for x-axis, json array
                        datasets: [{
                                label: "Number of Logins",
                                data:<%=loginBreakdownAD.getValues()%>, // actual data, json array
                                backgroundColor: randomColor(0.5),
                                hoverBackgroundColor: randomColor(0.5)
                            }]
                    };
                    var myLineChart = new Chart(ctx4, {
                        type: 'line',
                        data: data4,
                        options: {
                            responsive: true,
                            title: {
                                display: true,
                                text: 'Number of logins at different times of the day' // Title of chart
                            }
                        }
                    });

                </script>
                <%
                    }
                %>
            </div>
        </div>
</body>

</html>
