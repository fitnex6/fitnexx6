<%-- 
    Document   : testana
    Created on : Oct 12, 2016, 1:24:42 PM
    Author     : jeremy.seow.2014
--%>
<%@page import="dao.UserDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="dao.RewardDAO"%>
<%@include file="../protect.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dao.AnalyticDAO,entity.AnalyticData"%>
<!DOCTYPE html>


<head>
    <title>Rewards Analytics</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.min.js'></script>
    <style>
        canvas{
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

</head>

<body>
    <div id="wrapper">            
        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../home.jsp">FitNexx Admin</a>
            </div>

            <!-- Top Menu Items -->
            <%@ include file="../topnavbar.jsp" %>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="../home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="Missions" class="collapse">
                            <li>
                                <a href="../building.jsp">Buildings</a>
                            </li>
                            <li>
                                <a href="../checkpoint.jsp">Checkpoints</a>
                            </li>
                            <li>
                                <a href="../mission.jsp">Missions</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="../adventure.jsp"> Adventure</a>
                            </li>
                            <li>
                                <a href="../clan.jsp"> Clan</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="../reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                    </li>
                    <li>
                        <a href="../redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                    </li>
                    <li>
                        <a href="../newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                    </li>
                    <li>
                        <a href="../feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                    </li>
                    <li>
                        <a href="../staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                    </li>
                    <li>
                        <a href="../user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                    </li>
                    <li class="active">
                        <a href="../analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                    </li>
                </ul>
            </div>
        </nav>

        <%            AnalyticDAO aDAO = new AnalyticDAO();
        %>
        <div id="page-wrapper">
            <div class="container-fluid">
                <a href="../analytics/analyticshome.jsp"><i class="fa fa-long-arrow-left" aria-hidden="true"></i><b> Back to Analytics Home</b></a>
                <legend><h1>Reward Analytics</h1></legend>
                <%                        char staffType = loggedIn.getStaffType();

                    if (staffType == 'R') {
                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                    } else {
                %>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="row">
                                <div class="col-lg-3">

                                    <center><h2><b>Reward Redemption for Past 7 Days</b></h2></center>

                                    <center><h1><%=aDAO.getPast7DaysRedemptionCount()%></h1></center>
                                </div>
                                <div class="col-lg-3">

                                    <center><h2><b>Total Reward Redeemed</b></h2></center><br/>

                                    <center><h1><%=aDAO.getTotalRedemptionCount()%></h1></center>

                                </div>
                                <div class="col-lg-3">

                                    <center><h2><b>Average Reward Redeemed</b></h2></center>

                                    <center><h1><%=aDAO.getAvgRedemptionCount()%></h1></center>

                                </div>
                                <div class="col-lg-3">

                                    <center><h2><b>Users with at least 1 redemption</b></h2></center>

                                    <center><h1><%=aDAO.getUniqueUserRedemptionCount()%></h1></center>

                                </div>
                            </div>
                        </div>


                    </div>

                </div>

                <script>
                    var randomScalingFactor = function () {
                        return Math.round(Math.random() * 100);
                        //return 0;
                    };
                    var randomColorFactor = function () {
                        return Math.round(Math.random() * 255);
                    };
                    var randomColor = function (opacity) {
                        return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
                    };</script>
                <br/>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <center><b>Redeemed Rewards </b></center>
                            </div>
                            <!--Reward-->
                            <div style="width:100%;">
                                <canvas id="rewardCanvas"></canvas>
                            </div>
                            <%            RewardDAO rDAO = new RewardDAO();

                                AnalyticData rewardData = null;
                                try {
                                    rewardData = rDAO.getRewardDecriptions();
                                } catch (Exception e) {
                                    //System.out.println(e.getMessage());
                                }

                            %>

                            <script>
                                var ctx2 = document.getElementById("rewardCanvas");
                                var colours = [];
                                for (i = 0; i < <%=rewardData.getLabels().size()%>; i++) {
                                    colours.push(randomColor(0.8));
                                }
                                var data = {
                                    labels: <%=rewardData.getLabels()%>, // Label for x-axis, json array
                                    datasets: [{
                                            data: <%=rewardData.getValues()%>, // actual data, json array
                                            backgroundColor: colours,
                                            hoverBackgroundColor: colours
                                        }]
                                };
                                var myDoughnutChart = new Chart(ctx2, {
                                    type: 'doughnut',
                                    data: data,
                                    options: {
                                        responsive: true,
                                        title: {
                                            display: false,
                                            text: 'Rewards' // Title of chart
                                        }
                                    }
                                });
                                var facultyDatasets = [];
                                var facultyLabels = [];</script>
                            <br/>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-yellow">
                            <!--Reward by Demographic-->
                            <div class="panel-heading">
                                <center><b>Individual Reward Breakdown by Faculty</b></center>
                            </div>

                            <%
                                HashMap<String, AnalyticData> rewardBreakdownHash = rDAO.getRewardsBreakdownByFaculty();

                                List<String> keyList = new ArrayList<String>();
                                keyList.addAll(rewardBreakdownHash.keySet());

                                for (String key : keyList) {
                                    AnalyticData tempAD = rewardBreakdownHash.get(key);
                            %>
                            <script>
                                facultyDatasets.push(<%=tempAD.getValues()%>);
                                facultyLabels.push(<%=tempAD.getLabels()%>);</script>
                                <%
                                    }

                                    try {
                                        rewardData = rewardBreakdownHash.get(1);
                                    } catch (Exception e) {
                                        //System.out.println(e.getMessage());
                                    }

                                %>

                            <i><h3><center>Select a reward:</center></h3></i>
                            <form>
                                <center>
                                    <select name="reward" onchange="changeReward(this.value)">
                                        <%                    for (int i = 1; i <= keyList.size(); i++) {
                                        %>
                                        <option value="<%=i%>"><%=keyList.get(i - 1)%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                </center>
                            </form>

                            <div style="width:100%;">
                                <canvas id="rewardDemographicCanvas"></canvas>
                            </div>

                            <script>
                                var ctx3 = document.getElementById("rewardDemographicCanvas");
                                var colours = [];
                                for (i = 0; i < 20; i++) {
                                    colours.push(randomColor(0.8));
                                }


                                var data3 = {
                                    labels: facultyLabels[0], // Label for x-axis, json array
                                    datasets: [{
                                            data: facultyDatasets[0], // actual data, json array
                                            backgroundColor: colours,
                                            hoverBackgroundColor: colours
                                        }]
                                };
                                var myDoughnutChart = new Chart(ctx3, {
                                    type: 'doughnut',
                                    data: data3,
                                    options: {
                                        responsive: true,
                                        title: {
                                            display: false,
                                            text: 'Individual Rewards Breakdown' // Title of chart
                                        }
                                    }
                                });
                                function changeReward(value) {
                                    var newValue = value;
                                    var newDataset = facultyDatasets[newValue - 1];
                                    var newLabels = facultyLabels[newValue - 1];
                                    myDoughnutChart.data.datasets[0].data = newDataset;
                                    myDoughnutChart.config.data.labels = newLabels;
                                    myDoughnutChart.update();
                                }
                                ;
                            </script>
                        </div>
                    </div>
                </div>

                <%
                    }
                %>
            </div>
        </div>
</body>

</html>
