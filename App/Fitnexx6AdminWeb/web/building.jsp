<%@include file="protect.jsp" %>
<%@page import="entity.Building"%>
<%@page import="dao.BuildingDAO"%>
<%@page import="java.util.TreeMap"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Building</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <!-- Help Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Building Help</h4>
                            </div>
                                                      
                            <div class="modal-body">
                                <center><b>Step 1: Create a Building</b><br><br> <a href="/Fitnexx6AdminWeb/building.jsp"><img src="images/building.png" style=width:128px;height:128px;></img></a></center><br><br>
                                <center><b>Step 2: Create a Mission</b><br><br> <a href="/Fitnexx6AdminWeb/mission.jsp"><img src="images/mission.png" style=width:128px;height:128px;></img></a></center><br><br>
                                <center><b>Step 3: Create a Checkpoint</b><br><br> <a href="/Fitnexx6AdminWeb/checkpoint.jsp"><img src="images/checkpoint.png" style=width:128px;height:128px;></img></a></center>
                            </div>
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">
                                        Buildings
                                        <!-- Trigger the modal with a button -->
                                        <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#myModal"><i class="fa fa-question" aria-hidden="true"></i></button>
                                    </h1>
                                </div>
                            </div>                 
                            <div class="table-responsive" style="overflow-x: hidden;">
                                <%                                    char staffType = loggedIn.getStaffType();

                                    if (staffType == 'A' || staffType == 'R') {
                                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                                    } else {
                                %>
                                <a class="add btn btn-success btn-md" data-toggle="modal" href="#add">Add Building</a><br><br>
                                <%
                                    BuildingDAO buildingDAO = new BuildingDAO();
                                    TreeMap<Integer, Building> buildingMap = buildingDAO.getBuilding();
                                %>
                                <table id="mytable" class="table table-bordered table-striped">
                                    <thead>
                                    <th>Building ID</th>
                                    <th>Name</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th>Action</th>
                                    </thead>

                                    <tbody>
                                        <%
                                            for (Building building : buildingMap.values()) {
                                                out.println("<tr>");
                                                out.println("<td align='middle'>" + building.getBuildingID() + "</td>");
                                                out.println("<td align='middle'>" + building.getName() + "</td>");
                                                out.println("<td align='middle'>" + building.getLatitude() + "</td>");
                                                out.println("<td align='middle'>" + building.getLongitude() + "</td>");
                                        %>
                                    <td align="middle"><a class="edit btn btn-primary btn-sm" data-toggle="modal" data-id="<%=building.getBuildingID()%>" data-name="<%=building.getName()%>" data-latitude="<%=building.getLatitude()%>" data-longitude="<%=building.getLongitude()%>" href="#edit">Edit</a>
                                        <a class="delete btn btn-danger btn-sm" data-toggle="modal" data-id="<%=building.getBuildingID()%>" href="#delete">Delete</a></td>
                                        <%
                                                    out.println("</tr>");
                                                }
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="AddBuildingServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Building</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    <iframe id='addMapboxMap' frameborder="0" scrolling="no" frameborder="0" style="overflow-x:hidden;" height="300" width="100%"></iframe>                                         
                                </div>
                                <div class="form-group">
                                    Name: <input class="form-control" type="text" placeholder="Name" name="name" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Latitude: (Enter 5 decimal places for accuracy) <input class="form-control" type="number" placeholder="Latitude" name="latitude" required="yes" step="any">
                                </div>    
                                <div class="form-group">
                                    Longitude: (Enter 5 decimal places for accuracy) <input class="form-control" type="number" placeholder="Longitude" name="longitude" required="yes" step="any">
                                </div> 
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="EditBuildingServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Building</h4>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="buildingID" id="editBuildingID">
                                </div>
                                <div class="form-group">
                                    <iframe id='editMapboxMap' frameborder="0" scrolling="no" frameborder="0" style="overflow-x:hidden;" height="300" width="100%"></iframe>                                         
                                </div>
                                <div class="form-group">
                                    Name: <input class="form-control" type="text" placeholder="Name" name="name" id="editName" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Latitude: (Enter 5 decimal places for accuracy) <input class="form-control" type="number" placeholder="Latitude" name="latitude" id="editLatitude" required="yes" step="any">
                                </div>
                                <div class="form-group">
                                    Longitude: (Enter 5 decimal places for accuracy) <input class="form-control" type="number" placeholder="Longitude" name="longitude" id="editLongitude" required="yes" step="any">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Edit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="delete" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="DeleteBuildingServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Delete Building</h4>
                            </div>

                            <div class="modal-body">
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record? </div>
                                <input type="hidden" name="buildingID" id="deleteBuildingID">
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <script>
                $(document).ready(function () {
                    $('#mytable').DataTable();

                    var doc = $('#addMapboxMap')[0].contentWindow.document;

                    var jsURL = 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.26.0/mapbox-gl.js',
                            cssURL = 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.26.0/mapbox-gl.css';

                    var exampleCSS = '#map { position:absolute; top:0; bottom:0; width:100%; }';

                    var examplePREFIX =
                            "<html><head><meta charset=utf-8 /><title></title>\n" +
                            "<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />\n" +
                            "<script src='" + jsURL + "'><\/script>\n" +
                            "<link href='" + cssURL + "' rel='stylesheet' />\n" +
                            "<style>" + exampleCSS + "</style>\n" +
                            "<script>" +
                            "mapboxgl.accessToken = 'pk.eyJ1IjoiamVyZW1pYXNnb2giLCJhIjoiY2lxYzVrenIwMDFtZWZybTExejhjN3RheiJ9.6Dv-027CiApS9XuzaG0QKA';" +
                            "<\/script>\n" +
                            "</head>\n<body>\n";

                    var exampleCONTENT = "<style type='text/css'>\n    #info {\n        display: block;\n        position: relative;\n        margin: 0px auto;\n        width: 70%;\n        padding: 10px;\n        border: none;\n        border-radius: 3px;\n        font-size: 12px;\n        text-align: center;\n        color: #222;\n        background: #fff;\n    }\n<\/style>\n<div id='map'><\/div>\n<pre id='info'><\/pre>\n<script>\nvar map = new mapboxgl.Map({\n    container: 'map', // container id\n    style: 'mapbox://styles/mapbox/streets-v9',\n    center: [103.77517, 1.33301], // starting position\n    zoom: 16 // starting zoom\n});\n\nmap.on('mousemove', function (e) {\n    document.getElementById('info').innerHTML =\n        // e.lngLat is the longitude, latitude geographical position of the event\n        JSON.stringify(e.lngLat);\n});\n<\/script>\n";
                    var examplePOSTFIX = "</body>\n</html>";

                    doc.open();
                    doc.write(examplePREFIX + exampleCONTENT + examplePOSTFIX);
                    doc.close();
                });

                $(document).on("click", ".edit", function () {
                    var buildingID = $(this).data('id');
                    var name = $(this).data('name');
                    var latitude = $(this).data('latitude');
                    var longitude = $(this).data('longitude');

                    $(".modal-body #editBuildingID").val(buildingID);
                    $(".modal-body #editName").val(name);
                    $(".modal-body #editLatitude").val(latitude);
                    $(".modal-body #editLongitude").val(longitude);

                    var doc = $('#editMapboxMap')[0].contentWindow.document;

                    var jsURL = 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.26.0/mapbox-gl.js',
                            cssURL = 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.26.0/mapbox-gl.css';

                    var exampleCSS = '#map { position:absolute; top:0; bottom:0; width:100%; }';

                    var examplePREFIX =
                            "<html><head><meta charset=utf-8 /><title></title>\n" +
                            "<meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />\n" +
                            "<script src='" + jsURL + "'><\/script>\n" +
                            "<link href='" + cssURL + "' rel='stylesheet' />\n" +
                            "<style>" + exampleCSS + "</style>\n" +
                            "<script>" +
                            "mapboxgl.accessToken = 'pk.eyJ1IjoiamVyZW1pYXNnb2giLCJhIjoiY2lxYzVrenIwMDFtZWZybTExejhjN3RheiJ9.6Dv-027CiApS9XuzaG0QKA';" +
                            "<\/script>\n" +
                            "</head>\n<body>\n";

                    var exampleCONTENT = "<style type='text/css'>\n    #info {\n        display: block;\n        position: relative;\n        margin: 0px auto;\n        width: 70%;\n        padding: 10px;\n        border: none;\n        border-radius: 3px;\n        font-size: 12px;\n        text-align: center;\n        color: #222;\n        background: #fff;\n    }\n<\/style>\n<div id='map'><\/div>\n<pre id='info'><\/pre>\n<script>\nvar map = new mapboxgl.Map({\n    container: 'map', // container id\n    style: 'mapbox://styles/mapbox/streets-v9',\n    center: [103.77517, 1.33301], // starting position\n    zoom: 16 // starting zoom\n});\n\nmap.on('mousemove', function (e) {\n    document.getElementById('info').innerHTML =\n        // e.lngLat is the longitude, latitude geographical position of the event\n        JSON.stringify(e.lngLat);\n});\n<\/script>\n";
                    var examplePOSTFIX = "</body>\n</html>";

                    doc.open();
                    doc.write(examplePREFIX + exampleCONTENT + examplePOSTFIX);
                    doc.close();
                });

                $(document).on("click", ".delete", function () {
                    var buildingID = $(this).data('id');
                    $(".modal-body #deleteBuildingID").val(buildingID);
                });
            </script>
        </div>
    </body>
</html>

