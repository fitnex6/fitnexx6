<%@include file="protect.jsp" %>
<%@page import="dao.AdventureDAO"%>
<%@page import="entity.Adventure"%>
<%@page import="java.util.TreeMap"%>
<%@page import="entity.Clan"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.ClanDAO"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Clan</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li class="active">
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <legend><h1>Clans</h1></legend>                          
                            <div class="table-responsive" style="overflow-x: hidden;">
                                <%                                    char staffType = loggedIn.getStaffType();

                                    ClanDAO clanDAO = new ClanDAO();
                                    ArrayList<Clan> clanList = clanDAO.getClan();

                                    AdventureDAO adventureDAO = new AdventureDAO();
                                    TreeMap<Integer, Adventure> adventureMap = adventureDAO.getAdventure();

                                    if (staffType == 'A' || staffType == 'R') {
                                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                                    } else {
                                        String error = (String) session.getAttribute("clanError");

                                        if (error != null) {
                                %>
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> <%=error%> </div>
                                <%
                                    }

                                    session.removeAttribute("clanError");
                                %>
                                <a class="add btn btn-success btn-md" data-toggle="modal" href="#add">Add Clan</a><br><br>

                                <table id="mytable" class="table table-bordered table-striped">
                                    <thead>
                                    <th>Adventure</th>
                                    <th>Clan Name</th>
                                    <th>Description</th>
                                    <th>Clan Picture</th>
                                    <th>Action</th>
                                    </thead>

                                    <tbody>
                                        <%
                                            for (Clan clan : clanList) {
                                                out.println("<tr>");
                                                out.println("<td align='middle'>" + adventureMap.get(clan.getAdventureID()).getName() + "</td>");
                                                out.println("<td align='middle'>" + clan.getClanName() + "</td>");
                                                out.println("<td align='middle'>" + clan.getDescription() + "</td>");
                                                out.println("<td align='middle'><img src=ImageServlet?clanName=" + clan.getClanName().replace(" ", "%20") + "&adventureID=" + clan.getAdventureID() + " style=width:128px;height:128px;></img></td>");
                                        %>
                                    <td align="middle"><a class="edit btn btn-primary btn-sm" data-toggle="modal" data-adventureid="<%=clan.getAdventureID()%>" data-name="<%=adventureMap.get(clan.getAdventureID()).getName()%>" data-clanname="<%=clan.getClanName()%>" data-description="<%=clan.getDescription()%>" data-isactive="<%=adventureMap.get(clan.getAdventureID()).isActive()%>" href="#edit">Edit</a>
                                        <a class="delete btn btn-danger btn-sm" data-toggle="modal" data-adventureid="<%=clan.getAdventureID()%>" data-clanname="<%=clan.getClanName()%>" data-isactive="<%=adventureMap.get(clan.getAdventureID()).isActive()%>" href="#delete">Delete</a></td>
                                        <%
                                                    out.println("</tr>");
                                                }
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="AddClanServlet" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Clan</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    Adventure:
                                    <select class="form-control" name="adventureID" >
                                        <%
                                            for (Integer key : adventureMap.keySet()) {
                                                if (!adventureMap.get(key).isActive()) {
                                                    out.println("<option value=" + key + ">" + adventureMap.get(key).getName() + "</option>");
                                                }
                                            }
                                        %>
                                    </select>
                                </div> 
                                <div class="form-group">
                                    Clan Name: <input class="form-control" type="text" placeholder="Clan Name" name="clanName" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Description: <input class="form-control" type="text" placeholder="Description" name="description" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Clan Picture: <input type="file" name="clanPicture" accept="image/png, image/jpeg" required="yes">
                                </div>                       
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="EditClanServlet" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Clan</h4>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="oldAdventureID" id="oldAdventureID">
                                    <input class="form-control" type="hidden" name="oldClanName" id="oldClanName">
                                    <input class="form-control" disabled="disabled" type="hidden" name="adventureID" id="adventureIDVal">
                                </div>
                                <div class="form-group" id="adventureIDDiv">
                                    Adventure:
                                    <select class="form-control" name="adventureID" id="editAdventureID">
                                        <%
                                            for (Integer key : adventureMap.keySet()) {
                                                if (!adventureMap.get(key).isActive()) {
                                                    out.println("<option value=" + key + ">" + adventureMap.get(key).getName() + "</option>");
                                                }
                                            }
                                        %>
                                    </select>
                                </div> 

                                <div class="form-group" id="adventureText" style="display:none;">
                                    Adventure: <input class="form-control" disabled="disabled" type="text" id="adventureTextVal" > 
                                </div>

                                <div class="form-group">
                                    Clan Name: <input class="form-control" type="text" placeholder="Clan Name" name="clanName" id="editClanName" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Description: <input class="form-control" type="text" placeholder="Description" name="description" id="editDescription" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Clan Picture: <input type="file" name="clanPicture" accept="image/png, image/jpeg">
                                </div>       
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Edit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="delete" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="DeleteClanServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Delete Clan</h4>
                            </div>

                            <div class="modal-body">
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record? </div>
                                <input type="hidden" name="adventureID" id="deleteAdventureID" >
                                <input type="hidden" name="clanName" id="deleteClanName" >
                                <input type="hidden" name="isActive" id="deleteIsActive" >
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <script>
                $(document).ready(function () {
                    $('#mytable').DataTable();
                });

                $(document).on("click", ".edit", function () {
                    var adventureID = $(this).data('adventureid');
                    var adventureName = $(this).data('name');
                    var clanName = $(this).data('clanname');
                    var description = $(this).data('description');
                    var isActive = $(this).data('isactive');

                    $(".modal-body #oldAdventureID").val(adventureID);
                    $(".modal-body #oldClanName").val(clanName);

                    $(".modal-body #editAdventureID").val(adventureID);
                    $(".modal-body #editClanName").val(clanName);
                    $(".modal-body #editDescription").val(description);
                    
                    if (isActive) {
                        $('#adventureIDDiv').hide();
                        $('#editAdventureID').prop('disabled', true);
                        
                        $('#adventureText').show();
                        $('#adventureTextVal').prop('disabled', false);
                        $('#adventureTextVal').val(adventureName);
                        $('#adventureTextVal').attr('readonly', true);
                                              
                        $('#adventureIDVal').prop('disabled', false);
                        $('#adventureIDVal').val(adventureID);
                    } else {
                        $('#adventureIDDiv').show();
                        $('#editAdventureID').prop('disabled', false);

                        $('#adventureText').hide();
                        $('#adventureTextVal').prop('disabled', true);
                        
                        $('#adventureIDVal').prop('disabled', true);
                    }
                });

                $(document).on("click", ".delete", function () {
                    var adventureID = $(this).data('adventureid');
                    var clanName = $(this).data('clanname');
                    var isActive = $(this).data('isactive');
                    
                    $(".modal-body #deleteAdventureID").val(adventureID);
                    $(".modal-body #deleteClanName").val(clanName);
                    $(".modal-body #deleteIsActive").val(isActive);
                });
            </script>
        </div>
    </body>
</html>

