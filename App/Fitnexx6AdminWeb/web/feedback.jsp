<%@include file="protect.jsp" %>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Date"%>
<%@page import="entity.Feedback"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.FeedbackDAO"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Feedback</title>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/viewFeedback.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">    
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li class="active">
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <legend><h1>Feedbacks</h1></legend>   
                            <div class="panel panel-default">
                                <div class="pull-left">
                                    <h5><b>Feedback Type:</b></h5>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary btn-filter" data-target="All">All</button>
                                        <button type="button" class="btn btn-success btn-filter" data-target="Suggestion">Suggestion</button>
                                        <button type="button" class="btn btn-danger btn-filter" data-target="Technical Bug">Technical Bug</button>
                                        <button type="button" class="btn btn-warning btn-filter" data-target="Others">Others</button>
                                    </div>
                                </div>
                                <div class="table-container">
                                    <table class="table table-filter">
                                        <tbody>
                                            <%                                                FeedbackDAO fbDAO = new FeedbackDAO();
                                                ArrayList<Feedback> fbList = fbDAO.getFeedback();
                                                Collections.reverse(fbList);

                                                for (Feedback fb : fbList) {
                                                    Date date = fb.getDateTime();
                                                    String areaOfConcern = fb.getAreaOfConcern();
                                                    String feedback = fb.getFeedback();
                                                    String email = fb.getEmail();
                                            %>
                                            <tr data-status="<%=areaOfConcern%>" class="selected">
                                                <td>
                                                    <div class="media">
                                                        <div class="media-body">
                                                            <span class="media-meta pull-right"><%=date%></span>
                                                            <h4 class="title">
                                                                <%=email%>
                                                                <span class="pull-right <%=areaOfConcern%>">(<%=areaOfConcern%>)</span>
                                                            </h4>
                                                            <p class="summary"><%=feedback%></p>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $(document).ready(function () {
                    $('.btn-filter').on('click', function () {
                        var $target = $(this).data('target');

                        if ($target != 'All') {
                            $('.table tr').css('display', 'none');
                            $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
                        } else {
                            $('.table tr').css('display', 'none').fadeIn('slow');
                        }
                    });

                });
            </script>
        </div>
    </body>
</html>

