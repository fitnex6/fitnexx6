<%@include file="protect.jsp" %>
<%@page import="dao.AnalyticDAO"%>
<%@page import="dao.CheckpointDAO"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>FitNexx Admin Portal</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="icon" type="image/png" href="images/fitNexx6_logo_xsmall.png" />
        <link href="css/icons.css" rel="stylesheet">
    </head>

    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="active">
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                                Dashboard <small>Statistics Overview</small>

                                <!-- Trigger the modal with a button -->
                                <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#myModal"><i class="fa fa-question" aria-hidden="true"></i></button>


                            </h1>
                            <ol class="breadcrumb">
                                <li class="active">
                                    <i class="fa fa-dashboard"></i> Current Statistics
                                </li>
                            </ol>
                        </div>
                    </div>

                    <!-- Help Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content - Help Content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Dashboard Help</h4>
                                </div>
                                <div class="modal-body">
                                    <p>This is the dashboard where you can view quick statistics about the current health of FitNexx. Select one of the options on the side to manage the various parts of the application</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.row -->
                    <div id="page-wrapper">
                        <%                                    //
                            AnalyticDAO analyticDAO = new AnalyticDAO();
                            int[] valuesArray = analyticDAO.getDashBoard();
                        %>
                        <!-- /.row -->
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="icon-footsteps"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge"><%=valuesArray[1]%></div>
                                                <div>Total Steps Taken</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="panel panel-green">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-group fa-4x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">

                                                <div class="huge"><%=valuesArray[0]%></div>
                                                <div>Total Users</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="panel panel-yellow">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-flag fa-4x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge"><%=valuesArray[2]%></div>
                                                <div>Missions Completed</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="panel panel-red">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fa fa-comments fa-4x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <div class="huge"><%=valuesArray[3]%></div>
                                                <div>Feedbacks</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- /#page-wrapper -->

            </div>
            <!-- /#wrapper -->

            <!-- jQuery -->
            <script src="js/jquery.js"></script>

            <!-- Bootstrap Core JavaScript -->
            <script src="js/bootstrap.min.js"></script>

            <!-- Morris Charts JavaScript -->
            <script src="js/plugins/morris/raphael.min.js"></script>
            <script src="js/plugins/morris/morris.min.js"></script>
            <script src="js/plugins/morris/morris-data.js"></script>
        </div>

        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>


    </body>
</html>
