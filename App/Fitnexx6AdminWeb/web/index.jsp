<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body background="images/walking.jpg">
    <center><img src="images/fitNexx6_logo_xsmall.png" alt="fitnexx logo"/></center>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form action="LoginServlet" method="post" role="form">
                            <fieldset>
                                <div class="form-group">
                                    <%
                                        String inputEmail = (String) request.getAttribute("inputEmail");

                                        if (inputEmail == null) {
                                            inputEmail = "";
                                        }
                                    %>
                                    <input class="form-control" placeholder="E-mail" name="email" value="<%=inputEmail%>" autofocus>
                                    <%
                                        request.removeAttribute("inputEmail");
                                    %>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password">
                                </div>
                                <center><font color="red">
                                    <%
                                        String error = (String) request.getAttribute("loginErrorMsg");

                                        if (error != null) {
                                            out.println(error);
                                        }

                                        request.removeAttribute("loginErrorMsg");
                                    %>
                                    </font></center>
                                <div>
                                    <input type="submit" value="Login"><br><br>
                                </div>
                                <div>
                                    <a href="forgetPW.jsp">Forgot Your Password?</a>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>
</html>
