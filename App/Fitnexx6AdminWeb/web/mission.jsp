<%@include file="protect.jsp" %>
<%@page import="java.util.HashMap"%>
<%@page import="entity.Building"%>
<%@page import="java.util.TreeMap"%>
<%@page import="dao.BuildingDAO"%>
<%@page import="entity.Checkpoint"%>
<%@page import="dao.CheckpointDAO"%>
<%@page import="entity.Mission"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.MissionDAO"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Mission</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <!-- Help Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Mission Help</h4>
                            </div>

                            <div class="modal-body">
                                <center><b>Step 1: Create a Building</b><br><br> <a href="/Fitnexx6AdminWeb/building.jsp"><img src="images/building.png" style=width:128px;height:128px;></img></a></center><br><br>
                                <center><b>Step 2: Create a Mission</b><br><br> <a href="/Fitnexx6AdminWeb/mission.jsp"><img src="images/mission.png" style=width:128px;height:128px;></img></a></center><br><br>
                                <center><b>Step 3: Create a Checkpoint</b><br><br> <a href="/Fitnexx6AdminWeb/checkpoint.jsp"><img src="images/checkpoint.png" style=width:128px;height:128px;></img></a></center>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">
                                        Missions
                                        <!-- Trigger the modal with a button -->
                                        <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#myModal"><i class="fa fa-question" aria-hidden="true"></i></button>
                                    </h1>
                                </div>
                            </div>                   
                            <div class="table-responsive" style="overflow-x: hidden;">
                                <%                                    char staffType = loggedIn.getStaffType();

                                    MissionDAO missionDAO = new MissionDAO();
                                    CheckpointDAO checkpointDAO = new CheckpointDAO();
                                    BuildingDAO buildingDAO = new BuildingDAO();
                                    TreeMap<Integer, Mission> missionMap = missionDAO.getMission();
                                    ArrayList<Checkpoint> checkpointList = checkpointDAO.getCheckpoint();
                                    TreeMap<Integer, Building> buildingMap = buildingDAO.getBuilding();
                                    HashMap<Integer, String> missionCheckpointsMap = missionDAO.getMissionCheckpoints();

                                    if (staffType == 'A' || staffType == 'R') {
                                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                                    } else {
                                %>
                                <a class="add btn btn-success btn-md" data-toggle="modal" href="#add">Add Mission</a><br><br>

                                <table id="mytable" class="table table-bordered table-striped">
                                    <thead>
                                    <th>Mission ID</th>
                                    <th>Description</th>
                                    <th>Mission Type</th>
                                    <th>Steps</th>
                                    <th>Checkpoints</th>
                                    <th>Points</th>
                                    <th>Action</th>
                                    </thead>

                                    <tbody>
                                        <%
                                            for (Mission mission : missionMap.values()) {
                                                out.println("<tr>");
                                                out.println("<td align='middle'>" + mission.getMissionID() + "</td>");
                                                out.println("<td align='middle'>" + mission.getDescription() + "</td>");
                                                out.println("<td align='middle'>" + mission.getMissionType() + "</td>");

                                                if (mission.getSteps() == 0) {
                                                    out.println("<td align='middle'>-</td>");
                                                } else {
                                                    out.println("<td align='middle'>" + mission.getSteps() + "</td>");
                                                }

                                                String missionCheckpoints = missionCheckpointsMap.get(mission.getMissionID());

                                                if (missionCheckpoints == null) {
                                                    missionCheckpoints = "-";
                                                }

                                                out.println("<td align='middle'>" + missionCheckpoints + "</td>");
                                                out.println("<td align='middle'>" + mission.getPoints() + "</td>");
                                        %>
                                    <td align="middle"><a class="edit btn btn-primary btn-sm" data-toggle="modal" data-id="<%=mission.getMissionID()%>" data-description="<%=mission.getDescription()%>" data-missiontype="<%=mission.getMissionType()%>" data-steps="<%=mission.getSteps()%>" data-points="<%=mission.getPoints()%>" data-missioncheckpoints="<%=missionCheckpoints%>" href="#edit">Edit</a>
                                        <a class="delete btn btn-danger btn-sm" data-toggle="modal" data-id="<%=mission.getMissionID()%>" href="#delete">Delete</a>
                                        <%
                                            if (mission.getMissionType().equalsIgnoreCase("Special")) {
                                        %>
                                        <a class="generate btn btn-success btn-sm" data-toggle="modal" data-id="<%=mission.getMissionID()%>" href="#generate">QR Code</a>
                                        <%
                                            }
                                        %>
                                    </td>
                                    <%                                                    out.println("</tr>");
                                            }
                                        }
                                    %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="AddMissionServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Mission</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    Mission Type:
                                    <select class="form-control" name="missionType" id="addMissionType">
                                        <option value="Checkpoint">Checkpoint</option>
                                        <option value="Steps">Steps</option>
                                        <option value="Special">Special</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    Description: <input class="form-control" type="text" placeholder="Description" name="description" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Points: <input class="form-control" type="number" placeholder="Points" name="points" required="yes" min="0" max="1000000">
                                </div> 

                                <div id="addCheckpointMission">
                                    <div class="form-group">
                                        <table width="100%" id="addCheckpointsTable">
                                            Checkpoints:
                                            <tr>
                                                <td>
                                                    <select class="form-control" name="checkpoints" id="addCheckpoints">
                                                        <%
                                                            for (Checkpoint checkpoint : checkpointList) {
                                                                out.println("<option value=" + checkpoint.getCheckpointID() + ">" + checkpoint.getCheckpointID() + " - " + buildingMap.get(checkpoint.getBuildingID()).getName() + " (" + checkpoint.getLocationDetails() + ")</option>");
                                                            }
                                                        %>
                                                    </select>
                                                </td>
                                            </tr>                                            
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-success" type="button" value="Add Checkpoint" id="addCheckpointOption">
                                    </div>
                                </div> 

                                <div id="addStepsMission" style="display:none;">                                  
                                    <div class="form-group">
                                        Steps: <input class="form-control" disabled="disabled" type="number" placeholder="Steps" name="steps" id="addSteps" required="yes" min="0" max="1000000">
                                    </div>
                                </div>  
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="EditMissionServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Mission</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="missionID" id="editMissionID" >
                                </div>                
                                <div class="form-group">                                      
                                    <input class="form-control" type="hidden" name="missionType" id="editMissionType" >
                                </div>
                                <div class="form-group">
                                    Description: <input class="form-control" type="text" placeholder="Description" name="description" id="editDescription" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Points: <input class="form-control" type="number" placeholder="Points" name="points" id="editPoints" required="yes" min="0" max="1000000">
                                </div> 

                                <div id="editCheckpointMission">
                                    <div class="form-group">
                                        <table width="100%" id="editCheckpointsTable">
                                            Checkpoints: 
                                            <tr>         
                                                <td>
                                                    <select class="form-control" name="checkpoints" id="editCheckpoints">
                                                        <%
                                                            for (Checkpoint checkpoint : checkpointList) {
                                                                out.println("<option value=" + checkpoint.getCheckpointID() + ">" + checkpoint.getCheckpointID() + " - " + buildingMap.get(checkpoint.getBuildingID()).getName() + " (" + checkpoint.getLocationDetails() + ")</option>");
                                                            }
                                                        %>
                                                    </select>
                                                </td>
                                            </tr>   
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <input class="btn btn-success" type="button" value="Add Checkpoint" id="editCheckpointOption">
                                    </div>
                                </div> 

                                <div id="editStepsMission" style="display:none;">                                  
                                    <div class="form-group">
                                        Steps: <input class="form-control" disabled="disabled" type="number" placeholder="Steps" name="steps" id="editSteps" required="yes" min="0" max="1000000">
                                    </div>
                                </div>  
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Edit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="delete" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="DeleteMissionServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Delete Mission</h4>
                            </div>

                            <div class="modal-body">
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record? </div>
                                <input type="hidden" name="missionID" id="deleteMissionID" >
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="generate" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            <h4 class="modal-title custom_align" id="Heading">QR Code</h4>
                        </div>

                        <div class="modal-body">
                            <center><b>Click on the image to download the QR Code!</b></center>
                            <center><a href="" download="qrcode" class="saveQRCode"><img src="" style=width:258px;height:258px; class="generateQR"></img></a></center>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Back</button>
                        </div>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <script>
                $(document).ready(function () {
                    $('#mytable').DataTable();
                });

                $("#addCheckpointOption").click(function () {
                    var tr = $("<tr />");
                    var td = $("<td width=\"90%\" />").append($("#addCheckpoints").clone())
                    var removeButton = $("<td width=\"10%\" />").append($("<input class=\"form-control btn btn-danger\" type=\"button\" value=\"Remove\" />"));
                    removeButton.click(function () {
                        $(this).parent().remove();
                    });
                    tr.append(td);
                    tr.append(removeButton);
                    $("#addCheckpointsTable").append(tr);
                });

                $('#addMissionType').change(function () {
                    selection = $(this).val();

                    switch (selection) {
                        case 'Checkpoint':
                            $('#addCheckpointMission').show();
                            $('#addCheckpoints').prop('disabled', false);

                            $('#addStepsMission').hide();
                            $('#addSteps').prop('disabled', true);

                            break;
                        case 'Steps':
                            $('#addCheckpointMission').hide();
                            $('#addCheckpoints').prop('disabled', true);

                            $('#addStepsMission').show();
                            $('#addSteps').prop('disabled', false);

                            break;
                        case 'Special':
                            $('#addCheckpointMission').hide();
                            $('#addCheckpoints').prop('disabled', true);

                            $('#addStepsMission').hide();
                            $('#addSteps').prop('disabled', true);

                            break;
                    }
                });

                $("#editCheckpointOption").click(function () {
                    var tr = $("<tr />");
                    var td = $("<td width=\"90%\" />").append($("#addCheckpoints").clone())
                    var removeButton = $("<td width=\"10%\" />").append($("<input class=\"form-control btn btn-danger\" type=\"button\" value=\"Remove\" />"));
                    removeButton.click(function () {
                        $(this).parent().remove();
                    });
                    tr.append(td);
                    tr.append(removeButton);
                    $("#editCheckpointsTable").append(tr);
                });

                $(document).on("click", ".edit", function () {
                    var missionID = $(this).data('id');
                    var description = $(this).data('description');
                    var missionType = $(this).data('missiontype');
                    var steps = $(this).data('steps');
                    var points = $(this).data('points');
                    var missionCheckpoints = $(this).data('missioncheckpoints').toString();

                    $(".modal-body #editMissionID").val(missionID);
                    $(".modal-body #editDescription").val(description);
                    $(".modal-body #editPoints").val(points);
                    $(".modal-body #editSteps").val(steps);
                    $(".modal-body #editMissionType").val(missionType);

                    switch (missionType) {
                        case 'Checkpoint':
                            $('#editCheckpointMission').show();
                            $('#editCheckpoints').prop('disabled', false);

                            $("#editCheckpointsTable").empty();
                            var arr = missionCheckpoints.split(',');

                            var tr = $("<tr />");
                            var td = $("<td />").append($("#addCheckpoints").clone());
                            tr.append(td);
                            $("#editCheckpointsTable").append(tr);

                            for (var i = 1; i < arr.length; i++) {
                                tr = $("<tr />");
                                td = $("<td width=\"90%\" />").append($("#addCheckpoints").clone())
                                var removeButton = $("<td width=\"10%\" />").append($("<input class=\"form-control btn btn-danger\" type=\"button\" value=\"Remove\" />"));
                                removeButton.click(function () {
                                    $(this).parent().remove();
                                });
                                tr.append(td);
                                tr.append(removeButton);

                                $("#editCheckpointsTable").append(tr);
                            }

                            $('#editCheckpointsTable tr').each(function (i) {
                                var $tds = $(this).find('td option').each(function () {
                                    var temp = parseInt($(this).val());

                                    if (temp == arr[i]) {
                                        $(this).prop('selected', true);
                                    }
                                })
                            })

                            $('#editStepsMission').hide();
                            $('#editSteps').prop('disabled', true);

                            break;
                        case 'Steps':
                            $('#editCheckpointMission').hide();
                            $('#editCheckpoints').prop('disabled', true);

                            $('#editStepsMission').show();
                            $('#editSteps').prop('disabled', false);

                            break;
                        case 'Special':
                            $('#editCheckpointMission').hide();
                            $('#editCheckpoints').prop('disabled', true);

                            $('#editStepsMission').hide();
                            $('#editSteps').prop('disabled', true);

                            break;
                    }
                });

                $(document).on("click", ".delete", function () {
                    var missionID = $(this).data('id');
                    $(".modal-body #deleteMissionID").val(missionID);
                });

                $(document).on("click", ".generate", function () {
                    var missionID = $(this).data('id');
                    $(".generateQR").attr("src", "GenerateQRCodeServlet?missionID=" + missionID);
                    $(".saveQRCode").attr("href", "GenerateQRCodeServlet?missionID=" + missionID);
                });
            </script>
        </div>
    </body>
</html>

