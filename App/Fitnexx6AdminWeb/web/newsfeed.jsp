<%@include file="protect.jsp" %>
<%@page import="utility.DateAndTime"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="entity.NewsFeed"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.NewsFeedDAO"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Newsfeed</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li class="active">
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <legend><h1>Newsfeed</h1></legend>                              
                            <div class="table-responsive" style="overflow-x: hidden;">
                                <%                                    char staffType = loggedIn.getStaffType();

                                    if (staffType == 'A' || staffType == 'R') {
                                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                                    } else {
                                        String error = (String) session.getAttribute("newsfeedError");

                                        if (error != null) {
                                %>
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> <%=error%> </div>
                                <%
                                    }
                                %>
                                <a class="add btn btn-success btn-md" data-toggle="modal" href="#add">Add Newsfeed</a><br><br>
                                <%
                                    session.removeAttribute("newsfeedError");
                                    NewsFeedDAO newsFeedDAO = new NewsFeedDAO();
                                    ArrayList<NewsFeed> newsFeedList = newsFeedDAO.getNewsFeed();

                                    if (newsFeedList.isEmpty()) {
                                        out.println("<p><b><center>No newsfeed</center></b></p>");
                                    } else {
                                        for (NewsFeed newsFeed : newsFeedList) {
                                            out.println("<article><header>");
                                            out.println("<h2>" + newsFeed.getSubject() + "</h2>");
                                            out.println("<p>Published: <b>" + DateAndTime.dateToStringNewsFeedFormat(newsFeed.getDateTime()) + "</b> by <b>" + newsFeed.getEmail() + "</b></p>");
                                            out.println("<p>Category: <b>" + newsFeed.getCategory() + "</b></p>");
                                %>
                                <a class="edit btn btn-primary btn-sm" data-toggle="modal" data-datetime="<%=DateAndTime.dateToStringDBFormat(newsFeed.getDateTime())%>" data-email="<%=newsFeed.getEmail()%>" data-subject="<%=newsFeed.getSubject()%>" data-content="<%=newsFeed.getContent()%>" data-category="<%=newsFeed.getCategory()%>" href="#edit">Edit</a>
                                <a class="delete btn btn-danger btn-sm" data-toggle="modal" data-datetime="<%=DateAndTime.dateToStringDBFormat(newsFeed.getDateTime())%>" data-email="<%=newsFeed.getEmail()%>" href="#delete">Delete</a>
                                <%
                                                out.println("</header><br>");

                                                if (newsFeed.getNewsPicture() != null) {
                                                    out.println("<center><p><img src=ImageServlet?dateTime=" + DateAndTime.dateToStringDBFormat(newsFeed.getDateTime()).replace(" ", "%20") + "&email=" + newsFeed.getEmail() + " style=width:70%;></img></p></center><br>");
                                                }

                                                out.println("<p>" + newsFeed.getContent() + "</p><hr></article>");
                                            }
                                        }
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="AddNewsFeedServlet" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Newsfeed</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    Subject: <input class="form-control" type="text" placeholder="Subject" name="subject" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Category:
                                    <select class="form-control" name="category">
                                        <option value="News" selected>News</option>
                                        <option value="Event">Event</option>
                                    </select>
                                </div> 
                                <div class="form-group">
                                    News Picture: <input type="file" name="newsPicture" accept="image/png, image/jpeg" required="yes">
                                </div>
                                <div class="form-group">
                                    Content: <br>
                                    <textarea style="resize:none;min-width:100%" name="content" rows="8" required="yes"></textarea>
                                </div>                           
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="EditNewsFeedServlet" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Newsfeed</h4>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="dateTime" id="editDateTime" >
                                    <input class="form-control" type="hidden" name="email" id="editEmail" >
                                </div>
                                <div class="form-group">
                                    Subject: <input class="form-control" type="text" placeholder="Subject" name="subject" id="editSubject" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Category:
                                    <select class="form-control" name="category" id="editCategory">
                                        <option value="News">News</option>
                                        <option value="Event">Event</option>
                                    </select>
                                </div> 
                                <div class="form-group">
                                    News Picture: <input type="file" name="newsPicture" accept="image/png, image/jpeg">
                                </div>
                                <div class="form-group">
                                    Content: <br>
                                    <textarea style="resize:none;min-width:100%" name="content" id="editContent" rows="8" required="yes"></textarea>
                                </div>    
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Edit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="delete" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="DeleteNewsFeedServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Delete Newsfeed</h4>
                            </div>

                            <div class="modal-body">
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record? </div>
                                <input type="hidden" name="dateTime" id="deleteDateTime">
                                <input type="hidden" name="email" id="deleteEmail">
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <script>
                $(document).on("click", ".edit", function () {
                    var dateTime = $(this).data('datetime');
                    var email = $(this).data('email');
                    var subject = $(this).data('subject');
                    var content = $(this).data('content');
                    var category = $(this).data('category');

                    $(".modal-body #editDateTime").val(dateTime);
                    $(".modal-body #editEmail").val(email);
                    $(".modal-body #editSubject").val(subject);
                    $(".modal-body #editContent").val(content);
                    $(".modal-body #editCategory").val(category);
                });

                $(document).on("click", ".delete", function () {
                    var dateTime = $(this).data('datetime');
                    var email = $(this).data('email');
                    $(".modal-body #deleteDateTime").val(dateTime);
                    $(".modal-body #deleteEmail").val(email);
                });
            </script>
        </div>
    </body>
</html>

