<%@include file="protect.jsp" %>
<%@page import="java.util.TreeMap"%>
<%@page import="entity.Reward"%>
<%@page import="dao.RewardDAO"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Reward</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li class="active">
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
                
            <div id="page-wrapper">
                <!-- Help Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content - Help Content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Rewards Help</h4>
                            </div>
                            <div class="modal-body">
                                <p>This is the rewards page, where you can manage all rewards which will be displayed in the FitNexx mobile app. <br><br>
                                    <b>RewardID</b> - Every reward will auto-generate it's own ID <br>
                                    <b>Product Picture </b> - A picture of the reward <br>
                                    <b>Description </b> - This can the item name of the reward or a short phrase to describe the reward <br>
                                    <b>Active </b> - This indicates whether the reward is currently eligible for users to redeem. Non-active rewards will not be displayed in the rewards shop <br>
                                    <b>Price </b> - The amount of points which users need to accumulate to redeem the reward <br>
                                    <b>Quantity </b> - Quantity of reward remaining. Rewards with 0 quantity will not be shown in the reward store. <br>
                                    <b>Quantity Claimed </b> - The quantity which has been claimed by users
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1 class="page-header">
                                        Rewards
                                        <!-- Trigger the modal with a button -->
                                        <button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#myModal"><i class="fa fa-question" aria-hidden="true"></i></button>
                                    </h1>
                                </div>
                            </div>
                            <div class="table-responsive" style="overflow-x: hidden;">
                                <%                                    char staffType = loggedIn.getStaffType();

                                    if (staffType == 'A') {
                                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                                    } else {
                                        String error = (String) session.getAttribute("rewardError");

                                        if (error != null) {
                                %>
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> <%=error%> </div>
                                <%
                                    }

                                    session.removeAttribute("rewardError");
                                %>
                                <a class="add btn btn-success btn-md" data-toggle="modal" href="#add">Add Reward</a><br><br>
                                <%
                                    RewardDAO rewardDAO = new RewardDAO();
                                    TreeMap<Integer, Reward> rewardList = rewardDAO.getReward();
                                %>
                                <table id="mytable" class="table table-bordered table-striped">
                                    <thead>
                                    <th>Reward ID</th>
                                    <th>Product Picture</th>
                                    <th>Description</th>
                                    <th>Active</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Quantity Claimed</th>
                                    <th>Action</th>
                                    </thead>

                                    <tbody>
                                        <%
                                            for (Reward reward : rewardList.values()) {
                                                out.println("<tr>");
                                                out.println("<td align='middle'>" + reward.getRewardID() + "</td>");
                                                out.println("<td align='middle'><img src=ImageServlet?rewardID=" + reward.getRewardID() + " style=width:128px;height:128px;></img></td>");
                                                out.println("<td align='middle'>" + reward.getDescription() + "</td>");

                                                if (reward.isActive()) {
                                                    out.println("<td align='middle'>Yes</td>");
                                                } else {
                                                    out.println("<td align='middle'>No</td>");
                                                }

                                                out.println("<td align='middle'>" + reward.getPrice() + "</td>");
                                                out.println("<td align='middle'>" + reward.getQty() + "</td>");
                                                out.println("<td align='middle'>" + reward.getQtyClaimed() + "</td>");
                                        %>
                                    <td align="middle"><a class="edit btn btn-primary btn-sm" data-toggle="modal" data-id="<%=reward.getRewardID()%>" data-description="<%=reward.getDescription()%>" data-active="<%=reward.isActive()%>" data-price="<%=reward.getPrice()%>" data-quantity="<%=reward.getQty()%>" href="#edit">Edit</a>
                                        <a class="delete btn btn-danger btn-sm" data-toggle="modal" data-id="<%=reward.getRewardID()%>" href="#delete">Delete</a></td>
                                        <%
                                                    out.println("</tr>");
                                                }
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="AddRewardServlet" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Reward</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    Description: <input class="form-control" type="text" placeholder="Description" name="description" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Price: <input class="form-control" type="number" placeholder="Price" name="price" required="yes" min="0" max="1000000">
                                </div>
                                <div class="form-group">
                                    Quantity: <input class="form-control" type="number" placeholder="Quantity" name="quantity" required="yes" min="0" max="1000000">
                                </div>
                                <div class="form-group">
                                    Active:
                                    <select class="form-control" name="isActive">
                                        <option value="true" selected>Yes</option>
                                        <option value="false">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    Product Picture: <input type="file" name="productPicture" accept="image/png, image/jpeg" required="yes">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="EditRewardServlet" method="post" enctype="multipart/form-data">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Reward</h4>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" name="rewardID" id="editRewardID" >
                                </div>
                                <div class="form-group">
                                    Description: <input class="form-control" type="text" placeholder="Description" name="description" id="editDescription" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Price: <input class="form-control" type="number" placeholder="Price" name="price" id="editPrice" required="yes" min="0" max="1000000">
                                </div>
                                <div class="form-group">
                                    Quantity: <input class="form-control" type="number" placeholder="Quantity" name="quantity" id="editQuantity" required="yes" min="0" max="1000000">
                                </div>
                                <div class="form-group">
                                    Active:
                                    <select class="form-control" name="isActive" id="editIsActive">
                                        <option id="activeYes" value="true">Yes</option>
                                        <option id="activeNo" value="false">No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    Product Picture: <input type="file" name="productPicture" accept="image/png, image/jpeg">
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Edit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="delete" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="DeleteRewardServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Delete Reward</h4>
                            </div>

                            <div class="modal-body">
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record? </div>
                                <input type="hidden" name="rewardID" id="deleteRewardID">
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <script>
                $(document).ready(function () {
                    $('#mytable').DataTable();
                });

                $(document).on("click", ".edit", function () {
                    var rewardID = $(this).data('id');
                    var description = $(this).data('description');
                    var active = $(this).data('active');
                    var price = $(this).data('price');
                    var quantity = $(this).data('quantity');

                    $(".modal-body #editRewardID").val(rewardID);
                    $(".modal-body #editDescription").val(description);

                    if (active) {
                        $('#activeYes').prop('selected', true);
                    } else {
                        $('#activeNo').prop('selected', true);
                    }

                    $(".modal-body #editPrice").val(price);
                    $(".modal-body #editQuantity").val(quantity);
                });

                $(document).on("click", ".delete", function () {
                    var rewardID = $(this).data('id');
                    $(".modal-body #deleteRewardID").val(rewardID);
                });
            </script>
        </div>
    </body>
</html>

