<%@include file="protect.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.StaffDAO"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Staff</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs-3.3.5/jq-2.1.4,dt-1.10.8/datatables.min.css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/r/bs-3.3.5/jqc-1.11.3,dt-1.10.8/datatables.min.js"></script>
        <!-- Custom CSS -->
        <link href="css/sb-admin.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!--<a href="home.jsp" class="navbar-right"><img src="images/fitNexx6_logo_xsmall.png" height="50px"></a>-->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">FitNexx Admin</a>
                </div>

                <!-- Top Menu Items -->
                <%@ include file="topnavbar.jsp" %>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="/Fitnexx6AdminWeb/home.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#Missions"><i class="fa fa-fw fa-building"></i> Missions/Locations <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="Missions" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/building.jsp">Buildings</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/mission.jsp">Missions</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/checkpoint.jsp">Checkpoints</a>
                                </li> 
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-globe"></i> Adventures <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="/Fitnexx6AdminWeb/adventure.jsp"> Adventure</a>
                                </li>
                                <li>
                                    <a href="/Fitnexx6AdminWeb/clan.jsp"> Clan</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/reward.jsp"><i class="fa fa-fw fa-trophy"></i> Rewards</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/redemption.jsp"><i class="fa fa-fw fa-gift"></i> Redemption History</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/newsfeed.jsp"><i class="fa fa-fw fa-newspaper-o"></i> Newsfeed</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/feedback.jsp"><i class="fa fa-fw fa-comment-o"></i> Feedback</a>
                        </li>
                        <li class="active">
                            <a href="/Fitnexx6AdminWeb/staff.jsp"><i class="fa fa-fw fa-user"></i> Staff</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/user.jsp"><i class="fa fa-fw fa-users"></i> User</a>
                        </li>
                        <li>
                            <a href="/Fitnexx6AdminWeb/analytics/analyticshome.jsp"><i class="fa fa-fw fa-area-chart"></i> Analytics</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <legend><h1>Staff</h1></legend>  
                            <div class="table-responsive" style="overflow-x: hidden;">
                                <%                                    char staffType = loggedIn.getStaffType();

                                    StaffDAO staffDAO = new StaffDAO();
                                    ArrayList<Staff> staffList = staffDAO.getStaff();

                                    if (staffType == 'A' || staffType == 'R') {
                                        out.println("<p><b><center>You do not have permission to view this page</center></b></p>");
                                    } else {
                                        String staffError = (String) session.getAttribute("staffError");

                                        if (staffError != null) {
                                %>
                                <div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> <%=staffError%> </div>
                                <%
                                    }

                                    session.removeAttribute("staffError");
                                %>
                                <a class="add btn btn-success btn-md" data-toggle="modal" href="#add">Add Staff</a><br><br>

                                <table id="mytable" class="table table-bordered table-striped">
                                    <thead>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>Staff Type</th>
                                    <th>Active</th>
                                    <th>Action</th>                                   
                                    </thead>

                                    <tbody>
                                        <%
                                            for (Staff staff : staffList) {
                                                out.println("<tr>");
                                                out.println("<td align='middle'>" + staff.getEmail() + "</td>");
                                                out.println("<td align='middle'>" + staff.getName() + "</td>");
                                                out.println("<td align='middle'>" + staff.getStaffTypeName() + "</td>");
                                                
                                                if (staff.isActive()) {
                                                    out.println("<td align='middle'>Yes</td>");
                                                } else {
                                                    out.println("<td align='middle'>No</td>");
                                                }
                                        %>
                                    <td align="middle"><a class="edit btn btn-primary btn-sm" data-toggle="modal" data-email="<%=staff.getEmail()%>" data-name="<%=staff.getName()%>" data-stafftype="<%=staff.getStaffType()%>" data-active="<%=staff.isActive()%>"  href="#edit">Edit</a></td>
                                    <%
                                                out.println("</tr>");
                                            }
                                        }
                                    %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="add" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="AddStaffServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Add Staff</h4>
                            </div>

                            <div class="modal-body">  
                                <div class="form-group">
                                    Email: <input class="form-control" type="text" placeholder="Email" name="email" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Name: <input class="form-control" type="text" placeholder="Name" name="name" required="yes" maxlength="100">
                                </div>
                                <div class="form-group">
                                    Staff Type:
                                    <select class="form-control" name="staffType" >
                                        <option value="E" selected>Admin</option>
                                        <option value="A">Analytics</option>
                                        <option value="R">Redemption</option>
                                    </select>
                                </div>       
                                <div class="form-group">
                                    Active:
                                    <select class="form-control" name="isActive">
                                        <option value="true" selected>Yes</option>
                                        <option value="false">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Add</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <div class="modal fade" id="edit" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="EditStaffServlet" method="post">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                <h4 class="modal-title custom_align" id="Heading">Edit Staff</h4>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" id="oldStaffType" name="oldStaffType" >
                                </div>
                                <div class="form-group">
                                    Email: <input class="form-control" type="text" id="editEmail" placeholder="Email" name="email" readonly>
                                </div>
                                <div class="form-group">
                                    Name: <input class="form-control" type="text" id="editName" placeholder="Name" name="name" >
                                </div>
                                <div class="form-group">
                                    Staff Type:
                                    <select class="form-control" name="staffType" id="editStaffType" >
                                        <option value="E" selected>Admin</option>
                                        <option value="A">Analytics</option>
                                        <option value="R">Redemption</option>
                                    </select>
                                </div>   
                                <div class="form-group">
                                    Active:
                                    <select class="form-control" name="isActive" id="editIsActive">
                                        <option id="activeYes" value="true">Yes</option>
                                        <option id="activeNo" value="false">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Edit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content --> 
                </div>
                <!-- /.modal-dialog --> 
            </div>

            <script>
                $(document).ready(function () {
                    $('#mytable').DataTable();
                });

                $(document).on("click", ".edit", function () {
                    var email = $(this).data('email');
                    var name = $(this).data('name');
                    var stafftype = $(this).data('stafftype');
                    var active = $(this).data('active');

                    $(".modal-body #editEmail").val(email);
                    $(".modal-body #editName").val(name);
                    $(".modal-body #editStaffType").val(stafftype);
                    $(".modal-body #oldStaffType").val(stafftype);
                    
                    if (active) {
                        $('#activeYes').prop('selected', true);
                    } else {
                        $('#activeNo').prop('selected', true);
                    }
                });

            </script>
        </div>
    </body>
</html>

