<%-- 
    Document   : topnavbar
    Created on : Sep 30, 2016, 5:48:18 PM
    Author     : mervyn.lee.2014
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- Top Menu Items -->
<ul class="nav navbar-right top-nav">
    <li>
        <a href="/Fitnexx6AdminWeb/changePW.jsp"><i class="fa fa-fw fa-user"></i> <%=loggedIn.getStaffTypeName()%></a>
    </li>
    <li>
         <a href="/Fitnexx6AdminWeb/logout.jsp"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
    </li>
</ul>