package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import dao.MissionDAO;
import dao.UserDAO;
import entity.CheckPoint;
import entity.Mission;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;
import utility.CalculateValues;

@WebServlet(name = "CompleteMissionServlet", urlPatterns = {"/CompleteMissionServlet"})
public class CompleteMissionServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        String accessToken = data.get("accessToken");
        String email = data.get("email");
        String acceptIDString = data.get("acceptID");
        String missionIDString = data.get("missionID");
        String missionType = data.get("missionType");
        String pointsString = data.get("points");
        String stepsString = data.get("steps");
        String checkPointIDString = data.get("checkPointID");
        String latitudeString = data.get("lat");
        String longitudeString = data.get("long");
        String specialIDString = data.get("specialID");
        int missionID = 0;
        int acceptID = 0;
        int points = 0;
        int steps = 0;
        int checkPointID = 0;
        double latitude = 0;
        double longitude = 0;
        int specialID = 0;

        if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty()
                || acceptIDString == null || acceptIDString.isEmpty() || missionIDString == null
                || missionIDString.isEmpty() || missionType == null || missionType.isEmpty()) {
            error = "Please fill out all required fields";
        } else {
            try {
                UserDAO uDAO = new UserDAO();
                String sharedSecret = uDAO.getUserSS(email);

                if (!email.toLowerCase().equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                    error = "Token invalid or expired";
                } else {
                    try {
                        acceptID = Integer.parseInt(acceptIDString);
                        missionID = Integer.parseInt(missionIDString);
                        points = Integer.parseInt(pointsString);

                        if (missionType.equals("Steps")) {
                            steps = Integer.parseInt(stepsString);
                        } else if (missionType.equals("Checkpoint")) {
                            String[] temp = checkPointIDString.split("-");
                            String missionTypeOfEndingCP = temp[0];

                            if (missionTypeOfEndingCP.equals("Special")) {
                                error = "This QR code is for special missions only";
                            } else {
                                checkPointID = Integer.parseInt(temp[1]);
                                latitude = Double.parseDouble(latitudeString);
                                longitude = Double.parseDouble(longitudeString);
                            }
                        } else {
                            String[] temp = specialIDString.split("-");
                            String missionTypeOfQR = temp[0];

                            if (missionTypeOfQR.equals("Special")) {
                                specialID = Integer.parseInt(temp[1]);
                            } else {
                                error = "This QR code is not for special missions";
                            }
                        }

                    } catch (NumberFormatException e) {
                        error = "Please enter a valid input";
                    }

                    if (error == null) {
                        try {
                            MissionDAO mDAO = new MissionDAO();
                            Mission m = mDAO.getMissionByType(missionType, missionID, checkPointID);

                            if (missionType.equals("Steps")) {
                                if (steps >= m.getSteps()) {
                                    mDAO.completeMission(acceptID, email, points);
                                    result.addProperty("status", "success");
                                    out.print(gson.toJson(result));
                                    out.close();
                                    return;
                                } else {
                                    error = "You have not taken enough steps";
                                }
                            } else if (missionType.equals("Checkpoint")) {
                                // check checkpoint
                                double dist = CalculateValues.calculateDistance(latitude, longitude, m.getLatitude(), m.getLongitude());
                                if (dist <= 50) { // In km
                                    mDAO.createScan(email, checkPointID);
                                    ArrayList<CheckPoint> list = mDAO.checkCheckPoints(acceptID, missionID, checkPointID);

                                    if (list.isEmpty()) {
                                        mDAO.completeMission(acceptID, email, points);
                                        result.addProperty("status", "success");
                                        out.print(gson.toJson(result));
                                        out.close();
                                        return;
                                    } else {
                                        //error = "You still have " + list.size() + " checkpoints to go!";
                                        result.addProperty("status", "incomplete");
                                        JsonArray arr = new JsonArray();
                                        for (CheckPoint cp : list) {
                                            arr.add(new JsonPrimitive("ID " + cp.getCheckPointID() + " : " + cp.getLocationDetails()));
                                        }
                                        result.addProperty("message", "You still have " + list.size() + " checkpoints to go!");
                                        result.add("remainingCheckpoints", arr);
                                        out.print(gson.toJson(result));
                                        out.close();
                                        return;
                                    }
                                } else {
                                    error = "You are too far from the checkpoint";
                                }
                            } else {
                                if (missionID == specialID) {
                                    mDAO.completeMission(acceptID, email, points);
                                    result.addProperty("status", "success");
                                    out.print(gson.toJson(result));
                                    out.close();
                                    return;
                                } else {
                                    error = "This is not the right QR code";
                                }
                            }
                        } catch (Exception e) {
                            if (e.getMessage().equals("wrong ending point")) {
                                error = "This is not the right ending checkpoint";
                            } else {
                                error = e.getMessage();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                // log error
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
