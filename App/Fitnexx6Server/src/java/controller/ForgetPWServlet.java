package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import dao.UserDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;
import utility.Email;
import utility.EmailValidator;

@WebServlet(name = "ForgetPWServlet", urlPatterns = {"/ForgetPWServlet"})
public class ForgetPWServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap

        JsonObject result = new JsonObject();
        String error = null;

        final String email = data.get("email");

        if (email == null || email.isEmpty()) {
            error = "Please fill out all required fields";
        } else if (!EmailValidator.validate(email)) {
            error = "Please enter a valid email address";
        } else {
            try {
                UserDAO userDAO = new UserDAO();
                String sharedSecret = userDAO.getUserSS(email);

                if (sharedSecret != null) {
                    final String activationCode = Cryptography.generateAccessToken(email.toLowerCase(), sharedSecret);

                    // Setting up and sending of email takes alot of time, so do in separate thread
                    // to not keep user waiting for response. Good idea?
                    Thread t1 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // Email activationCode to user to activate account
                            String content = "Please click on the following link to reset your password! \n"
                                    + "http://fypfitnexx-leroyce.rhcloud.com/Fitnexx6Server/ResetPWServlet?email=" + email.toLowerCase() + "&activationCode=" + activationCode;
                            //String content = "http://localhost:8084/Fitnexx6Server/ResetPWServlet?email=" + email.toLowerCase() + "&activationCode=" + activationCode;
                            Email.sendMail(email, "", "Your login details for Fitnexx", content);
                        }
                    });

                    t1.start();
                }

                result.addProperty("status", "success");
                result.addProperty("message", "Your username and details about how to reset your password have been sent to you by email");
                out.print(gson.toJson(result));
                out.close();
                return;
            } catch (Exception e) {
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
