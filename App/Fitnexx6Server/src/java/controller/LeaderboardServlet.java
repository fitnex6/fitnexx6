package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dao.ClanDAO;
import dao.LeaderboardDAO;
import dao.MissionDAO;
import dao.UserDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import utility.Cryptography;

@WebServlet(name = "LeaderboardServlet", urlPatterns = {"/LeaderboardServlet"})
public class LeaderboardServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        String accessToken = data.get("accessToken");
        String email = data.get("email");

        if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty()) {
            error = "Please fill out all required fields";
        } else {
            try {
                UserDAO userDAO = new UserDAO();
                String sharedSecret = userDAO.getUserSS(email);
                email = email.toLowerCase();

                if (!email.equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                    error = "Token invalid or expired";
                } else {
                    LeaderboardDAO leaderboardDAO = new LeaderboardDAO();

                    Calendar c = Calendar.getInstance();
                    Date currentDate = new Date(c.getTimeInMillis()); //today's date

                    /*
                     c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                     Date startDate = new Date(c.getTimeInMillis()); //monday's date
                     c.add(Calendar.DATE, 6);
                     Date endDate = new Date(c.getTimeInMillis()); //sunday's date
                     */
                    //ArrayList<String> dayUsers = leaderboardDAO.getUniqueUsersByDay(currentDate);
                    //ArrayList<String> weekUsers = leaderboardDAO.getUniqueUsersByWeek(startDate, endDate);
//                    ArrayList<String> monthUsers = leaderboardDAO.getUniqueUsersByMonth(currentDate); //removed by mervyn
                    //HashMap<String, Integer> dayMap = leaderboardDAO.getDayTop(dayUsers, currentDate);
                    //HashMap<String, Integer> weekMap = leaderboardDAO.getWeekTop(weekUsers, startDate, endDate);
                    HashMap<String, Integer> monthMap = leaderboardDAO.getMonthTop();

                    //Map<String, Integer> sortedDayMap = sortByComparator(dayMap);
                    //Map<String, Integer> sortedWeekMap = sortByComparator(weekMap);
                    Map<String, Integer> sortedMonthMap = sortByComparator(monthMap);

                    HashMap<String, String> userMap = userDAO.getNameOfUser();
                    HashMap<String, String> profilePicMap = userDAO.getProfilePic();

                    //JsonArray dayArr = new JsonArray();
                    //JsonArray weekArr = new JsonArray();
                    JsonArray userMonthArr = new JsonArray();

                    /*
                     for (String user : sortedDayMap.keySet()) {
                     if (count++ < 10) {
                     int steps = sortedDayMap.get(user);
                     JsonObject dayObject = new JsonObject();
                     dayObject.addProperty("rank", count);
                     dayObject.addProperty("name", userMap.get(user));
                     dayObject.addProperty("profilePic", profilePicMap.get(user));
                     dayObject.addProperty("steps", steps);
                     dayArr.add(dayObject);
                     } else {
                     break;
                     }
                     }

                     count = 0;

                     for (String user : sortedWeekMap.keySet()) {
                     if (count++ < 10) {
                     int steps = sortedWeekMap.get(user);
                     JsonObject weekObject = new JsonObject();
                     weekObject.addProperty("rank", count);
                     weekObject.addProperty("name", userMap.get(user));
                     weekObject.addProperty("profilePic", profilePicMap.get(user));
                     weekObject.addProperty("steps", steps);
                     weekArr.add(weekObject);
                     } else {
                     break;
                     }
                     }
                     */
                    int count = 0;
                    boolean exist = false;

                    for (String user : sortedMonthMap.keySet()) {
                        if (count++ < 10) {
                            if (user.equals(email)) {
                                exist = true;
                            }

                            int steps = sortedMonthMap.get(user);
                            JsonObject monthObject = new JsonObject();
                            monthObject.addProperty("rank", count);
                            monthObject.addProperty("email", user);
                            monthObject.addProperty("name", userMap.get(user));
                            monthObject.addProperty("profilePic", profilePicMap.get(user));
                            monthObject.addProperty("steps", steps);
                            userMonthArr.add(monthObject);
                        } else if (exist) {
                            break;
                        } else if (user.equals(email)) {
                            int steps = sortedMonthMap.get(user);
                            JsonObject monthObject = new JsonObject();
                            monthObject.addProperty("rank", count);
                            monthObject.addProperty("email", user);
                            monthObject.addProperty("name", userMap.get(user));
                            monthObject.addProperty("profilePic", profilePicMap.get(user));
                            monthObject.addProperty("steps", steps);
                            userMonthArr.add(monthObject);
                            break;
                        }
                    }

                    MissionDAO missionDAO = new MissionDAO();
                    ClanDAO clanDAO = new ClanDAO();

                    HashMap<String, Integer> clanStatsMap = missionDAO.getClanStats();
                    JsonArray clanMonthArr = new JsonArray();

                    if (!clanStatsMap.isEmpty()) {
                        Map<String, Integer> sortedClanMap = sortByComparator(clanStatsMap);
                        HashMap<String, byte[]> clanPictureMap = clanDAO.getClanPic();
                        HashMap<String, String> userClanMap = userDAO.getUserClan();

                        count = 1;

                        for (String clan : sortedClanMap.keySet()) {
                            int missionsCompleted = clanStatsMap.get(clan);
                            JsonObject monthObject = new JsonObject();
                            monthObject.addProperty("rank", count++);
                            monthObject.addProperty("clan", clan);
                            monthObject.addProperty("inClan", userClanMap.get(email).equals(clan));
                            monthObject.addProperty("clanPicture", DatatypeConverter.printBase64Binary(clanPictureMap.get(clan)));
                            monthObject.addProperty("missionsCompleted", missionsCompleted);
                            clanMonthArr.add(monthObject);
                        }
                    }

                    result.addProperty("status", "success");
                    //result.add("day", dayArr);
                    //result.add("week", weekArr);
                    result.add("userMonth", userMonthArr);
                    result.add("clanMonth", clanMonthArr);
                    out.print(gson.toJson(result));
                    out.close();
                    return;
                }
            } catch (Exception e) {
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

    protected Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) {
        // Convert Map to List
        List<Map.Entry<String, Integer>> list
                = new LinkedList<>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                    Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // Convert sorted map back to a Map
        Map<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
