package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import dao.AchievementDAO;
import dao.UserDAO;
import entity.Achievement;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;

@WebServlet(name = "LogStepsServlet", urlPatterns = {"/LogStepsServlet"})
public class LogStepsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        String accessToken = data.get("accessToken");
        String email = data.get("email");
        String stepsString = data.get("steps");
        String heightString = data.get("height");
        String weightString = data.get("weight");
        String lastActiveDateInMsString = data.get("lastActiveDateInMs");
        int steps = 0;
        double height = 0;
        double weight = 0;
        long lastActiveDateInMs = 0;

        if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty()
                || stepsString == null || stepsString.isEmpty()) {
            // || heightString == null || heightString.isEmpty()
            //    || weightString == null || weightString.isEmpty()
            error = "Please fill out all required fields";
        } else {
            try {
                UserDAO uDAO = new UserDAO();
                String sharedSecret = uDAO.getUserSS(email);

                if (!email.toLowerCase().equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                    error = "Token invalid or expired";
                } else {
                    try {
                        steps = Integer.parseInt(stepsString);
                        lastActiveDateInMs = Long.parseLong(lastActiveDateInMsString);
                        //height = Double.parseDouble(heightString);
                        //weight = Double.parseDouble(weightString);
                    } catch (NumberFormatException e) {
                        error = "Please enter a valid number for steps";
                    }

                    if (error == null) {
                        try {
                            //double caloriesBurnt = Math.round(CalculateValues.calculateCalories(steps, height, weight) * 1000000.0) / 1000000.0;
                            uDAO.logSteps(email.toLowerCase(), steps, 0, lastActiveDateInMs);
                            String clanName = uDAO.getClanName(email);
                            AchievementDAO aDAO = new AchievementDAO();
                            Achievement achieve = aDAO.checkRankUp(email);
                            result.addProperty("status", "success");
                            result.addProperty("rankUped", achieve.isRankUp());
                            result.addProperty("tier", achieve.getTier());
                            result.addProperty("tierPic", achieve.getTierPic());
                            result.addProperty("criterionToNextTier", achieve.getCriterionToNextTier());
                            if(clanName == null){
                                result.addProperty("clanName", "N/A"); 
                            }else{
                                result.addProperty("clanName", clanName); 
                            }
                            
                            out.print(gson.toJson(result));
                            out.close();
                            return;
                        } catch (Exception e) {
                            // catch checkpoint or email error
                            error = e.getMessage();
                        }
                    }
                }
            } catch (Exception e) {
                // log error
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
