package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dao.NewsFeedDAO;
import dao.UserDAO;
import entity.NewsFeed;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import utility.Cryptography;
import utility.DateAndTime;

@WebServlet(name = "NewsFeedServlet", urlPatterns = {"/NewsFeedServlet"})
public class NewsFeedServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        String accessToken = data.get("accessToken");
        String email = data.get("email");

        if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty()) {
            error = "Please fill out all required fields";
        } else {
            try {
                UserDAO userDAO = new UserDAO();
                String sharedSecret = userDAO.getUserSS(email);

                if (!email.toLowerCase().equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                    error = "Token invalid or expired";
                } else {
                    NewsFeedDAO newsFeedDAO = new NewsFeedDAO();

                    Calendar c = Calendar.getInstance();
                    Date currentDate = new Date(c.getTimeInMillis()); //today's date

                    ArrayList<NewsFeed> newsFeedList = newsFeedDAO.getNewsFeedList(currentDate);

                    JsonArray newsFeedArr = new JsonArray();

                    for (NewsFeed newsFeed : newsFeedList) {
                        JsonObject newsFeedObject = new JsonObject();
                        newsFeedObject.addProperty("date", DateAndTime.dateToStringNewsFeedFormat(newsFeed.getDateTime()));
                        newsFeedObject.addProperty("email", newsFeed.getEmail());
                        newsFeedObject.addProperty("subject", newsFeed.getSubject());
                        newsFeedObject.addProperty("content", newsFeed.getContent());
                        newsFeedObject.addProperty("newsPicture", DatatypeConverter.printBase64Binary(newsFeed.getNewsPicture()));
                        newsFeedObject.addProperty("category", newsFeed.getCategory());
                        newsFeedArr.add(newsFeedObject);
                    }

                    result.addProperty("status", "success");
                    result.add("newsfeed", newsFeedArr);
                    out.print(gson.toJson(result));
                    out.close();
                    return;
                }
            } catch (Exception e) {
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
