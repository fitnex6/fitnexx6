package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import dao.RewardDAO;
import dao.UserDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;

@WebServlet(name = "RedeemRewardServlet", urlPatterns = {"/RedeemRewardServlet"})
public class RedeemRewardServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        String accessToken = data.get("accessToken");
        String email = data.get("email");
        String rewardIDString = data.get("rewardID");
        String staffPinString = data.get("staffPin");
        String priceString = data.get("price");
        int rewardID = 0;
        int staffPin = 0;
        int price = 0;

        if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty() || rewardIDString == null
                || rewardIDString.isEmpty() || staffPinString == null || staffPinString.isEmpty()
                || priceString == null || priceString.isEmpty()) {
            error = "Please fill out all required fields";
        } else {
            try {
                UserDAO uDAO = new UserDAO();
                String sharedSecret = uDAO.getUserSS(email);

                if (!email.toLowerCase().equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                    error = "Token invalid or expired";
                } else {
                    try {
                        rewardID = Integer.parseInt(rewardIDString);
                        staffPin = Integer.parseInt(staffPinString);
                        price = Integer.parseInt(priceString);
                    } catch (NumberFormatException e) {
                        error = "Please enter a valid input";
                    }

                    if (error == null) {
                        try {
                            RewardDAO rDAO = new RewardDAO();
                            rDAO.redeem(email.toLowerCase(), rewardID, staffPin, price);

                            result.addProperty("status", "success");
                            out.print(gson.toJson(result));
                            out.close();
                            return;
                        } catch (Exception e) {
                            if (e.getMessage().contains("Not enough points")) {
                                error = "Not enough points to redeem this reward";
                            } else if (e.getMessage().contains("Reward is not available")) {
                                error = "Reward is not available";
                            } else {
                                error = "Please enter a correct staff PIN";
                            }
                        }
                    }
                }
            } catch (Exception e) {
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
