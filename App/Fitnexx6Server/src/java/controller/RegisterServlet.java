package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import dao.UserDAO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;
import utility.DateAndTime;
import utility.Email;
import utility.EmailValidator;

@WebServlet(name = "RegisterServlet", urlPatterns = {"/RegisterServlet"})
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        final String email = data.get("email");
        String name = data.get("name");
        String passWord = data.get("passWord");
        String passWord2 = data.get("passWord2");
        String DOBString = data.get("DOB");
        String faculty = data.get("faculty");
        String gender = data.get("gender");
        Date DOB = null;

        if (email == null || email.isEmpty() || name == null || name.isEmpty() || passWord == null || passWord.isEmpty()
                || passWord2 == null || passWord2.isEmpty()
                || faculty == null || faculty.isEmpty()) {
            error = "Please fill out all required fields";
        } else {
            // how else to check email format?
            if (!EmailValidator.validate(email)) {
                error = "Please use a valid email";
            } else if (!passWord.matches("^(?=.*\\d)(?i)(?=.*[a-z])[^\\s]{8,}$")) {
                error = "Password must be 8 or more alphanumeric characters";
            } else if (!passWord.equals(passWord2)) {
                error = "Passwords must match";
            } else if (!faculty.matches("(Business & Accountancy|Design & Environment|Engineering|Film & Media Studies|"
                    + "Health Sciences|Humanities & Social Sciences|InfoComm Technology|Interdisciplinary Studies|"
                    + "Life Sciences & Chemical Technology|Others)")) {
                error = "Please choose a valid faculty";
            } else {
                try {
                    if(gender != null && !gender.isEmpty()){
                        if (!gender.substring(0, 1).matches("(?i)[MF]")) {
                            error = "Please choose a valid gender";
                        }else{
                            gender = gender.substring(0, 1);
                        }
                    }
                    
                    if(DOBString != null && !DOBString.isEmpty()){
                        DOB = DateAndTime.stringToDate(DOBString);
                    }                    
                } catch (ParseException e) {
                    error = "Please enter the correct date format";
                }
            }

            if (error == null) {
                try {
                    UserDAO uDAO = new UserDAO();
                    String passWordHash = Cryptography.hash(passWord);
                    String sharedSecret = Cryptography.generateRandString(32); // Generate a random String of length 32
                    final String activationCode = Cryptography.generateAccessToken(email.toLowerCase(), sharedSecret);
                    uDAO.register(email.toLowerCase(), name, passWordHash, DOB, faculty, gender, sharedSecret);

                    // Setting up and sending of email takes alot of time, so do in separate thread
                    // to not keep user waiting for response. Good idea?
                    Thread t1 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // Email activationCode to user to activate account
                            String content = "Please click on the following link to activate your account! \n"
                                    + "http://fypfitnexx-leroyce.rhcloud.com/Fitnexx6Server/ActivationServlet?email=" + email.toLowerCase() + "&activationCode=" + activationCode;
                            //String content = "http://localhost:8084/Fitnexx6Server/ActivationServlet?email=" + email.toLowerCase() + "&activationCode=" + activationCode;
                            Email.sendMail(email, "", "Account Activation", content); // Temporary method till we get NP's email server details
                        }
                    });
                    t1.start();

                    result.addProperty("status", "success");
                    out.print(gson.toJson(result));
                    out.close();
                    return;
                } catch (Exception e) {
                    if (e.getMessage().contains("Duplicate entry")) {
                        error = "Email has already been taken";
                    } else {
                        error = "An error has occured. Please try again later";
                    }
                }
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
