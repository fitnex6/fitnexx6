/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import dao.MissionDAO;
import dao.UserDAO;
import entity.CheckPoint;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;

/**
 *
 * @author jeremy.seow.2014
 */
@WebServlet(name = "RemainingCheckpointsServlet", urlPatterns = {"/RemainingCheckpointsServlet"})
public class RemainingCheckpointsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            StringBuilder sb = new StringBuilder();
            BufferedReader br = request.getReader(); // Reads the body of POST request

            String str = null;
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
            br.close();

            Gson gson = new GsonBuilder().create();
            HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
            // hence we convert to hashmap
            JsonObject result = new JsonObject();
            String error = null;

            String accessToken = data.get("accessToken");
            String email = data.get("email");
            String acceptIDString = data.get("acceptID");
            String missionIDString = data.get("missionID");
            int missionID = 0;
            int acceptID = 0;

            if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty()
                    || acceptIDString == null || acceptIDString.isEmpty() || missionIDString == null
                    || missionIDString.isEmpty()) {
                error = "Please fill out all required fields";
            } else {
                try {
                    UserDAO uDAO = new UserDAO();
                    String sharedSecret = uDAO.getUserSS(email);

                    if (!email.toLowerCase().equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                        error = "Token invalid or expired";
                    } else {
                        try {
                            acceptID = Integer.parseInt(acceptIDString);
                            missionID = Integer.parseInt(missionIDString);
                        } catch (NumberFormatException e) {
                            error = "Please enter a valid input";
                        }

                        if (error == null) {
                            MissionDAO mDAO = new MissionDAO();

                            ArrayList<CheckPoint> list = mDAO.checkCheckPoints(acceptID, missionID, 0);

                            if (list.isEmpty()) {
                                result.addProperty("status", "success");
                                out.print(gson.toJson(result));
                                out.close();
                                return;
                            } else {
                                result.addProperty("status", "incomplete");
                                JsonArray arr = new JsonArray();
                                for (CheckPoint cp : list) {
                                    arr.add(new JsonPrimitive("ID " + cp.getCheckPointID() + " : " + cp.getLocationDetails()));
                                }
                                result.addProperty("message", "You still have " + list.size() + " checkpoints to go!");
                                result.add("remainingCheckpoints", arr);
                                out.print(gson.toJson(result));
                                out.close();
                                return;
                            }
                        }
                    }
                } catch (Exception e) {
                    // log error
                    error = "An error has occured. Please try again later";
                }
            }
            result.addProperty("status", "error");
            result.addProperty("errors", error);
            out.print(gson.toJson(result));
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
