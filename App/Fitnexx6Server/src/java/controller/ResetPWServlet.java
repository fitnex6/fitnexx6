package controller;

import dao.UserDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utility.Cryptography;
import utility.Email;

@WebServlet(name = "ResetPWServlet", urlPatterns = {"/ResetPWServlet"})
public class ResetPWServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String message = null;
        final String email = request.getParameter("email");
        String activationCode = request.getParameter("activationCode");

        if (email != null && !email.isEmpty() && activationCode != null && !activationCode.isEmpty()) {
            try {
                UserDAO userDAO = new UserDAO();
                String sharedSecret = userDAO.getUserSS(email);

                if (email.toLowerCase().equals(Cryptography.verifyAccessToken(activationCode, sharedSecret))) {
                    final String newPassword = Cryptography.generateRandString(8);
                    String newPasswordHash = Cryptography.hash(newPassword);

                    userDAO.updatePW(email, newPasswordHash, null);

                    // Setting up and sending of email takes alot of time, so do in separate thread
                    // to not keep user waiting for response. Good idea?
                    Thread t1 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            // Email activationCode to user to activate account
                            String content = "You have successfully reset your password! Here are your login details: \n"
                                    + "Username: " + email.toLowerCase() + "\nPassword: " + newPassword;
                            Email.sendMail(email, "", "Your new password for Fitnexx", content);
                        }
                    });

                    t1.start();

                    message = "You have successfully reset your password. Please check your email for your new login details";
                } else {
                    message = "Link expired";
                }
            } catch (Exception e) {
                message = "An error has occured. Please try again later";
            }
        }

        HttpSession session = request.getSession();
        session.setAttribute("message", message);
        response.sendRedirect("resetPW.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
