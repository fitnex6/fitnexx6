package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dao.MissionDAO;
import dao.UserDAO;
import entity.ClanMissionCount;
import entity.Mission;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;

@WebServlet(name = "ScanServlet", urlPatterns = {"/ScanServlet"})
public class ScanServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        String accessToken = data.get("accessToken");
        String email = data.get("email");
        String IDString = data.get("checkPointID");
        String missionType = null;
        int ID = 0;

        if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty() || IDString == null || IDString.isEmpty()) {
            error = "Please fill out all required fields";
        } else {
            try {
                UserDAO uDAO = new UserDAO();
                String sharedSecret = uDAO.getUserSS(email);

                if (!email.toLowerCase().equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                    error = "Token invalid or expired";
                } else {
                    try {
                        String[] temp = IDString.split("-");
                        missionType = temp[0];
                        ID = Integer.parseInt(temp[1]);
                    } catch (Exception e) {
                        error = "This is not a valid QR code";
                    }

                    if (error == null) {
                        try {
                            MissionDAO mDAO = new MissionDAO();
                            Mission m = null;
                            if (missionType.equals("Special")) {
                                m = mDAO.getMissionByMissionID(ID);
                            } else {
                                mDAO.createScan(email, ID);
                                m = mDAO.getMissionByCheckPoint(ID);
                                // get the info for graph; clan name and missionCount
                            }
                            ArrayList<ClanMissionCount> list = mDAO.getClanMissionCount(m.getMissionID());
                            JsonArray arr = new JsonArray();
                            for (ClanMissionCount cmc : list) {
                                JsonObject temp = new JsonObject();
                                temp.addProperty("label", cmc.getClanName());
                                temp.addProperty("value", cmc.getMissionCount());
                                arr.add(temp);
                            }

                            result.addProperty("status", "success");
                            result.addProperty("missionID", m.getMissionID());
                            result.addProperty("description", m.getDescription());
                            result.addProperty("missionType", m.getMissionType());
                            result.addProperty("points", m.getPoints());
                            result.add("missionCount", arr);
                            out.print(gson.toJson(result));
                            out.close();
                            return;
                        } catch (Exception e) {
                            // catch checkpoint or email error
                            if (e.getMessage().contains("foreign key constraint")) {
                                error = "This is not the right QR code";
                            } else {
                                error = e.getMessage();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                // log error
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
