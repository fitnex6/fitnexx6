package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import dao.MissionDAO;
import dao.UserDAO;
import entity.AcceptedMission;
import entity.AvailableMission;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;
import utility.DateAndTime;

@WebServlet(name = "ViewMissionsServlet", urlPatterns = {"/ViewMissionsServlet"})
public class ViewMissionsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        String accessToken = data.get("accessToken");
        String email = data.get("email");

        if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty()) {
            error = "Please fill out all required fields";
        } else {
            try {
                UserDAO uDAO = new UserDAO();
                String sharedSecret = uDAO.getUserSS(email);

                if (!email.toLowerCase().equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                    error = "Token invalid or expired";
                } else {
                    JsonArray arr = new JsonArray();
                    JsonArray arr2 = new JsonArray();
                    MissionDAO mDAO = new MissionDAO();
                    ArrayList<AcceptedMission> missionList = mDAO.getMissionList(email);
                    ArrayList<AvailableMission> availableList = mDAO.getAvailableMissions();

                    if (!missionList.isEmpty()) {
                        for (AcceptedMission am : missionList) {
                            JsonObject temp = new JsonObject();
                            temp.addProperty("acceptID", am.getAcceptID());
                            temp.addProperty("missionID", am.getMissionID());
                            temp.addProperty("dateTimeAccepted", DateAndTime.dateToString(am.getDateTimeAccepted()));

                            // Will NPE as uncompleted missions will have null for dateTimeCompleted
//                            if (am.getDateTimeCompleted() == null) {
//                                temp.addProperty("dateTimeCompleted", "--");
//                            } else {
//                                temp.addProperty("dateTimeCompleted", DateAndTime.dateToString(am.getDateTimeCompleted()));
//                            }
                            //temp.addProperty("isCompleted", am.isIsCompleted());
                            temp.addProperty("description", am.getDescription());
                            temp.addProperty("missionType", am.getMissionType());
                            temp.addProperty("steps", am.getSteps());
                            temp.addProperty("points", am.getPoints());
                            arr.add(temp);
                        }
                    }

                    if (!availableList.isEmpty()) {
                        for (AvailableMission am : availableList) {
                            JsonObject temp = new JsonObject();
                            temp.addProperty("missionType", am.getMissionType());
                            temp.addProperty("checkpointID", am.getCheckPointID());
                            temp.addProperty("locationDetails", am.getLocationDetails());
                            temp.addProperty("blk", am.getBuildingID());
                            temp.addProperty("name", am.getName());
                            arr2.add(temp);
                        }
                    }
                    
                    int currentDayMissionCount = mDAO.getCurrentDayMissionCount(email);
                    JsonObject missionCountJO = new JsonObject();
                    missionCountJO.addProperty("currentDay",currentDayMissionCount);
                    
                    result.addProperty("status", "success");
                    result.add("acceptedMissions", arr);
                    result.add("availableMissions", arr2);
                    result.add("missionCount",missionCountJO);
                    out.print(gson.toJson(result));
                    out.close();
                    return;
                }
            } catch (Exception e) {
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
