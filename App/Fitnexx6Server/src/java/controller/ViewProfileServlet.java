package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import dao.UserDAO;
import entity.User;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utility.Cryptography;
import utility.DateAndTime;

@WebServlet(name = "ViewProfileServlet", urlPatterns = {"/ViewProfileServlet"})
public class ViewProfileServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        StringBuilder sb = new StringBuilder();
        BufferedReader br = request.getReader(); // Reads the body of POST request

        String str = null;
        while ((str = br.readLine()) != null) {
            sb.append(str);
        }
        br.close();

        Gson gson = new GsonBuilder().create();
        HashMap<String, String> data = gson.fromJson(sb.toString(), HashMap.class); // Data in POST body is in name-value format,
        // hence we convert to hashmap
        JsonObject result = new JsonObject();
        String error = null;

        String accessToken = data.get("accessToken");
        String email = data.get("email");

        if (accessToken == null || accessToken.isEmpty() || email == null || email.isEmpty()) {
            error = "Please fill out all required fields";
        } else {
            try {
                UserDAO uDAO = new UserDAO();
                String sharedSecret = uDAO.getUserSS(email);

                if (!email.toLowerCase().equals(Cryptography.verifyAccessToken(accessToken, sharedSecret))) {
                    error = "Token invalid or expired";
                } else {
                    User loggedIn = uDAO.retrieveProfile(email);

                    if (loggedIn == null) {
                        error = "User does not exist or account not activated";
                    } else {
                        result.addProperty("status", "success");
                        result.addProperty("email", loggedIn.getEmail());
                        result.addProperty("name", loggedIn.getName());
                        result.addProperty("DOB", DateAndTime.dateToString(loggedIn.getDOB()));
                        result.addProperty("age", loggedIn.getAge());

                        // To send image over HTTP, we can set Content-Type to image/jpeg. However, as we are sending text along with image,
                        // the text and image will be mixed together and cannot be intepreted.
                        // We can either 
                        //      1. set Content-Type to multipart and separate the text from the image within the response using boundaries
                        //      2. or we can encode the image to Base64 and send over as a String and decode at the receiving end to binary
                        // The 2nd method is chosen because we can send the image as part of a JSON message, hence it will be easier to manipulate
                        result.addProperty("profilePic", loggedIn.getProfilePic()); // if profilePic is null, will have error
                        result.addProperty("faculty", loggedIn.getFaculty());
                        result.addProperty("gender", loggedIn.getGender());
                        result.addProperty("height", loggedIn.getHeight());
                        result.addProperty("weight", loggedIn.getWeight());
                        result.addProperty("BMI", loggedIn.getBMI());
                        result.addProperty("points", loggedIn.getPoints());

                        out.print(gson.toJson(result));
                        out.close();
                        return;
                    }
                }
            } catch (Exception e) {
                // Log error
                error = "An error has occured. Please try again later";
            }
        }

        result.addProperty("status", "error");
        result.addProperty("errors", error);
        out.print(gson.toJson(result));
        out.close();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
