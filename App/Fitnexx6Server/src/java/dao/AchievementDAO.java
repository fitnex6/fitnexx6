package dao;

import entity.Achievement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class AchievementDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;
    
    public Achievement checkRankUp(String email) throws Exception {
        try {
            boolean rankUp = true;
            String tier = null;
            String tierPic = null;
            int criterionToNextTier = 0;
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "UPDATE user AS u, (SELECT tier "
                    + "FROM achievement "
                    + "WHERE criterion <= (SELECT SUM(steps) from steps_history GROUP BY email HAVING email = ?) "
                    + "ORDER BY criterion DESC "
                    + "LIMIT 1 ) AS nt SET u.tier = nt.tier "
                    + "WHERE email = ? AND u.tier <> nt.tier";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setString(2, email);
            int rowsUpdated = stmt.executeUpdate();

            if (rowsUpdated == 0) {
                rankUp = false;
            }

            sql = "SELECT u.tier, tier_picture FROM user AS u INNER JOIN achievement AS a "
                    + "ON u.tier = a.tier WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            rs = stmt.executeQuery();

            if (rs.next()) {
                tier = rs.getString("u.tier");
                tierPic = rs.getString("tier_picture");
            }

            sql = "SELECT criterion FROM achievement WHERE criterion > (SELECT criterion FROM achievement a INNER JOIN user u ON a.tier = u.tier WHERE email = ?) ORDER BY criterion ASC LIMIT 1";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            rs = stmt.executeQuery();

            if (rs.next()) {
                criterionToNextTier = rs.getInt("criterion");
            }

            Achievement achieve = new Achievement(rankUp, tier, tierPic, criterionToNextTier);
            
            return achieve;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
