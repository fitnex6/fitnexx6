package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

public class ClanDAO {
    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;
    
    public HashMap<String, byte[]> getClanPic() throws Exception {
        try {
            HashMap<String, byte[]> resultMap = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT clan_name, clan_picture FROM clan";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String clanName = rs.getString("clan_name");
                byte[] clanPicture = rs.getBytes("clan_picture");

                resultMap.put(clanName, clanPicture);
            }

            return resultMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
