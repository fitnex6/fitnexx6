package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class LeaderboardDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public ArrayList<String> getUniqueUsersByDay(Date date) throws Exception {
        try {
            ArrayList<String> userList = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT DISTINCT email FROM steps_history "
                    + "WHERE CAST(dateTime AS date) = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(date.getTime()));
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");

                userList.add(email);
            }

            return userList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<String> getUniqueUsersByWeek(Date startDate, Date endDate) throws Exception {
        try {
            ArrayList<String> userList = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT DISTINCT email FROM steps_history "
                    + "WHERE CAST(dateTime AS date) BETWEEN ? AND ?";
            stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(startDate.getTime()));
            stmt.setTimestamp(2, new Timestamp(endDate.getTime()));
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");

                userList.add(email);
            }

            return userList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<String> getUniqueUsersByMonth(Date date) throws Exception {
        try {
            ArrayList<String> userList = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT DISTINCT email FROM steps_history "
                    + "WHERE MONTH(dateTime) = MONTH(?)";
            stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(date.getTime()));
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");

                userList.add(email);
            }

            return userList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public HashMap<String, Integer> getDayTop(ArrayList<String> userList, Date date) throws Exception {
        try {
            HashMap<String, Integer> resultMap = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();

            for (String user : userList) {
                int steps = 0;
                sql = "SELECT steps FROM steps_history "
                        + "WHERE CAST(dateTime AS date) = ? "
                        + "AND email = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setTimestamp(1, new Timestamp(date.getTime()));
                stmt.setString(2, user);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    steps += rs.getInt("steps");
                }

                resultMap.put(user, steps);
            }

            return resultMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public HashMap<String, Integer> getWeekTop(ArrayList<String> userList, Date startDate, Date endDate) throws Exception {
        try {
            HashMap<String, Integer> resultMap = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();

            for (String user : userList) {
                int steps = 0;
                sql = "SELECT steps FROM steps_history "
                        + "WHERE CAST(dateTime AS date) BETWEEN ? AND ? "
                        + "AND email = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setTimestamp(1, new Timestamp(startDate.getTime()));
                stmt.setTimestamp(2, new Timestamp(endDate.getTime()));
                stmt.setString(3, user);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    steps += rs.getInt("steps");
                }

                resultMap.put(user, steps);
            }

            return resultMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public HashMap<String, Integer> getMonthTop() throws Exception {
        try {
            HashMap<String, Integer> resultMap = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();

            Date date = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int month = cal.get(Calendar.MONTH)+1;

            int steps = 0;
            sql = "SELECT `user`.email, IFNULL(`monthuser`.steps,0) AS `Month_Steps` FROM `user` left join (SELECT email,sum(steps) AS `steps` FROM `steps_history` WHERE Month(dateTime) = ? Group by email) AS `monthuser` on `monthuser`.email = `user`.email\n" +
"ORDER BY `monthuser`.`steps`  DESC";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, month);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String user = rs.getString("email");
                int stepCount = rs.getInt("Month_Steps");
                resultMap.put(user, stepCount);
            }

            return resultMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
