/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.Building;
import entity.CheckPoint;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author jeremy.seow.2014
 */
public class LocationDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public HashMap<Integer, Building> getLocations() throws Exception {
        try {
            HashMap<Integer, Building> buildingMap = new HashMap<Integer, Building>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            
            // Use an INNER JOIN instead of nested ResultSet. Somehow the 2nd rs will break the first rs
            // This is also more efficient compared to doing a nested query and rs
            sql = "SELECT bd.bid, name, latitude, longitude, checkpointID, location_details "
                    + "FROM building bd INNER JOIN checkpoint cp "
                    + "ON bd.bid = cp.bid ORDER BY bd.bid ASC";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int buildingID = rs.getInt("bd.bid");
                String name = rs.getString("name");
                double lat = rs.getDouble("latitude");
                double lng = rs.getDouble("longitude");
                int checkPointID = rs.getInt("checkpointID");
                String locationDetails = rs.getString("location_details");
                ArrayList<CheckPoint> list = new ArrayList<CheckPoint>();

                Building temp = buildingMap.get(buildingID);
                CheckPoint cp = new CheckPoint(checkPointID, locationDetails);
                if (temp != null) {
                    temp.getList().add(cp);
                } else {
                    list.add(cp);
                    Building bd = new Building(name, buildingID, lat, lng, list);
                    buildingMap.put(buildingID, bd);
                }

            }
            return buildingMap;

        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
