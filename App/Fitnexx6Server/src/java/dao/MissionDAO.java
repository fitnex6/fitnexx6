package dao;

import entity.AcceptedMission;
import entity.AvailableMission;
import entity.CheckPoint;
import entity.ClanMissionCount;
import entity.Mission;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MissionDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public void createScan(String email, int checkPointID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO scan VALUES (?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setInt(2, checkPointID);
            stmt.setTimestamp(3, new Timestamp(new Date().getTime()));
            stmt.executeUpdate();

            // might need to log points for missions but shouldnt be steps
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public Mission getMissionByCheckPoint(int checkPointID) throws Exception {
        try {
            Mission m = null;

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT m.missionID, description, mission_type, points "
                    + "FROM mission m INNER JOIN checkpoint cp "
                    + "ON m.missionID = cp.missionID "
                    + "WHERE checkpointID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkPointID);
            rs = stmt.executeQuery();

            if (rs.next()) {
                int missionID = rs.getInt("missionID");
                String description = rs.getString("description");
                String missionType = rs.getString("mission_type");
                int points = rs.getInt("points");

                m = new Mission(missionID, description, missionType, points);
            }

            return m;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public Mission getMissionByMissionID(int missionID) throws Exception {
        try {
            Mission m = null;

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT description, mission_type, points "
                    + "FROM mission WHERE missionID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, missionID);
            rs = stmt.executeQuery();

            if (rs.next()) {
                String description = rs.getString("description");
                String missionType = rs.getString("mission_type");
                int points = rs.getInt("points");

                m = new Mission(missionID, description, missionType, points);
            }

            return m;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public Mission getMissionByType(String missionType, int missionID, int checkPointID) throws Exception {
        try {
            Mission m = null;

            conn = ConnectionPoolManager.getInstance().getConnection();
            if (missionType.equals("Steps")) {
                sql = "SELECT steps FROM mission m WHERE missionID = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, missionID);
                rs = stmt.executeQuery();

                if (rs.next()) {
                    m = new Mission(rs.getInt("steps"));
                }

            } else if (missionType.equals("Checkpoint")) {
                sql = "SELECT latitude, longitude FROM building b "
                        + "INNER JOIN checkpoint cp ON b.bid = cp.bid "
                        + "WHERE checkpointID = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, checkPointID);
                rs = stmt.executeQuery();
                if (rs.next()) {
                    m = new Mission(rs.getDouble("latitude"), rs.getDouble("longitude"));
                }
            } else {
                // What to check for special missions?
            }

            return m;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<ClanMissionCount> getClanMissionCount(int missionID) throws Exception {
        try {
            ArrayList<ClanMissionCount> list = new ArrayList<ClanMissionCount>();
            Date startDate = null;
            Date endDate = null;
//            Calendar c = Calendar.getInstance();
//            c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
//            Date startDate = new Date(c.getTimeInMillis()); //monday's date
//            c.add(Calendar.DATE, 6);
//            Date endDate = new Date(c.getTimeInMillis()); //sunday's date

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT startDate, endDate FROM adventure "
                    + "WHERE isActive = ? "
                    + "AND ? BETWEEN startDate AND endDate";
            stmt = conn.prepareStatement(sql);
            stmt.setBoolean(1, true);
            stmt.setDate(2, new java.sql.Date(new Date().getTime()));
            rs = stmt.executeQuery();
            if (rs.next()) {
                startDate = rs.getDate("startDate");
                endDate = rs.getDate("endDate");
            }

            if (startDate != null && endDate != null) {
                sql = "SELECT c.clan_name, COUNT(temp.email) FROM clan c "
                        + "LEFT OUTER JOIN ("
                        + "SELECT clan_name, u.email FROM user u "
                        + "INNER JOIN accepted_missions am ON u.email = am.email "
                        + "AND am.isCompleted = true "
                        + "AND am.dateTime_completed BETWEEN ? AND ? "
                        + "AND missionID = ?) AS temp "
                        + "ON c.clan_name = temp.clan_name "
                        + "INNER JOIN adventure a "
                        + "ON c.adventureID = a.adventureID "
                        + "WHERE a.isActive = true "
                        + "GROUP BY c.clan_name";
                stmt = conn.prepareStatement(sql);
                stmt.setTimestamp(1, new Timestamp(startDate.getTime()));
                stmt.setTimestamp(2, new Timestamp(endDate.getTime()));
                stmt.setInt(3, missionID);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    ClanMissionCount cmc = new ClanMissionCount(rs.getString("c.clan_name"), rs.getInt("COUNT(temp.email)"));
                    list.add(cmc);
                }
            }

            return list;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<CheckPoint> checkCheckPoints(int acceptID, int missionID, int checkPointID) throws Exception {
        try {
            ArrayList<CheckPoint> list = new ArrayList<CheckPoint>();

            conn = ConnectionPoolManager.getInstance().getConnection();
            if (checkPointID != 0) {
                sql = "UPDATE accepted_missions_checkpoint_progress SET isCompleted = ? "
                        + "WHERE acceptID = ? AND missionID = ? AND checkpointID = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setBoolean(1, true);
                stmt.setInt(2, acceptID);
                stmt.setInt(3, missionID);
                stmt.setInt(4, checkPointID);
                int rowsUpdated = stmt.executeUpdate();
                if (rowsUpdated == 0) {
                    throw new Exception("wrong ending point");
                }
            }

            sql = "SELECT amcp.checkpointID, location_details FROM accepted_missions_checkpoint_progress amcp "
                    + "INNER JOIN checkpoint c ON amcp.checkpointID = c.checkpointID "
                    + "WHERE amcp.acceptID = ? AND amcp.missionID = ? AND amcp.isCompleted = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, acceptID);
            stmt.setInt(2, missionID);
            stmt.setBoolean(3, false);
            rs = stmt.executeQuery();

            while (rs.next()) {
                CheckPoint cp = new CheckPoint(rs.getInt("amcp.checkpointID"), rs.getString("location_details"));
                list.add(cp);
            }
            return list;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<AcceptedMission> getMissionList(String email) throws Exception {
        try {
            ArrayList<AcceptedMission> missionList = new ArrayList<>();

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT acceptID, am.missionID, dateTime_accepted, description, mission_type, steps, points "
                    + "FROM accepted_missions am INNER JOIN mission m "
                    + "ON am.missionID = m.missionID "
                    + "WHERE email = ? "
                    + "AND isCompleted = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setBoolean(2, false);
            rs = stmt.executeQuery();

            while (rs.next()) {
                AcceptedMission am = new AcceptedMission(rs.getInt("acceptID"), rs.getInt("am.missionID"), rs.getTimestamp("dateTime_accepted"),
                        rs.getString("description"), rs.getString("mission_type"), rs.getInt("steps"), rs.getInt("points"));

                missionList.add(am);
            }

            return missionList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<AvailableMission> getAvailableMissions() throws Exception {
        try {
            ArrayList<AvailableMission> availableList = new ArrayList<AvailableMission>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT mission_type, cp.checkpointID, location_details, b.bid, b.name "
                    + "FROM checkpoint cp INNER JOIN mission m ON cp.missionID = m.missionID "
                    + "INNER JOIN building b ON cp.bid = b.bid";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                AvailableMission am = new AvailableMission(rs.getString("mission_type"), rs.getInt("cp.checkpointID"),
                        rs.getString("location_details"), rs.getInt("b.bid"), rs.getString("name"));

                availableList.add(am);
            }

            return availableList;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void acceptMission(String email, String missionType, int missionID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            // need verify no existing incomplete same mission
            if (missionType.equals("Steps")) {
                sql = "SELECT * FROM accepted_missions WHERE missionID = ? AND DATE(dateTime_accepted) = ? AND email = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, missionID);
                stmt.setDate(2, new java.sql.Date(new Date().getTime()));
                stmt.setString(3, email);
                rs = stmt.executeQuery();

                if (rs.next()) {
                    throw new Exception("You can only take one Steps mission a day");
                }
            } else { // For checkpoints and special missions, cannot take on same mission again if previous attempt not completed
                sql = "SELECT * FROM accepted_missions WHERE missionID = ? AND isCompleted = ? AND email = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, missionID);
                stmt.setBoolean(2, false);
                stmt.setString(3, email);
                rs = stmt.executeQuery();

                if (rs.next()) {
                    throw new Exception("This checkpoint mission is already in progress");
                }
            }

            sql = "INSERT INTO accepted_missions (email, missionID, dateTime_accepted, isCompleted) VALUES (?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setInt(2, missionID);
            stmt.setTimestamp(3, new Timestamp(new Date().getTime()));
            stmt.setBoolean(4, false);
            stmt.executeUpdate();

            // There is no need to check whether the insert was successful because we did not set any conditions
            // Hence the only way the insert will fail is things like FK or duplicate PK so an exception will be thrown
            // and the following code will not be executed
            if (missionType.equals("Checkpoint")) {
                int acceptID = 0;
                sql = "SELECT LAST_INSERT_ID()";
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                if (rs.next()) {
                    acceptID = rs.getInt("LAST_INSERT_ID()");
                }

                sql = "SELECT checkpointID FROM mission_checkpoints WHERE missionID = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, missionID);
                rs = stmt.executeQuery();

                sql = "INSERT INTO accepted_missions_checkpoint_progress (acceptID, missionID, checkpointID, isCompleted) "
                        + "VALUES (?, ?, ?, ?)";
                stmt = conn.prepareStatement(sql);
                while (rs.next()) {
                    stmt.setInt(1, acceptID);
                    stmt.setInt(2, missionID);
                    stmt.setInt(3, rs.getInt("checkpointID"));
                    stmt.setBoolean(4, false);

                    stmt.addBatch();
                }
                stmt.executeBatch();
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void completeMission(int acceptID, String email, int points) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "UPDATE accepted_missions SET dateTime_completed = ?, isCompleted = ? WHERE acceptID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(new Date().getTime()));
            stmt.setBoolean(2, true);
            stmt.setInt(3, acceptID);
            stmt.executeUpdate();

            sql = "UPDATE user SET points = points + ? WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, points);
            stmt.setString(2, email);
            stmt.executeUpdate();

//            sql = "UPDATE clan SET mission_count = mission_count + 1 WHERE "
//                    + "clan_name = (SELECT clan_name FROM user where email = ?)";
//            stmt = conn.prepareStatement(sql);
//            stmt.setString(1, email);
//            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void abortMission(int acceptID) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "DELETE FROM accepted_missions WHERE acceptID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, acceptID);
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public HashMap<String, Integer> getClanStats() throws Exception {
        try {
            HashMap<String, Integer> resultMap = new HashMap<>();
            HashMap<String, String> userMap = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();

            sql = "SELECT * FROM adventure WHERE isActive = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setBoolean(1, true);
            rs = stmt.executeQuery();
            Date startDate = null;
            Date endDate = null;
            int adventureID = 0;

            while (rs.next()) {
                startDate = rs.getDate("startDate");
                endDate = rs.getDate("endDate");
                adventureID = rs.getInt("adventureID");
            }

            if (adventureID != 0) {
                sql = "SELECT DISTINCT am.email, clan_name FROM accepted_missions am "
                        + "INNER JOIN user u ON am.email = u.email "
                        + "WHERE CAST(dateTime_accepted AS date) BETWEEN ? AND ? "
                        + "AND CAST(dateTime_completed AS date) BETWEEN ? AND ? "
                        + "AND isCompleted = ? AND clan_name IS NOT NULL";
                stmt = conn.prepareStatement(sql);
                stmt.setDate(1, new java.sql.Date(startDate.getTime()));
                stmt.setDate(2, new java.sql.Date(endDate.getTime()));
                stmt.setDate(3, new java.sql.Date(startDate.getTime()));
                stmt.setDate(4, new java.sql.Date(endDate.getTime()));
                stmt.setBoolean(5, true);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    String email = rs.getString("email");
                    String clanName = rs.getString("clan_name");

                    userMap.put(email, clanName);
                }

                for (String user : userMap.keySet()) {
                    sql = "SELECT count(*) AS count FROM accepted_missions "
                            + "WHERE CAST(dateTime_accepted AS date) BETWEEN ? AND ? "
                            + "AND CAST(dateTime_completed AS date) BETWEEN ? AND ? "
                            + "AND isCompleted = ? "
                            + "AND email = ?";
                    stmt = conn.prepareStatement(sql);
                    stmt.setDate(1, new java.sql.Date(startDate.getTime()));
                    stmt.setDate(2, new java.sql.Date(endDate.getTime()));
                    stmt.setDate(3, new java.sql.Date(startDate.getTime()));
                    stmt.setDate(4, new java.sql.Date(endDate.getTime()));
                    stmt.setBoolean(5, true);
                    stmt.setString(6, user);
                    rs = stmt.executeQuery();

                    while (rs.next()) {
                        int count = rs.getInt("count");

                        if (resultMap.get(userMap.get(user)) == null) {
                            resultMap.put(userMap.get(user), count);
                        } else {
                            int currentCount = resultMap.get(userMap.get(user));
                            resultMap.put(userMap.get(user), count + currentCount);
                        }
                    }
                }

                sql = "SELECT clan_name FROM clan "
                        + "WHERE adventureID = ? ";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, adventureID);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    String clanName = rs.getString("clan_name");

                    if (!resultMap.containsKey(clanName)) {
                        resultMap.put(clanName, 0);
                    }
                }
            }

            return resultMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public int getCurrentDayMissionCount(String email) throws Exception {
        int result = 0;
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT count(*) AS `mission_count` FROM `accepted_missions` WHERE `email` = ? AND `isCompleted` = 1 AND date(`dateTime_completed`) = CURDATE()";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            rs = stmt.executeQuery();

            while (rs.next()) {
                result = rs.getInt("mission_count");
            }

        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
        return result;
    }
}
