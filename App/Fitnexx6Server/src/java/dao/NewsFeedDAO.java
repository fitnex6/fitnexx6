package dao;

import entity.NewsFeed;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class NewsFeedDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;
    
    public ArrayList<NewsFeed> getNewsFeedList(Date date) throws Exception {
        try {
            ArrayList<NewsFeed> result = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM newsfeed "
                    + "WHERE MONTH(dateTime) BETWEEN MONTH(?) - 1 AND MONTH(?) "
                    + "ORDER BY dateTime DESC";
            stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(date.getTime()));
            stmt.setTimestamp(2, new Timestamp(date.getTime()));
            rs = stmt.executeQuery();

            while (rs.next()) {
                NewsFeed newsFeed = new NewsFeed(rs.getDate("dateTime"), rs.getString("email"), rs.getString("subject"), rs.getString("content"), rs.getBytes("news_picture"), rs.getString("category"));
                result.add(newsFeed);
            }
            
            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
