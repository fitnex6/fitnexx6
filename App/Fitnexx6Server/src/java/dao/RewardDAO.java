package dao;

import entity.Reward;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class RewardDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;

    public ArrayList<Reward> getRewardList() throws Exception {
        try {
            ArrayList<Reward> result = new ArrayList<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM reward WHERE isActive = ? AND qty > ? ORDER BY price ASC";
            stmt = conn.prepareStatement(sql);
            stmt.setBoolean(1, true);
            stmt.setInt(2, 0);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int rewardID = rs.getInt("rewardID");
                String description = rs.getString("description");
                byte[] productPicture = rs.getBytes("product_picture");
                int price = rs.getInt("price");
                int qty = rs.getInt("qty");
                int qtyClaimed = rs.getInt("qty_claimed");
                boolean isActive = rs.getBoolean("isActive");

                Reward r = new Reward(rewardID, description, productPicture, price, qty, qtyClaimed, isActive);
                result.add(r);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void redeem(String email, int rewardID, int staffPin, int price) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT points FROM user WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            rs = stmt.executeQuery();
            int points = 0;

            if (rs.next()) {
                points = rs.getInt("points");
            }

            sql = "SELECT qty FROM reward WHERE rewardID = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, rewardID);
            rs = stmt.executeQuery();
            int qty = 0;

            if (rs.next()) {
                qty = rs.getInt("qty");
            }

            if (points < price) {
                throw new Exception("Not enough points");
            } else if (qty == 0) {
                throw new Exception("Reward is not available");
            } else {
                sql = "INSERT INTO redemption (email, rewardID, staff_pin, dateTime) VALUES (?, ?, ?, ?)";
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, email);
                stmt.setInt(2, rewardID);
                stmt.setInt(3, staffPin);
                stmt.setTimestamp(4, new Timestamp(new Date().getTime()));
                stmt.executeUpdate();
                
                sql = "UPDATE user SET points = points - ? "
                        + "WHERE email = ? ";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, price);
                stmt.setString(2, email);
                stmt.executeUpdate();

                sql = "UPDATE reward SET qty = qty - ?, qty_claimed = qty_claimed + ? WHERE rewardID = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, 1);
                stmt.setInt(2, 1);
                stmt.setInt(3, rewardID);
                stmt.executeUpdate();
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
