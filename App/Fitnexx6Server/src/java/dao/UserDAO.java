package dao;

import entity.FitnessHistory;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class UserDAO {

    private Connection conn;
    private PreparedStatement stmt;
    private ResultSet rs;
    private String sql;
    
    public HashMap<String, String> getUserClan() throws Exception {
        try {
            HashMap<String, String> result = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT email, clan_name FROM user";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");
                String clan_name = rs.getString("clan_name");

                result.put(email, clan_name);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
    
    public HashMap<String, String> getNameOfUser() throws Exception {
        try {
            HashMap<String, String> result = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT email, name FROM user";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");
                String name = rs.getString("name");

                result.put(email, name);
            }

            return result;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public String getUserSS(String email) throws Exception {
        try {
            String sharedSecret = null;
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT shared_secret FROM user WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            rs = stmt.executeQuery();

            while (rs.next()) {
                //Retrieve by column name
                sharedSecret = rs.getString("shared_secret");
            }

            return sharedSecret;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void updateUserSS(String email, String sharedSecret) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "UPDATE user SET shared_secret = ? WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, sharedSecret);
            stmt.setString(2, email);
            int rowsUpdated = stmt.executeUpdate();

            if (rowsUpdated == 0) {
                throw new Exception("Email don't exist");
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void register(String email, String name, String passWord, Date DOB, String faculty,
            String gender, String sharedSecret) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO user (email, name, password_hash, DOB, faculty, gender, shared_secret, isActivated, last_active_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setString(2, name);
            stmt.setString(3, passWord);

            if (DOB != null) {
                stmt.setTimestamp(4, new Timestamp(DOB.getTime()));
            } else {
                stmt.setTimestamp(4, null);
            }

            stmt.setString(5, faculty);
            stmt.setString(6, gender);
            stmt.setString(7, sharedSecret);
            stmt.setBoolean(8, false);// Email validated? Default is no
            stmt.setTimestamp(9, new Timestamp(new Date().getTime()));
            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void activateAccount(String email) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT adventureID FROM adventure WHERE isActive = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setBoolean(1, true);
            rs = stmt.executeQuery();
            int adventureID = 0;
            String clan_name = null;

            while (rs.next()) {
                adventureID = rs.getInt("adventureID");
            }

            if (adventureID != 0) {
                sql = "SELECT COUNT(clan_name), clan_name FROM user "
                        + "WHERE clan_name IS NOT NULL "
                        + "GROUP BY clan_name "
                        + "ORDER BY COUNT(clan_name), clan_name ASC "
                        + "LIMIT 1";
                stmt = conn.prepareStatement(sql);
                rs = stmt.executeQuery();

                while (rs.next()) {
                    clan_name = rs.getString("clan_name");
                }
            }

            if (clan_name != null) {
                sql = "UPDATE user SET isActivated = ?, clan_name = ? WHERE email = ?";
            } else {
                sql = "UPDATE user SET isActivated = ? WHERE email = ?";
            }

            stmt = conn.prepareStatement(sql);
            stmt.setBoolean(1, true);

            if (clan_name != null) {
                stmt.setString(2, clan_name);
                stmt.setString(3, email);
            } else {
                stmt.setString(2, email);
            }

            stmt.executeUpdate();
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public User retrieveProfile(String email) throws Exception {
        try {
            User u = null;

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT * FROM (SELECT u.email, name, password_hash, DOB, profile_picture, faculty, "
                    + "gender, points, a.tier, a.tier_picture, s.total_steps, isActivated, last_active_date FROM user AS u "
                    + "LEFT OUTER JOIN achievement AS a ON u.tier = a.tier "
                    + "LEFT OUTER JOIN (SELECT SUM(steps) AS total_steps, email from steps_history GROUP BY email) AS s ON u.email = s.email "
                    + "WHERE u.email = ?) AS u "
                    + "LEFT OUTER JOIN (SELECT height, weight, bmi FROM fitness_history "
                    + "WHERE dateTime = (SELECT MAX(dateTime) FROM fitness_history WHERE email = ?)) "
                    + "AS f ON email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            stmt.setString(2, email);
            stmt.setString(3, email);
            rs = stmt.executeQuery();

            if (rs.next()) {
                //Retrieve by column name
                boolean activated = rs.getBoolean("isActivated");

                if (activated) {
                    String passwordHash = rs.getString("password_hash");
                    String name = rs.getString("name");
                    Date DOB = rs.getDate("DOB");
                    String profilePic = rs.getString("profile_picture");
                    String faculty = rs.getString("faculty");
                    char gender;
                    if (rs.getString("gender") != null) {
                        gender = rs.getString("gender").charAt(0);
                    } else {
                        gender = ' ';
                    }

                    double height = rs.getDouble("height");
                    double weight = rs.getDouble("weight");
                    double BMI = rs.getDouble("bmi");
                    int points = rs.getInt("points");
                    String tier = rs.getString("tier");
                    String tierPic = rs.getString("tier_picture");
                    int totalSteps = rs.getInt("total_steps");
                    Date lastActiveDate = rs.getTimestamp("last_active_date");
                    int criterionToNextTier = 0;

                    sql = "SELECT criterion FROM achievement WHERE criterion > (SELECT criterion FROM achievement a INNER JOIN user u ON a.tier = u.tier WHERE email = ?) ORDER BY criterion ASC LIMIT 1";
                    stmt = conn.prepareStatement(sql);
                    stmt.setString(1, email);
                    rs = stmt.executeQuery();

                    if (rs.next()) {
                        criterionToNextTier = rs.getInt("criterion");
                    }

                    u = new User(email, passwordHash, name, DOB, profilePic, faculty, gender, height, weight, BMI,
                            points, tier, tierPic, totalSteps, lastActiveDate, criterionToNextTier);
                } else {
                    throw new Exception("not activated");
                }
            }

            return u;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public void updateProfile(String email, String profilePic, double height, double weight, double BMI) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "UPDATE user SET profile_picture = ? WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, profilePic);
            stmt.setString(2, email);
            int rowsUpdated = stmt.executeUpdate();

            if (rowsUpdated != 0) {
                sql = "INSERT INTO fitness_history(email, height, weight, bmi, dateTime) VALUES(?, ?, ?, ?, ?)";
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, email);
                stmt.setDouble(2, height);
                stmt.setDouble(3, weight);
                stmt.setDouble(4, BMI);
                stmt.setTimestamp(5, new Timestamp(new Date().getTime()));
                stmt.executeUpdate();
            } else {
                throw new Exception("Email doesn't exist");
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void logSteps(String email, int steps, double caloriesBurnt, long lastActiveDateInMs) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "INSERT INTO steps_history VALUES (?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, new Timestamp(new Date().getTime()));
            stmt.setInt(2, steps);
            stmt.setDouble(3, caloriesBurnt);
            stmt.setString(4, email);
            stmt.executeUpdate();

            // By right we cannot update table that we use in a select statement
            // However, by wrapping the sub-query in another sub-query as a temp table,
            // we are able to do so. However, need to take note in case it doesn't
            // work in other databases
            //sql = "UPDATE user SET points = "
            //+ "(SELECT * FROM (SELECT points FROM user WHERE email = ?) AS temp) + ? "
            //+ "WHERE email = ?";
            sql = "UPDATE user SET points = "
                    + "points + ?, last_active_date = ? "
                    + "WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, steps / 3);
            stmt.setTimestamp(2, new Timestamp(lastActiveDateInMs));
            stmt.setString(3, email);
            stmt.executeUpdate();

        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }
    
    public String getClanName(String email) throws Exception {
        try {
            String clanName = null;
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT clan_name FROM user WHERE email = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            rs = stmt.executeQuery();

            if(rs.next()){
                clanName = rs.getString("clan_name");
            }
            return clanName;

        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public void updatePW(String email, String newPasswordHash, String oldPasswordHash) throws Exception {
        try {
            conn = ConnectionPoolManager.getInstance().getConnection();

            if (oldPasswordHash == null) {
                sql = "UPDATE user SET password_hash = ? WHERE email = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, newPasswordHash);
                stmt.setString(2, email);
            } else {
                sql = "UPDATE user SET password_hash = ? WHERE email = ? AND password_hash = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setString(1, newPasswordHash);
                stmt.setString(2, email);
                stmt.setString(3, oldPasswordHash);
            }

            int count = stmt.executeUpdate();

            if (count == 0) {
                throw new Exception("Old password must match the current password");
            }
        } finally {
            ConnectionPoolManager.close(conn, stmt);
        }
    }

    public HashMap<String, String> getProfilePic() throws Exception {
        try {
            HashMap<String, String> resultMap = new HashMap<>();
            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT email, profile_picture FROM user";
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                String email = rs.getString("email");
                String profilePic = rs.getString("profile_picture");

                resultMap.put(email, profilePic);
            }

            return resultMap;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }

    public ArrayList<FitnessHistory> getBMI(String email) throws Exception {
        try {
            ArrayList<FitnessHistory> list = new ArrayList<FitnessHistory>();
            Calendar cal = Calendar.getInstance();
            Date now = new Date();
            cal.setTime(now);
            long endTime = (long) (Math.ceil(cal.getTimeInMillis() / 1000.0 / 60.0 / 60.0 / 24.0) * 24.0);
            cal.add(Calendar.DAY_OF_WEEK, -7);
            long startTime = (long) (Math.ceil(cal.getTimeInMillis() / 1000.0 / 60.0 / 60.0 / 24.0) * 24.0);

            conn = ConnectionPoolManager.getInstance().getConnection();
            sql = "SELECT distinct(bmi), dateTime FROM fitness_history WHERE email = ? "
                    + "ORDER BY dateTime DESC LIMIT 10";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, email);
            //stmt.setTimestamp(2, new Timestamp(now.getTime()));
            //stmt.setDate(3, new java.sql.Date(now.getTime()));
            rs = stmt.executeQuery();

            while (rs.next()) {
                FitnessHistory fh = new FitnessHistory(new Date(rs.getTimestamp("dateTime").getTime()), rs.getDouble("bmi"));
                list.add(fh);
            }

            return list;
        } finally {
            ConnectionPoolManager.close(conn, stmt, rs);
        }
    }
}
