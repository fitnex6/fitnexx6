package entity;

import java.util.Date;

public class AcceptedMission {
    private int acceptID;
    private int missionID;
    private Date dateTimeAccepted;
    //private Date dateTimeCompleted;
    //private boolean isCompleted;
    private String description;
    private String missionType;
    private int steps;
    private int points;

    public AcceptedMission(int acceptID, int missionID, Date dateTimeAccepted, String description, String missionType, int steps, int points) {
        this.acceptID = acceptID;
        this.missionID = missionID;
        this.dateTimeAccepted = dateTimeAccepted;
        this.description = description;
        this.missionType = missionType;
        this.steps = steps;
        this.points = points;
    }

    public int getAcceptID() {
        return acceptID;
    }

    public int getMissionID() {
        return missionID;
    }

    public Date getDateTimeAccepted() {
        return dateTimeAccepted;
    }

//    public Date getDateTimeCompleted() {
//        return dateTimeCompleted;
//    }
//
//    public boolean isIsCompleted() {
//        return isCompleted;
//    }

    public String getDescription() {
        return description;
    }

    public String getMissionType() {
        return missionType;
    }
    
    public int getSteps(){
        return steps;
    }
    
    public int getPoints(){
        return points;
    }
}
