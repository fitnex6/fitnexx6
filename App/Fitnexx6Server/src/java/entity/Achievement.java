package entity;

public class Achievement {

    private boolean rankUp;
    private String tier;
    private String tierPic;
    private int criterionToNextTier;

    public Achievement(boolean rankUp, String tier, String tierPic, int criterionToNextTier) {
        this.rankUp = rankUp;
        this.tier = tier;
        this.tierPic = tierPic;
        this.criterionToNextTier = criterionToNextTier;
    }

    public boolean isRankUp() {
        return rankUp;
    }

    public String getTier() {
        return tier;
    }

    public String getTierPic() {
        return tierPic;
    }

    public int getCriterionToNextTier() {
        return criterionToNextTier;
    }
}
