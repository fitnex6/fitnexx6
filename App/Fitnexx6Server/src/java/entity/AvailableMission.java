/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author jeremy.seow.2014
 */
public class AvailableMission {
    private String missionType;
    private int checkPointID;
    private String locationDetails;
    private int buildingID;
    private String name;

    public AvailableMission(String missionType, int checkPointID, String locationDetails, int buildingID, String name) {
        this.missionType = missionType;
        this.checkPointID = checkPointID;
        this.locationDetails = locationDetails;
        this.buildingID = buildingID;
        this.name = name;
    }

    public String getMissionType() {
        return missionType;
    }

    public int getCheckPointID() {
        return checkPointID;
    }

    public String getLocationDetails() {
        return locationDetails;
    }

    public int getBuildingID() {
        return buildingID;
    }

    public String getName() {
        return name;
    }
    
}
