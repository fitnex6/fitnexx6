/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;

/**
 *
 * @author jeremy.seow.2014
 */
public class Building {
    private String name;
    private int buildingID;
    private double lat;
    private double lng;
    private ArrayList<CheckPoint> list;

    public Building(String name, int buildingID, double lat, double lng, ArrayList<CheckPoint> list) {
        this.name = name;
        this.buildingID = buildingID;
        this.lat = lat;
        this.lng = lng;
        this.list = list;
    }

    public String getName() {
        return name;
    }

    public int getBuildingID() {
        return buildingID;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public ArrayList<CheckPoint> getList() {
        return list;
    }
    
}
