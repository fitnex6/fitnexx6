package entity;

public class CheckPoint {

    private int checkPointID;
    private String locationDetails;

    public CheckPoint(int checkPointID, String locationDetails) {
        this.checkPointID = checkPointID;
        this.locationDetails = locationDetails;
    }

    public int getCheckPointID() {
        return checkPointID;
    }

    public String getLocationDetails() {
        return locationDetails;
    }
}
