package entity;

public class Clan {
    private String clanName;
    private String description;
    private String clanPicture;

    public Clan(String clanName, String description, String clanPicture) {
        this.clanName = clanName;
        this.description = description;
        this.clanPicture = clanPicture;
    }

    public String getClanName() {
        return clanName;
    }

    public String getDescription() {
        return description;
    }

    public String getClanPicture() {
        return clanPicture;
    }
}
