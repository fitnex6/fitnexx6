/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author jeremy.seow.2014
 */
public class ClanMissionCount {
    private String clanName;
    private int missionCount;

    public ClanMissionCount(String clanName, int missionCount) {
        this.clanName = clanName;
        this.missionCount = missionCount;
    }

    public String getClanName() {
        return clanName;
    }

    public int getMissionCount() {
        return missionCount;
    }
    
}
