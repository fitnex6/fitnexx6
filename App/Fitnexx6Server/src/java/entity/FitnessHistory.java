/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;

/**
 *
 * @author jeremy.seow.2014
 */
public class FitnessHistory {
    private Date dateTime;
    private double BMI;

    public FitnessHistory(Date dateTime, double BMI) {
        this.dateTime = dateTime;
        this.BMI = BMI;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public double getBMI() {
        return BMI;
    }
    
}
