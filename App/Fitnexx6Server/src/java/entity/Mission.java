package entity;

import java.util.ArrayList;

public class Mission {

    private int missionID;
    private String description;
    private String missionType;
    private int points;
    private int steps;
    private ArrayList<CheckPoint> list;
    private double latitude;
    private double longitude;

    public Mission(int missionID, String description, String missionType, int points) {
        this.missionID = missionID;
        this.description = description;
        this.missionType = missionType;
        this.points = points;
    }

    public Mission(int steps) {
        this.steps = steps;
    }

    public Mission(ArrayList<CheckPoint> list) {
        this.list = list;
    }

    public Mission(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getMissionID() {
        return missionID;
    }

    public String getDescription() {
        return description;
    }

    public String getMissionType() {
        return missionType;
    }

    public int getPoints() {
        return points;
    }

    public int getSteps() {
        return steps;
    }

    public ArrayList<CheckPoint> getList() {
        return list;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
