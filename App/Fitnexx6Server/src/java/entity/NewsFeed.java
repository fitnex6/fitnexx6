package entity;

import java.util.Date;

public class NewsFeed {

    private Date dateTime;
    private String email;
    private String subject;
    private String content;
    private byte[] newsPicture;
    private String category;

    public NewsFeed(Date dateTime, String email, String subject, String content, byte[] newsPicture, String category) {
        this.dateTime = dateTime;
        this.email = email;
        this.subject = subject;
        this.content = content;
        this.newsPicture = newsPicture;
        this.category = category;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public String getEmail() {
        return email;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public byte[] getNewsPicture() {
        return newsPicture;
    }
    
    public String getCategory() {
        return category;
    }
}
