package entity;

import java.util.Date;

public class User {

    private String email;
    private String passwordHash;
    private String name;
    private Date DOB;
    private int age;
    private String profilePic;
    private String faculty;
    private char gender;
    private double height;
    private double weight;
    private int points;
    private double BMI;
    private String tier;
    private String tierPic;
    private int totalSteps;
    private Date lastActiveDate;
    private int criterionToNextTier;

    public User(String email, String passwordHash, String name, Date DOB, String profilePic, String faculty, char gender, double height, double weight, double BMI, int points, String tier, String tierPic, int totalSteps, Date lastActiveDate, int criterionToNextTier) {
        this.email = email;
        this.passwordHash = passwordHash;
        this.name = name;
        this.DOB = DOB;
        //age = DateAndTime.diffInDays(new Date(), DOB) / 365;
        this.profilePic = profilePic;
        this.faculty = faculty;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
        this.BMI = BMI;
        this.points = points;
        this.tier = tier;
        this.tierPic = tierPic;
        this.totalSteps = totalSteps;
        this.lastActiveDate = lastActiveDate;
        this.criterionToNextTier = criterionToNextTier;
    }

    public String getEmail() {
        return email;
    }
    
    public String getPasswordHash() {
        return passwordHash;
    }

    public String getName() {
        return name;
    }

    public Date getDOB() {
        return DOB;
    }

    public int getAge() {
        return age;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public String getFaculty() {
        return faculty;
    }

    public char getGender() {
        return gender;
    }

    public double getHeight() {
        return height;
    }

    public double getWeight() {
        return weight;
    }

    public int getPoints() {
        return points;
    }

    public double getBMI() {
        return BMI;
    }

    public String getTier() {
        return tier;
    }

    public String getTierPic() {
        return tierPic;
    }

    public int getCriterionToNextTier() {
        return criterionToNextTier;
    }

    public int getTotalSteps() {
        return totalSteps;
    }

    public Date getLastActiveDate() {
        return lastActiveDate;
    }
    
}
