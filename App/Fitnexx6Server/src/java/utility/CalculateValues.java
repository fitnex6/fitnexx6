/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

/**
 *
 * @author jeremy.seow.2014
 */
public class CalculateValues {

    public static double calculateDistance(double lat1, double long1, double lat2, double long2) {
        int r = 6371; // Radius of Earth in km
        double p = Math.PI / 180; // Degree to radian ratio
        double a = 0.5 - Math.cos((lat2 - lat1) * p) / 2 + Math.cos(lat1 * p) * Math.cos(lat2 * p) * (1 - Math.cos((long2 - long1) * p)) / 2;
        return 12742 * Math.asin(Math.sqrt(a)); // Diameter of Earth = 2 * r = 12742
    }

    public static double calculateCalories(int steps, double height, double weight) {
        double walkingFactor = 0.57;
        double caloriesBurnedPerMile = walkingFactor * (weight * 2.2); // Convert kg to pounds
        double stride = height * 0.415; // Calculate the stride based on height
        double stepToMile = 160934.4 / stride; // miles in cm
        double conversationFactor = caloriesBurnedPerMile / stepToMile;
        double caloriesBurned = steps * conversationFactor;
        return caloriesBurned;
    }
}
