package utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateAndTime {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static String dateToString(Date date) {
        return simpleDateFormat.format(date);
    }
    
    public static String dateToStringNewsFeedFormat(Date date) {
        SimpleDateFormat simpleDateFormatNewsFeedFormat = new SimpleDateFormat("MMMM dd, yyyy");
        return simpleDateFormatNewsFeedFormat.format(date);
    }

    public static Date stringToDate(String dateString) throws ParseException {
        return simpleDateFormat.parse(dateString);
    }

    public static int diffInDays(Date start, Date end) {
        TimeUnit unit = TimeUnit.DAYS;
        long diffInMillisec = start.getTime() - end.getTime();
        int diffInDays = (int) unit.convert(diffInMillisec, TimeUnit.MILLISECONDS);
        return diffInDays;
    }
}
