<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Account Activation</title>
    </head>
    <body>
        <%
            String message = (String) session.getAttribute("message");
        %>
        <h1><Center><font face="Century Gothic"><%= message%></font></centre></h1>
        <%
            session.removeAttribute("message");
        %>
    </body>
</html>
