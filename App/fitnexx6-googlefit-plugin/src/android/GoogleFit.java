package fitnexx6.googlefit.plugin;

import android.content.IntentSender;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Subscription;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.fitness.result.DataSourcesResult;
import com.google.android.gms.fitness.result.ListSubscriptionsResult;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.Calendar;
import java.util.Date;

/**
 * This sample demonstrates how to use the History API of the Google Fit platform to insert data,
 * query against existing data, and remove data. It also demonstrates how to authenticate
 * a user with Google Play Services and how to properly represent data in a {@link DataSet}.
 */
public class GoogleFit extends CordovaPlugin {
    public GoogleApiClient mClient = null;
	private OnDataPointListener mListener = null;

    private Activity act;
    private Context context;
	
	public static final String TAG = "GoogleFit Plugin";
	private static final int REQUEST_OAUTH = 1;

	@Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		Log.i(TAG, "Initializing");
		act = cordova.getActivity();
		context = act.getApplicationContext();

		cordova.setActivityResultCallback(this);
		initializeAPIClient();
	}
	
	@Override
    public void onStart() {
        super.onStart();
        if (!mClient.isConnected()) {
            mClient.connect();
			Log.i(TAG, "Connected!!!");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mClient.isConnected()) {
            mClient.disconnect();
			Log.i(TAG, "Disconnected!!!");
        }
    }

	public void initializeAPIClient(){
		// Create the Google API Client
		mClient = new GoogleApiClient.Builder(context)
				.addApi(Fitness.RECORDING_API) // Allows the recording of all steps taken to be stored with Google
				.addApi(Fitness.SENSORS_API) // Allows us to retrieve the steps taken by user since the last call
				.addApi(Fitness.HISTORY_API) // Allows us to retrieve the steps_history recorded by RECORDING_API
				.addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ)) // Permission required to use steps_data. Check Google API
				.addConnectionCallbacks(
				new GoogleApiClient.ConnectionCallbacks() {
					@Override
					public void onConnected(Bundle bundle) {
						Log.i(TAG, "Connected!!!");
						// Now that we have initialized the GoogleFit client with the required API and scope,
						// we can do the actual retrieving of data
						
						subscribe(); // Subscribe for long term recording of data
					}

					@Override
					public void onConnectionSuspended(int i) {
						// If your connection to the sensor gets lost at some point,
						// you'll be able to determine the reason and react to it here.
						if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
							Log.i(TAG, "Connection lost.  Cause: Network Lost.");
						} else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
							Log.i(TAG, "Connection lost.  Reason: Service Disconnected");
						}
					}
				}
			).addOnConnectionFailedListener( // If connection fails
				new GoogleApiClient.OnConnectionFailedListener() {
					// Called whenever the API client fails to connect.
					@Override
					public void onConnectionFailed(ConnectionResult result) {
						Log.i(TAG, "Connection failed. Cause: " + result.toString());
						if (result.hasResolution()) { // If connection fails cause user hasn't give permission to use GoogleFit
							try {
								Log.i(TAG, "Start oauth login...");

								cordova.setActivityResultCallback(GoogleFit.this);
								result.startResolutionForResult(act, REQUEST_OAUTH); // Ask user for permission to use GoogleFit

							} catch (IntentSender.SendIntentException e) {
								Log.i(TAG, "OAuth login failed", e);
							}
						} else {
							// Show the localized error dialog
							Log.i(TAG, "Show error dialog!");

							GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), act, 0).show();
						}
					}
				}
            ).build();
			mClient.connect(); // To connect to GoogleFit API
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) { // After user give permission and returns to our app
		Log.w(TAG, "onActivityResult requestCode: " + requestCode + ", resultCode: " + resultCode + ", data: " + data);

		if (requestCode == REQUEST_OAUTH && resultCode == Activity.RESULT_OK) {
			mClient.connect(); 
		}
	}
	
	public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
		if ("retrieveStepsByDay".equals(action)) { // This is for the retrieval of long term data for progress checking
            retrieveData(callbackContext, "day");
        }
		if ("retrieveStepsByWeek".equals(action)) { // This is for the retrieval of long term data for progress checking
            retrieveData(callbackContext, "week");
        }
		if ("retrieveStepsByMonth".equals(action)) { // This is for the retrieval of long term data for progress checking
            retrieveData(callbackContext, "month");
        }
		if("scanData".equals(action)){ // This is for the retrieval of data since the last call for scanning of QR code
			initializeSensor(callbackContext);
		}

        return true;  // Returning false will result in a "MethodNotFound" error.
    }

	//Short term API
	//Steps taken since last call
	public void initializeSensor(final CallbackContext callbackContext){
		// Note: Fitness.SensorsApi.findDataSources() requires the ACCESS_FINE_LOCATION permission.
		Fitness.SensorsApi.findDataSources(mClient, new DataSourcesRequest.Builder()
				// At least one datatype must be specified.
				.setDataTypes(DataType.TYPE_STEP_COUNT_DELTA) // Referring to the data of steps taken
				.setDataSourceTypes(DataSource.TYPE_DERIVED) // Can specify whether data type is raw or derived.
				.build())
				.setResultCallback(new ResultCallback<DataSourcesResult>() {
					@Override
					public void onResult(DataSourcesResult dataSourcesResult) {
						Log.i(TAG, "Result: " + dataSourcesResult.getStatus().toString());
						for (DataSource dataSource : dataSourcesResult.getDataSources()) {
							Log.i(TAG, "Data source found: " + dataSource.toString());
							Log.i(TAG, "Data Source type: " + dataSource.getDataType().getName());

							//Let's register a listener to receive Activity data!
							if (dataSource.getDataType().equals(DataType.TYPE_STEP_COUNT_DELTA)
									&& mListener == null) {
								Log.i(TAG, "Data source for LOCATION_SAMPLE found!  Registering.");
								
								registerFitnessDataListener(dataSource,
										DataType.TYPE_STEP_COUNT_DELTA, callbackContext);
							}
						}
					}
				});
	}

	//response for short term
	public void registerFitnessDataListener(DataSource dataSource, DataType dataType, final CallbackContext callbackContext){
		mListener = new OnDataPointListener() {
			@Override
			public void onDataPoint(DataPoint dataPoint) { // Action taken by listener when it receives the response
			int totalSteps = 0;
				for (Field field : dataPoint.getDataType().getFields()) {
					final Value val = dataPoint.getValue(field);
					Log.i(TAG, "Detected DataPoint field: " + field.getName());
					Log.i(TAG, "Detected DataPoint value: " + val.asInt());
					totalSteps += val.asInt();
				}
			stopSensor();
			callbackContext.success(totalSteps); // Return result to the successCallback method passed in execute method
			}
		};

		Fitness.SensorsApi.add(
				mClient,
				new SensorRequest.Builder()
						.setDataSource(dataSource) // Optional but recommended for custom data sets.
						.setDataType(dataType) // Can't be omitted.
						.setSamplingRate(10, TimeUnit.SECONDS)
						.build(),
				mListener) // Adding the listener to the SensorRequest
				.setResultCallback(new ResultCallback<Status>() {
					@Override
					public void onResult(Status status) {
						if (status.isSuccess()) {
							Log.i(TAG, "Listener registered!");
						} else {
							Log.i(TAG, "Listener not registered.");
						}
					}
				});
	}

	public void stopSensor(){
		// Waiting isn't actually necessary as the unregister call will complete regardless,
		// even if called from within onStop, but a callback can still be added in order to
		// inspect the results.

		Fitness.SensorsApi.remove(
				mClient,
				mListener)
				.setResultCallback(new ResultCallback<Status>() {
					@Override
					public void onResult(Status status) {
						if (status.isSuccess()) {
							Log.i(TAG, "Listener was removed!");
						} else {
							Log.i(TAG, "Listener was not removed.");
						}
					}
				});
	}

	//long term
	public void subscribe(){
		// To create a subscription, invoke the Recording API. As soon as the subscription is
		// active, fitness data will start recording.
		// [START subscribe_to_datatype]
		Fitness.RecordingApi.subscribe(mClient, DataType.TYPE_STEP_COUNT_DELTA) // Referring to the data of steps taken
				.setResultCallback(new ResultCallback<Status>() {
					@Override
					public void onResult(Status status) {
						if (status.isSuccess()) {
							if (status.getStatusCode()
									== FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
								Log.i(TAG, "Existing subscription for activity detected.");
							} else {
								Log.i(TAG, "Successfully subscribed!");
							}
						} else {
							Log.i(TAG, "There was a problem subscribing.");
						}
					}
				});
	}

	public void retrieveData(final CallbackContext callbackContext, String dayOrMonthOrWeek) {
		// Create start and end time for retrieval
		Calendar cal = Calendar.getInstance();
		Date now = new Date();
		int duration;
		TimeUnit timeUnit;
		SimpleDateFormat sdf;

		cal.setTime(now);
		long endTime = (long)(Math.ceil(cal.getTimeInMillis()/1000.0/60.0/60.0/24.0) * 24.0 - 8.0);
		
		if(dayOrMonthOrWeek.equals("month")){
			duration = 7;
			cal.add(Calendar.WEEK_OF_YEAR, -4); // Time range of 1 month
			timeUnit = TimeUnit.DAYS;
			sdf = new SimpleDateFormat("dd MMM");
		} else if (dayOrMonthOrWeek.equals("week")){
			duration = 1;
			cal.add(Calendar.WEEK_OF_YEAR, -1); // Time range of 1 week
			timeUnit = TimeUnit.DAYS;
			sdf = new SimpleDateFormat("EEE, dd");
		} else{
			duration = 3;
			cal.add(Calendar.DAY_OF_YEAR, -1);
			timeUnit = TimeUnit.HOURS;
			sdf = new SimpleDateFormat("HH:mm");
		}
		
		long startTime = (long)(Math.ceil(cal.getTimeInMillis()/1000.0/60.0/60.0/24.0) * 24.0 - 8.0);

		final DataReadRequest readRequest = new DataReadRequest.Builder()
				// The data request can specify multiple data types to return, effectively
				// combining multiple data queries into one call.
				// In this example, it's very unlikely that the request is for several hundred
				// datapoints each consisting of a few steps and a timestamp.  The more likely
				// scenario is wanting to see how many steps were walked per day, for 7 days.
				.aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
				//.read(DataType.AGGREGATE_STEP_COUNT_DELTA)
				// Analogous to a "Group By" in SQL, defines how data should be aggregated.
				// bucketByTime allows for a time span, whereas bucketBySession would allow
				// bucketing by "sessions", which would need to be defined in code.
				.bucketByTime(duration, timeUnit) // Data is grouped by days
				.setTimeRange(startTime, endTime, TimeUnit.HOURS) // The time range of data to retrieve
				.build();

				DataReadResult dataReadResult =
						Fitness.HistoryApi.readData(mClient, readRequest).await(10, TimeUnit.SECONDS); // HISTORY_API to retrieve steps_history
				
				// Store the result in an JSONArray to return
				JSONArray result = new JSONArray();
				
				// used for aggregated data/ bucketed data
				for (Bucket bucket : dataReadResult.getBuckets()) {
					List<DataSet> dataSets = bucket.getDataSets();
					
					for (DataSet dataSet : dataSets) {
						try{
							if(dataSet.getDataPoints().isEmpty()){ // If there are no data on that day, we return 0 instead of skipping that day
								JSONObject temp = new JSONObject();
								temp.put("label", sdf.format(new Date(bucket.getStartTime(TimeUnit.MILLISECONDS))));
								temp.put("value", 0);
								result.put(temp);								
							} else{
								for (DataPoint dp : dataSet.getDataPoints()) {
									for (Field field : dp.getDataType().getFields()) {
										// Naming is for the purpose of the charts API used in progress tracking
										JSONObject temp = new JSONObject();
										temp.put("label", sdf.format(new Date(bucket.getStartTime(TimeUnit.MILLISECONDS))));
										temp.put("value", dp.getValue(field).asInt());
										result.put(temp);
									}
								}
							}
						} catch (JSONException e){
							callbackContext.error("Failed to parse parameters");
						}
					}
				}
				
				Log.i(TAG, result.toString());
				callbackContext.success(result);

	}

}
