function GoogleFit() {
}

GoogleFit.prototype.retrieveStepsByDay = function(successCallback, failureCallback) {
  cordova.exec(successCallback, failureCallback, "GoogleFit", "retrieveStepsByDay", []);
};

GoogleFit.prototype.retrieveStepsByWeek = function(successCallback, failureCallback) {
  cordova.exec(successCallback, failureCallback, "GoogleFit", "retrieveStepsByWeek", []);
};

GoogleFit.prototype.retrieveStepsByMonth = function(successCallback, failureCallback) {
  cordova.exec(successCallback, failureCallback, "GoogleFit", "retrieveStepsByMonth", []);
};

GoogleFit.prototype.scanData = function(successCallback, failureCallback) {
  cordova.exec(successCallback, failureCallback, "GoogleFit", "scanData", []);
};

var googleFit = new GoogleFit();
module.exports = googleFit;