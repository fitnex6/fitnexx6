// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js/
//var serverURL = "http://localhost:8084/"
var serverURL = "http://fypfitnexx-leroyce.rhcloud.com/"

angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'ngCordova','ng-walkthrough'])

.config(function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.views.transition('none');
})

.run(function($ionicPlatform, $state, $http) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // StatusBar.style(1) //Light
      // Statusbar.styleColor('black')
      if (ionic.Platform.isAndroid()) {
          StatusBar.backgroundColorByHexString("#4C7BFF");
      }
    }
	if(window.localStorage.getItem("accessToken") !== null){
		$http({
			url: serverURL + "Fitnexx6Server/VerifyTokenServlet",
			method: "POST",
			data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken")},
			headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
		}).then(function (response) {
			// handle success
			var result = angular.fromJson(response.data);
      navigator.splashscreen.hide();
			if(result.status == "success"){
				$state.go('tabsController.mySteps');
			} else{
				$state.go('login'); // next time change to grabbing stored credentials in phone and initialise login
			}
		}, function (response) { // handle error
        navigator.splashscreen.hide();
				alert("Server down. Please try again later.");
				ionic.Platform.exitApp();
		});
    } else{
		$state.go('login');
	}
  });
})
