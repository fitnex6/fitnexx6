//var serverURL = "http://localhost:8084/"
var serverURL = "http://fypfitnexx-leroyce.rhcloud.com/"

angular.module('app.controllers', ['ng-walkthrough', 'nvd3', 'ionic', 'leaflet-directive', 'ionic-toast', 'ui.bootstrap'])


.controller('registerCtrl', function($scope, $http, $state, ionicToast) {

    $scope.register = function () {
	    $http({
			url: serverURL + "Fitnexx6Server/RegisterServlet",
			method: "POST",
			data: {'email': $scope.email, 'name': $scope.name, 'passWord': $scope.passWord, 'passWord2': $scope.passWord2, 'DOB': $scope.DOB, 'faculty': $scope.faculty, 'gender': $scope.gender},
			headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
		}).then(function (response) { // handle success
				var result = angular.fromJson(response.data);

				if(result.status == "success"){
						ionicToast.show("We have sent you an email to activate your account", 'middle', false, 3500);
						$state.go('login');
				}
				else {
						ionicToast.show(result.errors, 'middle', false, 3500);
				}
		}, function (response) { // handle error
						// log response.status + " " + response.statusText
			alert("Server down. Please try again later");
		});
	}
})


.controller('feedbackCtrl', function($scope, $http, $state, ionicToast) {
		$scope.submitFeedback = function () {
				$http({
						url: serverURL + "Fitnexx6Server/FeedbackServlet",
						method: "POST",
						 data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'areaOfConcern': $scope.areaOfConcern, 'feedback': $scope.feedback},
						headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
				}).then(function (response) { // handle success
						var result = angular.fromJson(response.data);

						if(result.status == "success"){
								ionicToast.show("Thank you! We have received your feedback", 'middle', false, 3500);
								$state.go('tabsController.mySteps');
						}
						else {
							if(result.errors == "Token invalid or expired"){
								ionicToast.show(result.errors, 'middle', false, 3500);
								$state.go('login');
							} else{
								ionicToast.show(result.errors, 'middle', false, 3500);
							}
						}
				}, function (response) { // handle error
						//log response.status + " " + response.statusText
						alert("Server down. Please try again later");
				});
		};
})

.controller('rewardCtrl', function($scope, $http, $state, $ionicPopup, ionicToast, $ionicLoading) {
    var tutorialActive = JSON.parse(window.localStorage.getItem("tutorialActive"));
    if(!tutorialActive.rewardsTutorial){
      $scope.tutorialActive = true;
      tutorialActive.rewardsTutorial = true;
      window.localStorage.setItem("tutorialActive", JSON.stringify(tutorialActive));
    }else{
      $scope.tutorialActive = false;
    }
		$ionicLoading.show({template:'<p>Loading Rewards...</p><ion-spinner class="spinner-calm" icon="lines"></ion-spinner>'});
		$scope.$on("$ionicView.beforeEnter", function () {
				$http ({
						url: serverURL + "Fitnexx6Server/ViewRewardsServlet",
						method: "POST",
						data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken")},
						headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}

				}).then(function (response) { // consider changing to ionic.beforeEnter
						var result = angular.fromJson(response.data);

						if(result.status == "success"){
							$scope.points = window.localStorage.getItem("points");
							$scope.rewards = result.rewards;
							$scope.result = $scope.points;

						} else{
							if(result.errors == "Token invalid or expired"){

								ionicToast.show(result.errors, 'middle', false, 3500);
								$state.go('login');
							} else{
								ionicToast.show(result.errors, 'middle', false, 3500);
							}
						}
            $ionicLoading.hide();
				}, function(response) {
						// log response.status + " " + response.statusText
						$ionicLoading.hide();
						alert("Server down. Please try again later");
				});

		});

		var redeemReward = function (id, price, name) {
				// check enough points here or disable redeem button for items that user cant afford

				$http ({
						url: serverURL + "Fitnexx6Server/RedeemRewardServlet",
						method: "POST",
						data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'rewardID': JSON.stringify(id), 'staffPin': $scope.data.pw,
						'price': JSON.stringify(price)},
						headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}

				}).then(function (response) {
						var result = angular.fromJson(response.data);

						if(result.status == "success"){
							$scope.points -= price;
							$scope.rewardName = name;
							window.localStorage.setItem("points", $scope.points);
							var alert = $ionicPopup.alert({
				                 title: 'Successfully Redeemed!',
				                 template: '<center>You have successfully redeemed ' + name + '!</center>'
			              	});
							//ionicToast.show("You have successfully redeemed the reward!", 'middle', true, 3500);

							alert.then(function(res)	{
								$state.go($state.current, {}, {reload: true});
							});

						} else{
							if(result.errors == "Token invalid or expired"){
								ionicToast.show(result.errors, 'middle', false, 3500);
								$state.go('login');
							} else{
								ionicToast.show(result.errors, 'middle', false, 3500);
							}
						}
				}, function(response) {
						// log response.status + " " + response.statusText
						alert("Server down. Please try again later");
				});
		};


		$scope.doRefresh = function() {

				$http ({
					url: serverURL + "Fitnexx6Server/ViewRewardsServlet",
					method: "POST",
					data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken")},
					headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}

				}).then(function (response) { // consider changing to ionic.beforeEnter
						var result = angular.fromJson(response.data);

						if(result.status == "success"){
							$scope.points = window.localStorage.getItem("points");
							$scope.rewards = result.rewards;
							$scope.result = $scope.points;

						} else {

							if(result.errors == "Token invalid or expired"){

								ionicToast.show(result.errors, 'middle', false, 3500);
								$state.go('login');
							} else{
								ionicToast.show(result.errors, 'middle', false, 3500);
							}
						}

				}, function(response) {
						// log response.status + " " + response.statusText
						alert("Server down. Please try again later");
				});

				$scope.$broadcast('scroll.refreshComplete');
		};


		$scope.showPopup = function(id, price, name) {
				$scope.data = {};
				// An elaborate, custom popup
				var myPopup = $ionicPopup.show({
						template: '<input type="password" ng-model="data.pw">',
						title: '<h5>Redeeming ' + name + '</h5>',
						subTitle: '<h6>Please Enter Staff Pin</h6>',
						scope: $scope,
						buttons: [
								{ text: 'Cancel' },
								{
										text: '<b>Enter</b>',
										type: 'button-positive',
										onTap: function(e) {
												redeemReward(id, price, name);
												return $scope.data.pw;
										}
								}
						]
				}).then(function(res) {
						console.log('Your password is', res); // ???
				});
		};
})

.controller('newsFeedCtrl', function($scope, $http, $state, ionicToast, $ionicLoading) {
  var tutorialActive = JSON.parse(window.localStorage.getItem("tutorialActive"));
  if(!tutorialActive.newsFeedTutorial){
    $scope.tutorialActive = true;
    tutorialActive.newsFeedTutorial = true;
    window.localStorage.setItem("tutorialActive", JSON.stringify(tutorialActive));
  }else{
    $scope.tutorialActive = false;
  }

  $ionicLoading.show({template:'<p>Loading News...</p><ion-spinner class="spinner-calm" icon="lines"></ion-spinner>'});
  $scope.$on("$ionicView.beforeEnter", function () {
  $http({
  				url: serverURL + "Fitnexx6Server/NewsFeedServlet",
  				method: "POST",
  				data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken")},
  				headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
  		}).then(function (response) { // consider changing to ionic.beforeEnter
  		 // handle success
  				var result = angular.fromJson(response.data);

  				if(result.status == "success"){
            		$scope.news = result.newsfeed;

  				} else{
  					if(result.errors == "Token invalid or expired"){
  						ionicToast.show(result.errors, 'middle', false, 3500);
  						$state.go('login');
  					} else{
  						ionicToast.show(result.errors, 'middle', false, 3500);
  					}
  				}
          $ionicLoading.hide();
  		}, function (response) { // handle error
  				// log response.status + " " + response.statusText
  				$ionicLoading.hide();
  				alert("Server down. Please try again later");
  		});
  });

  $scope.doRefresh = function() {

      $http({
          url: serverURL + "Fitnexx6Server/NewsFeedServlet",
          method: "POST",
          data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken")},
          headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
      }).then(function (response) { // consider changing to ionic.beforeEnter
       // handle success
          var result = angular.fromJson(response.data);

          if(result.status == "success"){
                $scope.news = result.newsfeed;

          } else{
            if(result.errors == "Token invalid or expired"){
              ionicToast.show(result.errors, 'middle', false, 3500);
              $state.go('login');
            } else{
              ionicToast.show(result.errors, 'middle', false, 3500);
            }
          }

      }, function (response) { // handle error
          // log response.status + " " + response.statusText
          alert("Server down. Please try again later");
      });

				$scope.$broadcast('scroll.refreshComplete');
	};
})

.controller('myStepsCtrl', function($scope, $rootScope, $http, $state, ionicToast) {
    var tutorialActive = JSON.parse(window.localStorage.getItem("tutorialActive"));
    if(!tutorialActive.homeTutorial){
      $scope.tutorialActive = true;
      tutorialActive.homeTutorial = true;
      window.localStorage.setItem("tutorialActive", JSON.stringify(tutorialActive));
    }else{
      $scope.tutorialActive = false;
    }

    //======START OF ANDROID======
      $scope.$on("$ionicView.beforeEnter", function () {
		    // Broadcast to the listener at side menu to update the profile pic, name and tier
		   $rootScope.$broadcast("profileUpdated");

		  	$scope.currentTime = new Date().getTime();
		  	$scope.name = window.localStorage.getItem("name");
		  	$scope.tier = window.localStorage.getItem("tier");
		  	$scope.tierPic = window.localStorage.getItem("tierPic");
		  	var criterionToNextTier = window.localStorage.getItem("criterionToNextTier");
		  	var totalSteps = window.localStorage.getItem("totalSteps");

		  	if(criterionToNextTier == 0){
		  		$scope.progressToNextTier = "At the highest league!";
		  		$scope.amtOfSteps = criterionToNextTier;
		  		$scope.amtToNextTier = criterionToNextTier;
		  	} else{
		  		$scope.progressToNextTier = totalSteps + " / " + criterionToNextTier;
		  		$scope.amtOfSteps = totalSteps;
		  		$scope.amtToNextTier = criterionToNextTier;
		  	}

		  	fitnexx6.googlefit.retrieveStepsByDay(successProgress, failureProgress);
		  	fitnexx6.googlefit.scanData(successLogSteps, failureLogSteps);
		  });

		  function successProgress(data){
		  		var result = angular.fromJson(data);
		  		var totalSteps = 0;
		  		for(var i=0; i<result.length; i++){
		  			totalSteps += parseInt(result[i].value);
		  		}
		  		$scope.steps = totalSteps;
		  		var targetSteps = 10000;
		  		var percent = Math.round((totalSteps / targetSteps) * 100);

		  		if(percent < 10){
		  		  $scope.progress = 0;
		  		}else if(percent < 30){
		  		  $scope.progress = 10;
		  		}else if(percent < 45){
		  		  $scope.progress = 30;
		  		}else if(percent < 60){
		  		  $scope.progress = 45;
		  		}else if(percent < 75){
		  		  $scope.progress = 60;
		  		}else if(percent < 90){
		  		  $scope.progress = 75;
		  		}else if(percent < 100){
		  		  $scope.progress = 90;
		  		}else{
		  		  $scope.progress = 100;
		  		}

		  		var walkingFactor = 0.57;
		        var caloriesBurnedPerMile = walkingFactor * (window.localStorage.getItem("weight") * 2.2); // Convert kg to pounds
		        var stride = window.localStorage.getItem("height") * 100 * 0.415; // Calculate the stride based on height
		        var stepToMile = 160934.4 / stride; // miles in cm
		        var conversationFactor = caloriesBurnedPerMile / stepToMile;
		        $scope.caloriesBurned = Math.ceil(totalSteps * conversationFactor);
		        $scope.distance = Math.ceil(totalSteps * stride / 100);

		  		$scope.$apply();
		  		navigator.splashscreen.hide();
		  }
		  function failureProgress(data){
		  		$scope.result = "error " + data;
		  }

		  function successLogSteps(data){
		  		logData(JSON.stringify(data));
		  }
		  function failureLogSteps(data){
		  		$scope.result = "error " + data;
		  }

		  function logData(data){
		  $http({
		  	  url: serverURL + "Fitnexx6Server/LogStepsServlet",
		  	  method: "POST",
		  	  data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'steps': data, 'height': window.localStorage.getItem("height"), 'weight': window.localStorage.getItem("weight"), 'lastActiveDateInMs': JSON.stringify($scope.currentTime)},
		  		headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
		  	}).then(function (response) { // handle success
		  		var result = angular.fromJson(response.data);

		  		if(result.status == "success"){
            		// need to parseInt due to + being a concentrator as well. for - no need
            			var points = parseInt(window.localStorage.getItem("points")) + parseInt(data);
		  			var totalSteps = parseInt(window.localStorage.getItem("totalSteps")) + parseInt(data);
            			window.localStorage.setItem("points", points);
		  			window.localStorage.setItem("totalSteps", totalSteps);
		  			window.localStorage.setItem("lastActiveDate", $scope.currentTime);

            			if(result.rankUped){
		  				window.localStorage.setItem("tier", result.tier);
		  				window.localStorage.setItem("tierPic", result.tierPic);
		  				window.localStorage.setItem("criterionToNextTier", result.criterionToNextTier);
		  				ionicToast.show("You have ranked up!", 'middle', false, 3500);
		  			}

		  			if(result.criterionToNextTier == 0){
		  				$scope.progressToNextTier = "At the highest league!";
		  				$scope.amtOfSteps = result.criterionToNextTier;
		  				$scope.amtToNextTier = result.criterionToNextTier;
		  			} else{
		  				$scope.progressToNextTier = totalSteps + " / " + result.criterionToNextTier;
		  				$scope.amtOfSteps = totalSteps;
		  				$scope.amtToNextTier = result.criterionToNextTier;
		  			}
         			 }

		  		 else {
		  			if(result.errors == "Token invalid or expired"){
		  				ionicToast.show(result.errors, 'middle', false, 3500);
		  				$state.go('login');
		  			} else{
		  				ionicToast.show(result.errors, 'middle', false, 3500);
		  			}
		  		 }

		  	}, function (response) { // handle error
		  		// log response.errors + " " + response.statusText
		  		alert("Server down. Please try again later");
		  	});
		  }

      //===================================START OF REFRESH ANDROID===========================================
		  $scope.doRefreshAndroid = function() {

	         $rootScope.$broadcast("profileUpdated");

	          $scope.currentTime = new Date().getTime();
	          $scope.name = window.localStorage.getItem("name");
	          $scope.tier = window.localStorage.getItem("tier");
	          $scope.tierPic = window.localStorage.getItem("tierPic");
	          var criterionToNextTier = window.localStorage.getItem("criterionToNextTier");
	          var totalSteps = window.localStorage.getItem("totalSteps");

	          if(criterionToNextTier == 0){
	            $scope.progressToNextTier = "At the highest league!";
	            $scope.amtOfSteps = criterionToNextTier;
	            $scope.amtToNextTier = criterionToNextTier;
	          } else{
	            $scope.progressToNextTier = totalSteps + " / " + criterionToNextTier;
	            $scope.amtOfSteps = totalSteps;
	            $scope.amtToNextTier = criterionToNextTier;
	          }

	          fitnexx6.googlefit.retrieveStepsByDay(successProgress, failureProgress);
	          fitnexx6.googlefit.scanData(successLogSteps, failureLogSteps);


	        function successProgress(data){
	            var result = angular.fromJson(data);
	            var totalSteps = 0;
	            for(var i=0; i<result.length; i++){
	              totalSteps += parseInt(result[i].value);
	            }
	            $scope.steps = totalSteps;
	            var targetSteps = 10000;
	            var percent = Math.round((totalSteps / targetSteps) * 100);

	            if(percent < 10){
	              $scope.progress = 0;
	            }else if(percent < 30){
	              $scope.progress = 10;
	            }else if(percent < 45){
	              $scope.progress = 30;
	            }else if(percent < 60){
	              $scope.progress = 45;
	            }else if(percent < 75){
	              $scope.progress = 60;
	            }else if(percent < 90){
	              $scope.progress = 75;
	            }else if(percent < 100){
	              $scope.progress = 90;
	            }else{
	              $scope.progress = 100;
	            }

	            var walkingFactor = 0.57;
	              var caloriesBurnedPerMile = walkingFactor * (window.localStorage.getItem("weight") * 2.2); // Convert kg to pounds
	              var stride = window.localStorage.getItem("height") * 100 * 0.415; // Calculate the stride based on height
	              var stepToMile = 160934.4 / stride; // miles in cm
	              var conversationFactor = caloriesBurnedPerMile / stepToMile;
	              $scope.caloriesBurned = Math.ceil(totalSteps * conversationFactor);
	              $scope.distance = Math.ceil(totalSteps * stride / 100);

	            $scope.$apply();
	            navigator.splashscreen.hide();
	        }
	        function failureProgress(data){
	            $scope.result = "error " + data;
	        }

	        function successLogSteps(data){
	            logData(JSON.stringify(data));
	        }
	        function failureLogSteps(data){
	            $scope.result = "error " + data;
	        }

	        function logData(data){
	        $http({
	            url: serverURL + "Fitnexx6Server/LogStepsServlet",
	            method: "POST",
	            data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'steps': data, 'height': window.localStorage.getItem("height"), 'weight': window.localStorage.getItem("weight"), 'lastActiveDateInMs': JSON.stringify($scope.currentTime)},
	            headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
	          }).then(function (response) { // handle success
	            var result = angular.fromJson(response.data);

	            if(result.status == "success"){
	                  // need to parseInt due to + being a concentrator as well. for - no need
	                    var points = parseInt(window.localStorage.getItem("points")) + parseInt(data);
	              var totalSteps = parseInt(window.localStorage.getItem("totalSteps")) + parseInt(data);
	                    window.localStorage.setItem("points", points);
	              window.localStorage.setItem("totalSteps", totalSteps);
	              window.localStorage.setItem("lastActiveDate", $scope.currentTime);

	                    if(result.rankUped){
	                window.localStorage.setItem("tier", result.tier);
	                window.localStorage.setItem("tierPic", result.tierPic);
	                window.localStorage.setItem("criterionToNextTier", result.criterionToNextTier);
	                ionicToast.show("You have ranked up!", 'middle', false, 3500);
	              }

	              if(result.criterionToNextTier == 0){
	                $scope.progressToNextTier = "At the highest league!";
	                $scope.amtOfSteps = result.criterionToNextTier;
	                $scope.amtToNextTier = result.criterionToNextTier;
	              } else{
	                $scope.progressToNextTier = totalSteps + " / " + result.criterionToNextTier;
	                $scope.amtOfSteps = totalSteps;
	                $scope.amtToNextTier = result.criterionToNextTier;
	              }
	                 }

	             else {
	              if(result.errors == "Token invalid or expired"){
	                ionicToast.show(result.errors, 'middle', false, 3500);
	                $state.go('login');
	              } else{
	                ionicToast.show(result.errors, 'middle', false, 3500);
	              }
	             }

	          }, function (response) { // handle error
	            // log response.errors + " " + response.statusText
	            alert("Server down. Please try again later");
	          });
	        }

	        $scope.$broadcast('scroll.refreshComplete');
      };

    //===================================END OF REFRESH ANDROID===========================================

    //======END OF ANDROID======

    //======START OF IOS======
   /*$scope.$on("$ionicView.beforeEnter", function () {
     // Broadcast to the listener at side menu to update the profile pic, name and tier
     $rootScope.$broadcast("profileUpdated");

     $scope.currentTime = new Date().getTime();
     $scope.name = window.localStorage.getItem("name");
     $scope.tier = window.localStorage.getItem("tier");
     $scope.tierPic = window.localStorage.getItem("tierPic");
     $scope.clanName = window.localStorage.getItem("clanName");

     var criterionToNextTier = window.localStorage.getItem("criterionToNextTier");
     var totalSteps = window.localStorage.getItem("totalSteps");

     if(criterionToNextTier == 0){
       $scope.progressToNextTier = "At the highest league!";
       $scope.amtOfSteps = criterionToNextTier;
       $scope.amtToNextTier = criterionToNextTier;
     } else{
       $scope.progressToNextTier = totalSteps + " / " + criterionToNextTier;
       $scope.amtOfSteps = totalSteps;
       $scope.amtToNextTier = criterionToNextTier;
     }

     window.plugins.healthkit.querySampleTypeAggregated(
           {
               'startDate': new Date(new Date().setHours(0,0,0,0)), // six days ago
               'endDate': new Date(new Date().setHours(23,0,0,0)), // six days ago
               'aggregation': 'hour',
               'sampleType': 'HKQuantityTypeIdentifierStepCount',
               'unit': 'count'
           },function (value) {
               $scope.successProgress(value);
           },function (msg) {
               $scope.result = msg;
           });

     window.plugins.healthkit.sumQuantityType({

         'startDate': new Date(parseInt(window.localStorage.getItem("lastActiveDate"))),
         'endDate': new Date($scope.currentTime),
         'sampleType': 'HKQuantityTypeIdentifierStepCount',
         'unit': 'count'
       }, function(value){

         //var roundValue = Math.round(value);
         if(value != 0){
   				logData(JSON.stringify(value));
       		}
       }, function(msg){
         $scope.result=msg;
     });

   });

   $scope.successProgress = function (data) {
       var result = angular.fromJson(data);
       var totalSteps = 0;
       for(var i=0; i<result.length; i++){
         totalSteps += parseInt(result[i].value);
       }
       $scope.steps = totalSteps;
       var targetSteps = 10000;
       var percent = Math.round((totalSteps / targetSteps) * 100);

       if(percent < 10){
         $scope.progress = 0;
       }else if(percent < 30){
         $scope.progress = 10;
       }else if(percent < 45){
         $scope.progress = 30;
       }else if(percent < 60){
         $scope.progress = 45;
       }else if(percent < 75){
         $scope.progress = 60;
       }else if(percent < 90){
         $scope.progress = 75;
       }else if(percent < 100){
         $scope.progress = 90;
       }else{
         $scope.progress = 100;
       }

	    var walkingFactor = 0.57;
	    var caloriesBurnedPerMile = walkingFactor * (window.localStorage.getItem("weight") * 2.2); // Convert kg to pounds
	    var stride = window.localStorage.getItem("height") * 100 * 0.415; // Calculate the stride based on height
	    var stepToMile = 160934.4 / stride; // miles in cm
	    var conversationFactor = caloriesBurnedPerMile / stepToMile;
	    $scope.caloriesBurned = Math.ceil(totalSteps * conversationFactor);
      $scope.distance = Math.ceil(totalSteps * stride / 100);

       $scope.$apply();
   }

   function logData(data){

   $http({
       url: serverURL + "Fitnexx6Server/LogStepsServlet",
       method: "POST",
       data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'steps': data, 'height': window.localStorage.getItem("height"), 'weight': window.localStorage.getItem("weight"), 'lastActiveDateInMs': JSON.stringify($scope.currentTime)},
       headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
     }).then(function (response) { // handle success
       var result = angular.fromJson(response.data);

       if(result.status == "success"){
             // need to parseInt due to + being a concentrator as well. for - no need
               var points = parseInt(window.localStorage.getItem("points")) + parseInt(data);
         var totalSteps = parseInt(window.localStorage.getItem("totalSteps")) + parseInt(data);
               window.localStorage.setItem("points", points);
         window.localStorage.setItem("totalSteps", totalSteps);
         window.localStorage.setItem("lastActiveDate", $scope.currentTime);
         window.localStorage.setItem("clanName", result.clanName);
         $scope.clanName = result.clanName;

               if(result.rankUped){
           window.localStorage.setItem("tier", result.tier);
           window.localStorage.setItem("tierPic", result.tierPic);
           window.localStorage.setItem("criterionToNextTier", result.criterionToNextTier);
           ionicToast.show("You have ranked up!", 'middle', false, 3500);
         }

         if(result.criterionToNextTier == 0){
           $scope.progressToNextTier = "At the highest league!";
           $scope.amtOfSteps = result.criterionToNextTier;
           $scope.amtToNextTier = result.criterionToNextTier;
         } else{
           $scope.progressToNextTier = totalSteps + " / " + result.criterionToNextTier;
           $scope.amtOfSteps = totalSteps;
           $scope.amtToNextTier = result.criterionToNextTier;
         }
            }

        else {
         if(result.errors == "Token invalid or expired"){
           ionicToast.show(result.errors, 'middle', false, 3500);
           $state.go('login');
         } else{
           ionicToast.show(result.errors, 'middle', false, 3500);
         }
        }

     }, function (response) { // handle error
       // log response.errors + " " + response.statusText
       alert("Server down. Please try again later");
     });
   }

//	  ==============================Start of IOS Refresh=======================================

   $scope.doRefreshIOS = function() {

           $rootScope.$broadcast("profileUpdated");

           $scope.currentTime = new Date().getTime();
           $scope.name = window.localStorage.getItem("name");
           $scope.tier = window.localStorage.getItem("tier");
           $scope.tierPic = window.localStorage.getItem("tierPic");
					 $scope.clanName = window.localStorage.getItem("clanName");
					 
           var criterionToNextTier = window.localStorage.getItem("criterionToNextTier");
           var totalSteps = window.localStorage.getItem("totalSteps");

           if(criterionToNextTier == 0){
             $scope.progressToNextTier = "At the highest league!";
             $scope.amtOfSteps = criterionToNextTier;
             $scope.amtToNextTier = criterionToNextTier;
           } else{
             $scope.progressToNextTier = totalSteps + " / " + criterionToNextTier;
             $scope.amtOfSteps = totalSteps;
             $scope.amtToNextTier = criterionToNextTier;
           }

           window.plugins.healthkit.querySampleTypeAggregated(
                 {
                     'startDate': new Date(new Date().setHours(0,0,0,0)), // six days ago
                     'endDate': new Date(new Date().setHours(23,0,0,0)), // six days ago
                     'aggregation': 'hour',
                     'sampleType': 'HKQuantityTypeIdentifierStepCount',
                     'unit': 'count'
                 },function (value) {
                     $scope.successProgress(value);
                 },function (msg) {
                     $scope.result = msg;
                 });

           window.plugins.healthkit.sumQuantityType({

               'startDate': new Date(parseInt(window.localStorage.getItem("lastActiveDate"))),
               'endDate': new Date($scope.currentTime),
               'sampleType': 'HKQuantityTypeIdentifierStepCount',
               'unit': 'count'
             }, function(value){
             		if(value != 0){
         				logData(JSON.stringify(value));
             		}
             }, function(msg){
               $scope.result=msg;
           });
           $scope.$broadcast('scroll.refreshComplete');
         }*/

    //==============END OF IOS REFRESH================

    //======END OF IOS======

    $scope.viewHistory = function () {
        $state.go('tabsController.history',{});
    }

  	$scope.directLeagueDetails = function() {
		$state.go('tabsController.leagueDetails', {});
	}

})

.controller('profileCtrl', function($scope, $http, $state, ionicToast) {
		$scope.$on("$ionicView.beforeEnter", function () {
			$scope.DOB = window.localStorage.getItem("DOB");
			$scope.height = window.localStorage.getItem("height");
			$scope.weight = window.localStorage.getItem("weight");
			$scope.BMI = window.localStorage.getItem("BMI");
			$scope.profilePic = window.localStorage.getItem("profilePic");

		});

		$scope.editProfile = function () {
				$state.go('tabsController.editProfile',{height: $scope.height, weight: $scope.weight, profilePic: $scope.profilePic});
		}
})

.controller('editProfileCtrl', function($scope, $rootScope, $http, $state, ionicToast) {
		$scope.height = $state.params.height;
		$scope.weight = $state.params.weight;
		$scope.profilePicture = $state.params.profilePic;

		var tutorialActive = JSON.parse(window.localStorage.getItem("tutorialActive"));
		if(!tutorialActive.editProfileTutorial){
		  $scope.tutorialActive = true;
		  tutorialActive.editProfileTutorial = true;
		  window.localStorage.setItem("tutorialActive", JSON.stringify(tutorialActive));
		}else{
		  $scope.tutorialActive = false;
		}

		$scope.editProfile = function (height, weight, profilePicture) {
				$http({
						url: serverURL + "Fitnexx6Server/PersonaliseProfileServlet",
						method: "POST",
						 data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'height': height, 'weight':weight,'profilePic': profilePicture },
						headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
				}).then(function (response) { // handle success
						var result = angular.fromJson(response.data);

						if(result.status == "success"){
								window.localStorage.setItem("profilePic", result.profilePic);
								window.localStorage.setItem("height", result.height);
								window.localStorage.setItem("weight", result.weight);
								window.localStorage.setItem("BMI", result.BMI);

								// Broadcast to the listener at side menu to update the profile pic, name and tier
								$rootScope.$broadcast("profileUpdated");
								$state.go('tabsController.profile');
						}
						else {
							if(result.errors == "Token invalid or expired"){
								ionicToast.show(result.errors, 'middle', false, 3500);
								$state.go('login');
							} else{
								ionicToast.show(result.errors, 'middle', false, 3500);
							}
						}
				}, function (response) { // handle error
						// log response.status + " " + response.statusText
						alert("Server down. Please try again later");
				});
		};
})

.controller('historyCtrl', function($scope, $state, ionicToast, $cordovaHealthKit) {
		$scope.selected = 'Week';
		$scope.isActive = function(type) {
        return type === $scope.selected;
    };

    var tutorialActive = JSON.parse(window.localStorage.getItem("tutorialActive"));
    if(!tutorialActive.historyTutorial){
      $scope.tutorialActive = true;
      tutorialActive.historyTutorial = true;
      window.localStorage.setItem("tutorialActive", JSON.stringify(tutorialActive));
    }else{
      $scope.tutorialActive = false;
    }

		var myStepChart = function (temp) {
				$scope.options = {
						chart: {
								type: 'discreteBarChart',
								height: 450,
								margin : {
										top: 25,
										right: 20,
										bottom: 40,
										left: 20
								},
								x: function(d){return d.label;},
								y: function(d){return d.value;},
								showYAxis: false,
								showValues: true,
								staggerLabels: true,
								tooltip: {
									enabled: false
								},
								valueFormat: function(d){return d3.format(',.0f')(d);},
								duration: 100,
								useVoronoi: false,

						}
				};

				$scope.data = [
						{
								key: "Cumulative Return",
								color: "#d67777",
								values: temp
						}
				];
				$scope.$apply();
		}
    //======START OF ANDROID======
		 function successProgress(data){
		 		myStepChart(data);
		 		navigator.splashscreen.hide();
		 }
		 function failureProgress(data){
		 		$scope.result = "error " + data;
		 }

		 $scope.stepsByDay = function(type){
		 	fitnexx6.googlefit.retrieveStepsByDay(successProgress, failureProgress);
		 	$scope.selected = type;
		 };

		 $scope.stepsByWeek = function(type){
		 	fitnexx6.googlefit.retrieveStepsByWeek(successProgress, failureProgress);
		 	$scope.selected = type;
		 };

		 $scope.stepsByMonth = function(type){
		 	fitnexx6.googlefit.retrieveStepsByMonth(successProgress, failureProgress);
		 	$scope.selected = type;
		 };

		 $scope.$on("$ionicView.beforeEnter", function () {
		 	fitnexx6.googlefit.retrieveStepsByWeek(successProgress, failureProgress);
    });
    //======END OF ANDROID======

    //======START OF IOS======
    /*$cordovaHealthKit.isAvailable().then(function(yes) {
        $scope.status = "Yes, successful"; // Is available
    }, function(no) {
        $scope.status = "No, not successful"; // Is not available
    });

    $cordovaHealthKit.requestAuthorization(
    [
      'HKQuantityTypeIdentifierStepCount'
    ],
    [
      //'HKQuantityTypeIdentifierStepCount'
    ]
  ).then(function(success) {
    $scope.granted = true;
  }, function(err) {
    $scope.granted = false;
  });

  $scope.$on("$ionicView.beforeEnter", function () {
    $scope.stepsByWeek('Week');
  });

  $scope.stepsByDay = function (temp) {
  window.plugins.healthkit.querySampleTypeAggregated(
        {
            'startDate': new Date(new Date().setHours(0,0,0,0)), // six days ago
            'endDate': new Date(new Date().setHours(23,0,0,0)), // six days ago
            //'startDate': new Date(new Date(new Date().getTime() - 24 * 60 * 60 * 1000).setHours(0,0,0,0)),
            //'endDate': new Date(new Date(new Date().getTime() - 24 * 60 * 60 * 1000).setHours(23,0,0,0)), // now
            'aggregation': 'hour',
          'sampleType': 'HKQuantityTypeIdentifierStepCount',
          'unit': 'count'
        },function (value) {
            //$scope.value = value;
            //var result = angular.fromJson(v);
            //$scope.result = JSON.stringify(value);
            $scope.selected = temp;
            myStepChart(value);
        },function (msg) {
            $scope.result = msg;
        });
    }

    $scope.stepsByWeek = function (temp) {
    window.plugins.healthkit.querySampleTypeAggregated(
          {
            'startDate': new Date((new Date().getTime() - 6 * 24 * 60 * 60 * 1000)), //day
            'endDate': new Date(), // now
            'aggregation': 'day',
            'sampleType': 'HKQuantityTypeIdentifierStepCount',
            'unit': 'count'
          },function (value) {
              //$scope.value = value;
              //var result = angular.fromJson(v);
              $scope.selected = temp;
              myStepChart(value);
          },function (msg) {
              $scope.result = msg;
          });
      }

      $scope.stepsByMonth = function (temp) {
      window.plugins.healthkit.querySampleTypeAggregated(
          {
                'startDate': new Date((new Date().getTime() - 27 * 24 * 60 * 60 * 1000)), // six days ago
                'endDate': new Date((new Date().getTime() - 6 * 24 * 60 * 60 * 1000)), // now
                'aggregation': 'week',
                'sampleType': 'HKQuantityTypeIdentifierStepCount',
                'unit': 'count'
          },function (value) {
              //$scope.value = value;
              //var result = angular.fromJson(v);
              //$scope.result = JSON.stringify(value);
              $scope.selected = temp;
              myStepChart(value);
          },function (msg) {
              $scope.result = msg;
          });
      }*/
      //======END OF IOS======
})

.controller('ngeeAnnPolytechnicMapCtrl', ['$scope','$http', '$state', function($scope, $http, $state, $log, leafletData, leafletHelpers, leafletMapEvents, leafletEvents) {
	var tutorialActive = JSON.parse(window.localStorage.getItem("tutorialActive"));
  if(!tutorialActive.mapTutorial){
    $scope.tutorialActive = true;
    tutorialActive.mapTutorial = true;
    window.localStorage.setItem("tutorialActive", JSON.stringify(tutorialActive));
  }else{
    $scope.tutorialActive = false;
  }

	var blk = $state.params.blk;
	var map = L.map('map');
	var southWest = L.latLng(1.327966, 103.770487), // Southwest coordinates
	northEast = L.latLng(1.337768, 103.780557), // Northeast coordinates
	bounds = L.latLngBounds(southWest, northEast);
	//map.setMaxBounds(map.getBounds());
	cordova.plugins.diagnostic.requestLocationAuthorization(successCallback, errorCallback);

  function successCallback(){

 	 map.locate({setView: false});
 	 function onLocationFound(e) {
 	 }

 	 map.on('locationfound', onLocationFound);
  }

  function errorCallback(){
 	 ionicToast.show("Location error!", 'middle', false, 3500);
  }

	var RedIcon = L.Icon.Default.extend({
		options: {
			//iconUrl: 'https://cdn1.iconfinder.com/data/icons/Map-Markers-Icons-Demo-PNG/256/Map-Marker-Marker-Outside-Azure.png',
			//iconUrl: 'img/star.png',
			iconUrl: 'img/icons/blueMarkerBig.png',
			iconAnchor: [20, 30],
			iconSize: [30, 30],
			popupAnchor: [0, -40]
		}
	});
	var BlueIcon = L.Icon.Default.extend({
		options: {
			//iconUrl: 'https://cdn1.iconfinder.com/data/icons/Map-Markers-Icons-Demo-PNG/256/Map-Marker-Marker-Outside-Azure.png',
			iconUrl: 'img/icons/pinkMarkerBig.png',
			iconAnchor: [20, 30],
			iconSize: [30, 30],
			popupAnchor: [0, -40]
		}
	});
	var redIcon = new RedIcon();
	var blueIcon = new BlueIcon();
	//Other things we could have changed
	//    iconSize:     [25, 41], // size of the icon
	//    shadowSize:   [50, 64], // size of the shadow
	//  iconAnchor:   [12, 41]  // point of the icon which will correspond to marker's location
	//  shadowAnchor: [4, 62],  // the same for the shadow
	//  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
	// L.marker(markerLocation, {icon: redIcon}).addTo(map);

	$scope.$on("$ionicView.beforeEnter", function () {
		$http ({
			url: serverURL + "Fitnexx6Server/MapServlet",
			method: "POST",
			data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken")},
			headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}

		}).then(function (response) { // consider changing to ionic.beforeEnter
			var result = angular.fromJson(response.data);
			$scope.status = result.status;
			$scope.errors = result.errors;

			if(result.status == "success"){

				L.tileLayer('https://api.mapbox.com/styles/v1/jeremiasgoh/ciq1noy05003ldtnpwpkkeea4/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiamVyZW1pYXNnb2giLCJhIjoiY2lxMW5pdnAyMDB5YmZ0bTR2MjlkYjlpOSJ9.c2xTUXhru4niIYMVFQnKVA', {
					// attribution: 'map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
					center: [1.332796,103.775193],
					zoom: 13,
					minZoom: 13,
					maxZoom: 20,
					maxBounds: bounds,
					id: 'ciq1noy05003ldtnpwpkkeea4',
					accessToken: 'pk.eyJ1IjoiamVyZW1pYXNnb2giLCJhIjoiY2lxYzVrenIwMDFtZWZybTExejhjN3RheiJ9.6Dv-027CiApS9XuzaG0QKA'
				}).addTo(map);
				map.fitBounds(bounds);

				$scope.doInit = function() {
					leafletData.getMap('map').then(function(map) {
						popup.openOn(map);
						$scope.map = map;
						$log.info(map);
						$log.info(map);
					});
				}

				map.locate({setView: false});
		  	 function onLocationFound(e) {
		  		 var radius = e.accuracy/2;
		  		 L.marker(e.latlng).addTo(map)
		  		 .bindPopup("You are within " + 25 + " meters from this point");
		  		 L.circle(e.latlng, radius).addTo(map);
		  	 }

		  	 map.on('locationfound', onLocationFound);


				//$scope.buildings = result.buildings;
				markers = result.buildings;

				for ( var i=0; i < markers.length; ++i )
				{
					var checkpoints="";

					for ( var j=0; j < markers[i].checkpoint.length; ++j )
					{
						checkpoints += '<li class="item">'+ markers[i].checkpoint[j] + '</li>';
					}

					if(markers[i].blk == blk){
						L.marker( [markers[i].lat, markers[i].lng], {icon: blueIcon} ) //To Customize markers
						//L.marker( [markers[i].lat, markers[i].lng] )
						.bindPopup('<strong><u> To complete Mission head to '+ markers[i].name +' and scan QR!</u></strong><br/><b>Block: </b> '+ markers[i].blk +'<br/><b>Checkpoints </b> ' + '<br/><ul class="list" align="left">'+ checkpoints+'</ul>' )
						.addTo(map)
						.bindPopup('To complete Mission head to <strong><u>  '+ markers[i].name +'</u></strong> and click the camera icon in the Missions Tab to scan QR<br/><b>Block: </b> '+ markers[i].blk +'<br/><b>Checkpoints </b> ' + '<br/><ul class="list" align="left">'+ checkpoints+'</ul>' ).openPopup();

					} else{
						L.marker( [markers[i].lat, markers[i].lng], {icon: redIcon} ) //To Customize markers
						//L.marker( [markers[i].lat, markers[i].lng] )
						.bindPopup('<strong><u>'+ markers[i].name +'</u></strong><br/><b>Block: </b> '+ markers[i].blk +'<br/><b>Checkpoints </b> ' + '<br/><ul class="list" align="left">'+ checkpoints+'</ul>' )
						.addTo(map);
					}
				}
			}
			else {
				if(result.errors == "Token invalid or expired"){
					ionicToast.show(result.errors, 'middle', false, 3500);
					$state.go('login');
				} else{
					ionicToast.show(result.errors, 'middle', false, 3500);
				}
			}
		}, function (response) { // handle error
			// log response.status + " " + response.statusText
			alert("Server down. Please try again later");


		});
	});


}])

.controller('leaderboardCtrl', function($scope, $http, $state, $ionicPopup, $ionicLoading, ionicToast) {

  var tutorialActive = JSON.parse(window.localStorage.getItem("tutorialActive"));
  if(!tutorialActive.leaderboardTutorial){
    $scope.tutorialActive = true;
    tutorialActive.leaderboardTutorial = true;
    window.localStorage.setItem("tutorialActive", JSON.stringify(tutorialActive));
  }else{
    $scope.tutorialActive = false;
  }

$ionicLoading.show({template:'<p>Loading Leaderboard...</p><ion-spinner class="spinner-calm" icon="lines"></ion-spinner>'});

    $scope.selected = 'student';
    $scope.isActive = function(type) {
        return type === $scope.selected;
    };

    $http({
        url: serverURL + "Fitnexx6Server/LeaderboardServlet",
        method: "POST",
        data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken")},
        headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
    }).then(function (response) { // consider changing to ionic.beforeEnter
     // handle success
        var result = angular.fromJson(response.data);

        $scope.students = result.userMonth;
        var studEmail = window.localStorage.getItem("email");
        $scope.checkEmail = studEmail.toLowerCase();

        if(result.status == "success"){
          $scope.Student = function(type){
            $scope.students = result.userMonth;
            $scope.selected = type;
          }

          $scope.Clan = function(type) {
            $scope.clan = result.clanMonth;
            $scope.adventureOn = result.clanMonth.length;
            $scope.selected = type;

          }

        } else{
          if(result.errors == "Token invalid or expired"){
            ionicToast.show(result.errors, 'middle', false, 3500);
            $state.go('login');
          } else{
            ionicToast.show(result.errors, 'middle', false, 3500);
          }
        }

        $ionicLoading.hide();

    }, function (response) { // handle error
        // log response.status + " " + response.statusText
       $ionicLoading.hide();

       $ionicPopup.alert({
         title: 'Server Down',
         template: 'Please try again later'
       });
    });
})

.controller('logoutCtrl', function($scope, $http, $state, ionicToast, $ionicLoading, $ionicHistory,$timeout) {

	$scope.name = window.localStorage.getItem("name");
	$scope.tier = window.localStorage.getItem("tier");
	$scope.profilePic = window.localStorage.getItem("profilePic");

  // Listener that updates the profile pic, name and tier when it receives the broadcast
	$scope.$on("profileUpdated", function(){
	$scope.name = window.localStorage.getItem("name");
    $scope.tier = window.localStorage.getItem("tier");
    $scope.profilePic = window.localStorage.getItem("profilePic");
	});

	$scope.logout = function(){
		$ionicLoading.show({template:'Logging out....'});

		window.localStorage.clear();
	    $timeout(function () {
	        $ionicLoading.hide();
	        $ionicHistory.clearCache();
	        $ionicHistory.clearHistory();
	        $ionicHistory.nextViewOptions({ disableBack: true, historyRoot: true });
	        $state.go('login');
				}, 300);

	};
})

.controller('loginCtrl', function($scope, $http, $state, $ionicHistory, $ionicSideMenuDelegate, ionicToast) {
	$scope.$on('$ionicView.enter', function(){
					$ionicHistory.clearCache();
					$ionicHistory.clearHistory();
			});

		$scope.$on("$ionicView.enter", function () {
				$ionicSideMenuDelegate.canDragContent(false);
				navigator.splashscreen.hide();
		});
		$scope.login = function () {
				$http({
						url: serverURL + "Fitnexx6Server/LoginServlet",
						method: "POST",
						data: {'email': $scope.email, 'passWord': $scope.passWord},
						headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
				}).then(function (response) { // handle success
						var result = angular.fromJson(response.data);

						if(result.status == "success"){
								window.localStorage.setItem("email", $scope.email);
								window.localStorage.setItem("accessToken", result.accessToken);
								// store all profile info into localStorage
								window.localStorage.setItem("name", result.name);
								window.localStorage.setItem("DOB", result.DOB);
								window.localStorage.setItem("age", result.age);
								window.localStorage.setItem("profilePic", result.profilePic);
								window.localStorage.setItem("faculty", result.faculty);
								window.localStorage.setItem("gender", result.gender);
								window.localStorage.setItem("height", result.height);
								window.localStorage.setItem("weight", result.weight);
								window.localStorage.setItem("BMI", result.BMI);
								window.localStorage.setItem("points", result.points);
								window.localStorage.setItem("tier", result.tier);
								window.localStorage.setItem("tierPic", result.tierPic);
								window.localStorage.setItem("criterionToNextTier", result.criterionToNextTier);
								window.localStorage.setItem("totalSteps", result.totalSteps);
								window.localStorage.setItem("lastActiveDate", result.lastActiveDate);

								if(window.localStorage.getItem("tutorialActive") == null){
								  window.localStorage.setItem("tutorialActive", JSON.stringify({'editProfileTutorial': false, 'historyTutorial': false, 'leaderboardTutorial': false, 'missionsTutorial': false, 'homeTutorial': false, 'newsFeedTutorial': false, 'rewardsTutorial': false, 'mapTutorial': false}));
								}
                window.localStorage.setItem("clanName", "N/A");
								$ionicHistory.nextViewOptions({
										disableBack: true
								});
								$state.go('tabsController.mySteps');
						}
						else {
								$scope.result = result.errors;
								//ionicToast.show(message, position, stick, time);
								ionicToast.show($scope.result, 'middle', false, 3500);

						}
				}, function (response) { // handle error
								// log response.status + " " + response.statusText
								alert("Server down. Please try again later");
				});
		};

		$scope.$on('$ionicView.leave', function(){
				$ionicSideMenuDelegate.canDragContent(true);
		});


})

.controller('forgetPasswordCtrl', function($scope, $http, $state, ionicToast) {
		$scope.forgetPW = function() {
				$http({
						url: serverURL + "Fitnexx6Server/ForgetPWServlet",
						method: "POST",
						data: {'email': $scope.email},
						headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
				}).then(function (response) { // handle success
						var result = angular.fromJson(response.data);

						if(result.status == "success"){
							// toast success message and redirect to login
							ionicToast.show("An email has been sent to you to reset your password", 'middle', false, 3500);
							$state.go('login');
						} else{
							//ionicToast.show(message, position, stick, time);
							ionicToast.show(result.errors, 'middle', false, 3500);
						}

				}, function (response) { // handle error
						// log response.status + " " + response.statusText
						alert("Server down. Please try again later");
				});
		};
})

.controller('changePasswordCtrl', function($scope, $http, $state, ionicToast) {
		$scope.changePW = function() {
				$http({
						url: serverURL + "Fitnexx6Server/ChangePWServlet",
						method: "POST",
						data: {'accessToken': window.localStorage.getItem("accessToken"), 'email': window.localStorage.getItem("email"), 'oldPassword': $scope.currentPass, 'newPassword': $scope.newPass, 'confirmNewPassword': $scope.rePass},
						headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
				}).then(function (response) {
						var result = angular.fromJson(response.data);

						if(result.status == "success"){
							ionicToast.show("Password changed!", 'middle', false, 3500);
							$state.go('login');
						} else{
							if(result.errors == "Token invalid or expired"){
								ionicToast.show(result.errors, 'middle', false, 3500);
								$state.go('login');
							} else{
								ionicToast.show(result.errors, 'middle', false, 3500);
							}
						}
				}, function(response) {
						// log response.status + " " + response.statusText
						alert("Server down. Please try again later");
				});
		};
})

.controller('acceptMissionDetailsCtrl', function($scope, $ionicPopup, $ionicLoading, $ionicHistory, $http, $state, ionicToast) {
  $scope.result = angular.fromJson($state.params.result);

  $scope.options = {
      chart: {
          type: 'discreteBarChart',
          height: 300,
          margin : {
              top: 20,
              right: 20,
              bottom: 50,
              left: 20
          },
          x: function(d){return d.label;},
          y: function(d){return d.value;},
          showYAxis: false,
          showValues: true,
          staggerLabels: true,
          tooltip: {
            enabled: false
          },
          valueFormat: function(d){return d3.format(',.0f')(d);},
          duration: 100,
          useVoronoi: false,
      }
  };

  $scope.data = [
      {
          key: "Cumulative Return",
          color: "#d67777",
          values: $scope.result.missionCount
      }
  ];

  $scope.acceptMission = function(){
      $http({
        url: serverURL + "Fitnexx6Server/AcceptMissionServlet",
        method: "POST",
        data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'missionType': $scope.result.missionType, 'missionID': JSON.stringify($scope.result.missionID)},
        headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
      }).then(function (response) { // handle success

        var result = angular.fromJson(response.data);

        if(result.status == "success"){
            // toast success msg
            ionicToast.show("Mission Accepted!", 'middle', false, 3500); // show a accept or decline mission popup
            //$state.go($state.current, {}, {reload: true});
			      var mytab = "false";
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.missions', {mytab: mytab});
        }
        else {
            if(result.errors == "Token invalid or expired"){
              ionicToast.show(result.errors, 'middle', false, 3500);
              $state.go('login');
            } else{
              ionicToast.show(result.errors, 'middle', false, 3500);
            }
        }
      }, function (response) { // handle error
            // log response.errors + " " + response.statusText
            $ionicLoading.hide();
            alert("Server down. Please try again later");
      });
  }

  $scope.declineMission = function(){
    $ionicHistory.nextViewOptions({
        disableBack: true
    });
    $state.go("tabsController.missions");
  }

})

.controller('missionsDetailsCtrl', function($scope, $ionicPopup, $ionicLoading, $ionicHistory, $cordovaBarcodeScanner, $http, $state, ionicToast, $cordovaGeolocation) {

  $scope.type = $state.params.type;
  $scope.desc = $state.params.desc;
  $scope.points = $state.params.points;
  $scope.dateStarted = $state.params.dateStarted;
  $scope.missionID = $state.params.id;
  $scope.missionAcceptID = $state.params.acceptID;
  $scope.noOfSteps = $state.params.steps;

  if($scope.type == "Checkpoint"){
    $http({
        url: serverURL + "Fitnexx6Server/RemainingCheckpointsServlet",
        method: "POST",
        // JSON.stringify() for non String variables
        data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'acceptID': JSON.stringify($scope.missionAcceptID), 'missionID': JSON.stringify($scope.missionID)},
        headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
    }).then(function (response) { // handle success
        var result = angular.fromJson(response.data);

        if(result.status == "success"){
        	var alert = $ionicPopup.alert({
            title: 'Mission Completed!',
            template: '<center>You have been awarded ' + $scope.points + ' points</center>'
          });

          $scope.checkpoints = result.remainingCheckpoints;

          alert.then(function(res)  {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('tabsController.missions', {}, {reload: true});
          });
        }

        else if(result.status == "incomplete"){
        	$scope.checkpoints = result.remainingCheckpoints;
    		//$state.go($state.current, {}, {reload: true});
        }
        else {
            if(result.errors == "Token invalid or expired"){
              ionicToast.show(result.errors, 'middle', false, 1500);
              $state.go('login');
            } else{
              ionicToast.show(result.errors, 'middle', false, 1500);
            }
        }
    }, function (response) { // handle error
        // log response.errors + " " + response.statusText
        alert("Server down. Please try again later");
    });
  }

	//for button to abort mission.
	$scope.abortMission = function(acceptID) {

    var confirmPopup = $ionicPopup.confirm({
       title: 'Abort Mission',
       template: 'Are you sure you want to abort this mission?'
      });

   	confirmPopup.then(function(res) {
 		if(res) {
	   	  $http({
		      url: serverURL + "Fitnexx6Server/AbortMissionServlet",
		      method: "POST",
		      // JSON.stringify() for non String variables
		      data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'acceptID': JSON.stringify(acceptID)},
		      headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
		  }).then(function (response) { // handle success
		      var result = angular.fromJson(response.data);

		      if(result.status == "success"){
		          ionicToast.show("Mission Aborted!", 'middle', false, 1500); // show a accept or decline mission popup
		          //$state.go('tabsController.missions', {}, {reload: true});
              //var mytab = "false";
              $ionicHistory.nextViewOptions({
                  disableBack: true
              });
              $state.go('tabsController.missions', {}, {reload: true});
		      }
		      else {
		          if(result.errors == "Token invalid or expired"){
		            ionicToast.show(result.errors, 'middle', false, 1500);
		            $state.go('login');
		          } else{
		            ionicToast.show(result.errors, 'middle', false, 1500);
		          }
		      }
		  }, function (response) { // handle error
		      // log response.errors + " " + response.statusText
		      alert("Server down. Please try again later");
		  });
	    } else {
	       console.log('You are not sure');
	     }
	});
   }

    $scope.completeMission = function(acceptID, missionID, missionType, points, $ionicPopup) {

		  $scope.acceptID = acceptID;
		  $scope.missionID = missionID;
		  $scope.missionType = missionType;
		  $scope.points = points;

      if(missionType == "Steps"){
        //======FOR IOS=======
        //$scope.stepsByDay();

        //======FOR ANDROID======
        fitnexx6.googlefit.retrieveStepsByDay(successProgress, failureProgress);
      } else if(missionType == "Checkpoint"){
        cordova.plugins.diagnostic.getLocationAuthorizationStatus(function (status){
          if(status != cordova.plugins.diagnostic.permissionStatus.GRANTED && status != cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE){
            cordova.plugins.diagnostic.requestLocationAuthorization(successGPSPermission, errorGPSPermission);
            // not determined
          }else{
            successGPSPermission(status);
          }
        }, function (error){
          alert("Couldn't check the authorization status of your GPS. Please try again later");
        });

      } else {
        $cordovaBarcodeScanner.scan().then(function(imageData) {
          if(imageData.text != ""){
            completeSpecialMission(imageData.text);
          }
          }, function(error) {
            alert("Couldn't read the QR code. Please try again later");
          });
      }
	}

  //android funtion
	function successProgress(data){

	  	$ionicLoading.show({template:'<p>Loading...</p><ion-spinner class="spinner-calm" icon="lines"></ion-spinner>'});

        var result = angular.fromJson(data);
        var totalSteps = 0;
        for(var i=0; i<result.length; i++){
          totalSteps += parseInt(result[i].value);
        }

        $http({
            url: serverURL + "Fitnexx6Server/CompleteMissionServlet",
            method: "POST",
            // JSON.stringify() for non String variables
            data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'acceptID': JSON.stringify($scope.acceptID), 'missionID': JSON.stringify($scope.missionID), 'missionType': $scope.missionType, 'points': JSON.stringify($scope.points), 'steps': JSON.stringify(totalSteps)},
            headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
        }).then(function (response) { // handle success
            var result = angular.fromJson(response.data);

            if(result.status == "success"){
                var points = parseInt(window.localStorage.getItem("points")) + parseInt($scope.points);
                window.localStorage.setItem("points", points);

                var alert = $ionicPopup.alert({
	                 title: 'Mission Completed!',
	                 template: '<center>You have been awarded ' + $scope.points + ' points</center>'
              	});

              	alert.then(function(res)  {
                  $ionicHistory.nextViewOptions({
                      disableBack: true
                  });
                	$state.go('tabsController.missions', {}, {reload: true});
              	});

            }

            else {
                if(result.errors == "Token invalid or expired"){
                  ionicToast.show(result.errors, 'middle', false, 3500);
                  $state.go('login');
                } else{
                  ionicToast.show(result.errors, 'middle', false, 3500);
                }
            }

            $ionicLoading.hide();

        }, function (response) { // handle error
            // log response.errors + " " + response.statusText
            alert("Server down. Please try again later");
            $ionicLoading.hide();
        });

    }

    function failureProgress(data){
        alert("Couldn't retrieve your step count. Please try again later");
    }

    //ios function
    /*$scope.stepsByDay = function () {

    $ionicLoading.show({template:'<p>Loading...</p><ion-spinner class="spinner-calm" icon="lines"></ion-spinner>'});

    window.plugins.healthkit.querySampleTypeAggregated(
          {
              'startDate': new Date(new Date().setHours(0,0,0,0)), // six days ago
              'endDate': new Date(new Date().setHours(23,0,0,0)), // six days ago
              'aggregation': 'hour',
              'sampleType': 'HKQuantityTypeIdentifierStepCount',
              'unit': 'count'
          },function (value) {
              var result = angular.fromJson(value);
              var totalSteps = 0;
              for(var i=0; i<result.length; i++){
                totalSteps += parseInt(result[i].value);
              }

              $http({
                  url: serverURL + "Fitnexx6Server/CompleteMissionServlet",
                  method: "POST",
                  // JSON.stringify() for non String variables
                  data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'acceptID': JSON.stringify($scope.acceptID), 'missionID': JSON.stringify($scope.missionID), 'missionType': $scope.missionType, 'points': JSON.stringify($scope.points), 'steps': JSON.stringify(totalSteps)},
                  headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
              }).then(function (response) { // handle success
                  var result = angular.fromJson(response.data);

                  if(result.status == "success"){
                      var points = parseInt(window.localStorage.getItem("points")) + parseInt($scope.points);
                      window.localStorage.setItem("points", points);
                      var alert = $ionicPopup.alert({
	                     title: 'Mission Completed!',
				               template: '<center>You have been awarded ' + $scope.points + ' points</center>'
			              });

	                  alert.then(function(res)  {
                      $ionicHistory.nextViewOptions({
                          disableBack: true
                      });
	                    $state.go('tabsController.missions', {}, {reload: true});
	                  });
                  }
                  else {
                      if(result.errors == "Token invalid or expired"){
                        ionicToast.show(result.errors, 'middle', false, 3500);
                        $state.go('login');
                      } else{
                        ionicToast.show(result.errors, 'middle', false, 3500);
                      }
                  }

                  $ionicLoading.hide();

              }, function (response) { // handle error
                  // log response.errors + " " + response.statusText
                  alert("Server down. Please try again later");
                  $ionicLoading.hide();
              });

          },function (msg) {
              alert("Couldn't retrieve your step count. Please try again later");
          });
    }*/


	function successGPSPermission(data){
    if(data != cordova.plugins.diagnostic.permissionStatus.GRANTED && data != cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE){
      if(data == "not_determined"){
        cordova.plugins.diagnostic.getLocationAuthorizationStatus(function (status){
          if(status != cordova.plugins.diagnostic.permissionStatus.GRANTED && status != cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE){
            cordova.plugins.diagnostic.requestLocationAuthorization(successGPSPermission, errorGPSPermission);
            // not determined
          }else{
            successGPSPermission(status);
          }
        }, function (error){
          alert("Couldn't check the authorization status of your GPS. Please try again later");
        });
      }else{
      	$ionicPopup.alert({
         title: 'GPS',
         template: 'Fitnexx needs to access your GPS for the function to work'
       });
      }

    } else{
        cordova.plugins.diagnostic.isLocationEnabled(successGPSEnabled, failureGPSEnabled);

        function successGPSEnabled(data){
          if(!data){
            alert("Fitnexx needs your GPS to be on for the function to work.");
          } else{
            $cordovaBarcodeScanner.scan().then(function(imageData) {
              if(imageData.text != ""){
                completeCheckPoint(imageData.text);
              }
              }, function(error) {
                alert("Couldn't read the QR code. Please try again later");
              });
          }
        }

        function failureGPSEnabled(date){
          alert("Error trying to use the GPS. Please try again later");
        }
      }
    }

    function errorGPSPermission(data){
      alert("Error trying to get authorization for GPS. Please try again later");
    }

    function completeCheckPoint(imageText){

	  $ionicLoading.show({template:'<p>Loading...</p><ion-spinner class="spinner-calm" icon="lines"></ion-spinner>'});
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $cordovaGeolocation
        .getCurrentPosition(posOptions)
        .then(function (position) {
          var lat  = position.coords.latitude
          var long = position.coords.longitude
          $http({
              url: serverURL + "Fitnexx6Server/CompleteMissionServlet",
              method: "POST",
              // JSON.stringify() for non String variables
              data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'acceptID': JSON.stringify($scope.acceptID), 'missionID': JSON.stringify($scope.missionID), 'missionType': $scope.missionType, 'points': JSON.stringify($scope.points), 'checkPointID': imageText, 'lat': JSON.stringify(lat), 'long': JSON.stringify(long)},
              headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
          }).then(function (response) { // handle success
              var result = angular.fromJson(response.data);

              if(result.status == "success"){

                  var points = parseInt(window.localStorage.getItem("points")) + parseInt($scope.points);
                  window.localStorage.setItem("points", points);
                  var alert = $ionicPopup.alert({
		          	title: 'Mission Completed!',
		          	template: '<center>You have been awarded ' + $scope.points + ' points</center>'
		          });

	              alert.then(function(res) {
                  $ionicHistory.nextViewOptions({
                      disableBack: true
                  });
	                $state.go('tabsController.missions', {}, {reload: true});
	              });
              }
              else if(result.status == "incomplete"){
              	  $scope.checkpoints = result.remainingCheckpoints;
              	  $state.go($state.current, {}, {reload: true});
              }
              else {
                  if(result.errors == "Token invalid or expired"){
                    ionicToast.show(result.errors, 'middle', false, 3500);
                    $state.go('login');
                  } else{
                    ionicToast.show(result.errors, 'middle', false, 3500);
                  }
              }

              $ionicLoading.hide();

          }, function (response) { // handle error
              // log response.errors + " " + response.statusText
              alert(response.statusText + " Server down. Please try again later");
              $ionicLoading.hide();
          });
        }, function(err) {
          alert(err);
          $ionicLoading.hide();
        });
    }

    function completeSpecialMission(imageText){
      $ionicLoading.show({template:'<p>Loading...</p><ion-spinner class="spinner-calm" icon="lines"></ion-spinner>'});
      $http({
          url: serverURL + "Fitnexx6Server/CompleteMissionServlet",
          method: "POST",
          // JSON.stringify() for non String variables
          data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'acceptID': JSON.stringify($scope.acceptID), 'missionID': JSON.stringify($scope.missionID), 'missionType': $scope.missionType, 'points': JSON.stringify($scope.points), 'specialID': imageText},
          headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
      }).then(function (response) { // handle success
          var result = angular.fromJson(response.data);

          if(result.status == "success"){
              var points = parseInt(window.localStorage.getItem("points")) + parseInt($scope.points);
              window.localStorage.setItem("points", points);

              var alert = $ionicPopup.alert({
	              title: 'Mission Completed!',
	              template: '<center>You have been awarded ' + $scope.points + ' points</center>'
              });

              alert.then(function(res) {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                 $state.go('tabsController.missions', {});
              });
          }
          else {
              if(result.errors == "Token invalid or expired"){
                ionicToast.show(result.errors, 'middle', false, 3500);
                $state.go('login');
              } else{
                ionicToast.show(result.errors, 'middle', false, 3500);
              }
          }

          $ionicLoading.hide();

      }, function (response) { // handle error
          // log response.errors + " " + response.statusText
          alert(response.statusText + " Server down. Please try again later");
          $ionicLoading.hide();
      });
    }

})

.controller("missionsCtrl", function($scope, $ionicPopup, $ionicLoading, $cordovaBarcodeScanner, $http, $state, ionicToast, $cordovaGeolocation) {

  var tutorialActive = JSON.parse(window.localStorage.getItem("tutorialActive"));
  if(!tutorialActive.missionsTutorial){
    $scope.tutorialActive = true;
    tutorialActive.missionsTutorial = true;
    window.localStorage.setItem("tutorialActive", JSON.stringify(tutorialActive));
  }else{
    $scope.tutorialActive = false;
  }
  $ionicLoading.show({template:'<p>Loading Missions...</p><ion-spinner class="spinner-calm" icon="lines"></ion-spinner>'});
  $scope.mytab = $state.params.mytab;
  if($scope.mytab == "false") {
      $scope.tab = 2;
  } else {
      $scope.tab=1;
  }

    $scope.$on("$ionicView.beforeEnter", function () {
        $http ({
            url: serverURL + "Fitnexx6Server/ViewMissionsServlet",
            method: "POST",
            data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken")},
            headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}

        }).then(function (response) { // consider changing to ionic.beforeEnter
            var result = angular.fromJson(response.data);

            if(result.status == "success"){
              $scope.acceptedMissions = result.acceptedMissions;
              $scope.acceptedSize = result.acceptedMissions.length;
              $scope.availableMissions = result.availableMissions;
              $scope.availableSize = result.availableMissions.length;
							$scope.missionCount = result.missionCount;

            } else{
              if(result.errors == "Token invalid or expired"){
                ionicToast.show(result.errors, 'middle', false, 3500);
                $state.go('login');
              } else{
                ionicToast.show(result.errors, 'middle', false, 3500);
              }
            }
            $ionicLoading.hide();
        }, function(response) {
            // log response.status + " " + response.statusText
            $ionicLoading.hide();
            alert("Server down. Please try again later");
        });

        $scope.missionDetails = function (missionType, missionDescription, missionPoints , dateStart , missionID, missionAcceptID, noOfSteps ) {
			    $state.go('tabsController.missionDetails',{ type : missionType, desc : missionDescription, points : missionPoints, dateStarted: dateStart, id : missionID, acceptID : missionAcceptID , steps : noOfSteps});
		}

		$scope.missions = function(){
	 		$state.go($state.current, {}, {reload: true});
		}

        $scope.directMap = function(bid) {
	    	$state.go('tabsController.missionMap' , {blk : bid} );
		}

    });

	function scan(imageText){
			$http({
					url: serverURL + "Fitnexx6Server/ScanServlet",
					method: "POST",
					data: {'email': window.localStorage.getItem("email"), 'accessToken': window.localStorage.getItem("accessToken"), 'checkPointID': imageText},
					headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
			}).then(function (response) { // handle success
					var result = angular.fromJson(response.data);

					if(result.status == "success"){
							$state.go('tabsController.acceptMissionDetails', {result: response.data});
					}
					else {
							if(result.errors == "Token invalid or expired"){
								ionicToast.show(result.errors, 'middle', false, 3500);
								$state.go('login');
							} else{
								ionicToast.show(result.errors, 'middle', false, 3500);
							}
					}
			}, function (response) { // handle error
					// log response.errors + " " + response.statusText
					alert("Server down. Please try again later");
			});
		}

	$scope.scanBarcode = function() {
		cordova.plugins.diagnostic.requestCameraAuthorization(successCamera,failureCamera);

		function successCamera(data){

	        if(data=="DENIED"){
	          alert("Fitnexx needs to access your camera for the function to work.");
	        } else  {

				$cordovaBarcodeScanner.scan().then(function(imageData) {

				if(imageData.text != ""){
              		scan(imageData.text);
            	}

				}, function(error) {
						console.log("An error happened -> " + error);
				});
			}
		}

		function failureCamera(data){
			ionicToast.show("Camera Failed", 'middle', false, 3500);
		}
	};



})

.controller('aboutusCtrl', function($scope, $state, ionicToast) {


});
