angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

	// Ionic uses AngularUI Router which uses the concept of states
	// Learn more here: https://github.com/angular-ui/ui-router
	// Set up the various states which the app can be in.
	// Each state's controller can be found in controllers.js
  	$stateProvider


    .state('tabsController', {
        url: '/tabs',
        templateUrl: 'templates/tabsController.html',
        abstract:true
    })

    .state('tabsController.mySteps', {
        url: '/home',
        cache: false,
        views: {
            'home-tab': {
                templateUrl: 'templates/mySteps.html',
                controller: 'myStepsCtrl'
            }
        }
    })

    .state('tabsController.history', {
        url: '/history',
        cache: false,
        views: {
            'home-tab': {
                templateUrl: 'templates/history.html',
                controller: 'historyCtrl'
            }
        }
    })

    .state('tabsController.leaderboard', {
        url: '/lb',
        cache: false,
        views: {
            'rank-tab': {
                templateUrl: 'templates/leaderboard.html',
                controller: 'leaderboardCtrl'
            }
        }
    })

    .state('tabsController.missions', {
        url: '/missions',
        cache: false,
        params: {
            mytab: null
        },
        views: {
            'missions-tab': {
                templateUrl: 'templates/missions.html',
                controller: 'missionsCtrl'
            }
        }
    })

	.state('tabsController.missionDetails', {
        url: '/missionDetails',
        params: {
            type: null,
            desc: null,
            points: null,
            dateStarted: null,
            id: null,
            acceptID: null,
            steps: null
        },
        views: {
            'missions-tab': {
                templateUrl: 'templates/missionDetails.html',
                controller: 'missionsDetailsCtrl'
            }
        }
    })

	.state('tabsController.acceptMissionDetails', {
        url: '/acceptMissionDetails',
        cache: false,
        params: {
            result: null
        },
        views: {
            'missions-tab': {
                templateUrl: 'templates/acceptMissionDetails.html',
                controller: 'acceptMissionDetailsCtrl'
            }
        }
    })

    .state('tabsController.reward', {
        url: '/rewards',
		cache: true,
        views: {
            'reward-tab': {
                  templateUrl: 'templates/reward.html',
                  controller: 'rewardCtrl'
            }
        }
    })

    .state('tabsController.newsFeed', {
        url: '/news',
        cache: false,
        views: {
            'newsfeed-tab': {
                templateUrl: 'templates/newsFeed.html',
                controller: 'newsFeedCtrl'
            }
        }
    })

    .state('tabsController.fitNexxCard', {
        url: '/card',
        views: {
            'card-tab': {
                templateUrl: 'templates/fitNexxCard.html',
                controller: 'fitNexxCardCtrl'
            }
        }
    })

    .state('tabsController.profile', {
        url: '/profile',
		cache: false,
        views: {
            'profile-tab': {
                templateUrl: 'templates/profile.html',
                controller: 'profileCtrl'
            }
        }
    })

    .state('tabsController.editProfile', {
        url: '/editProfile',
		cache: false,
        params: {
            height: null,
            weight: null,
            profilePic: null
        },
        views: {
            'profile-tab': {
                templateUrl: 'templates/editProfile.html',
                controller: 'editProfileCtrl'
            }
        }
    })

    .state('tabsController.ngeeAnnPolytechnicMap', {
        url: '/map',
        cache: false,
        params: {
            blk : null
        },
        views: {
            'map-tab': {
                templateUrl: 'templates/ngeeAnnPolytechnicMap.html',
                controller: 'ngeeAnnPolytechnicMapCtrl'
            }
        }
    })

    .state('tabsController.missionMap', {
        url: '/missionMap',
        cache: false,
        params: {
            blk : null
        },
        views: {
            'missions-tab': {
                templateUrl: 'templates/missionMap.html',
                controller: 'ngeeAnnPolytechnicMapCtrl'
            }
        }
    })

    .state('tabsController.changePassword', {
        url: '/changepassword',
		cache: false,
        views: {
            'changepw-tab': {
                templateUrl: 'templates/changePassword.html',
                controller: 'changePasswordCtrl'
            }
        }
    })

    .state('tabsController.feedback', {
        url: '/feedback',
		cache: false,
        views: {
            'feedback-tab': {
                templateUrl: 'templates/feedback.html',
                controller: 'feedbackCtrl'
            }
        }
    })

    .state('register', {
        url: '/signup',
		cache: false,
        templateUrl: 'templates/register.html',
        controller: 'registerCtrl'
    })

    .state('tabsController.leagueDetails', {
        url: '/leagueDetails',
        cache: false,
        views: {
            'home-tab': {
                templateUrl: 'templates/leagueDetails.html'
            }
        }
    })

    .state('login', {
        url: '/Login',
        cache: false,
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
    })

    .state('forgetPassword', {
        url: '/forgetpassword',
		cache: false,
        templateUrl: 'templates/forgetPassword.html',
        controller: 'forgetPasswordCtrl'
    })

    .state('logout', {
         url: '/logout',
         cache: false,
        // templateUrl: 'templates/login.html',
         controller: 'logoutCtrl'
     })

     .state('tabsController.aboutus', {
         url: '/aboutus',
 		     cache: false,
         views: {
             'aboutus-tab': {
                 templateUrl: 'templates/aboutus.html',
                 controller: 'aboutusCtrl'
             }
         }
     })

});
